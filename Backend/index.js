require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const passport = require("passport");
const PORT = process.env.PORT || 5001;
mongoose
  .connect(process.env.DB_CONNECTION)
  .then(() => console.log("Database Connected"))
  .catch((err) => console.log(err));
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());

require("./Service/userAuth");
require("./Models");
require("./Routes")(app);

app.listen(PORT, () => {
  console.log("Server is running on port 5000");
});
