const ctl = require("../Controllers/authController");
const crud = require("../Controllers/crudController");
const authUser = require("../Middlewares/authUser")["authenticate"];

module.exports = (app) => {
  app.post(`${process.env.APP_URL}/auth/login`, ctl.login);
  app.post(`${process.env.APP_URL}/auth/register`, ctl.register);
  app.get(`${process.env.APP_URL}/auth/schools`, ctl.schools);
  app.post(
    `${process.env.APP_URL}/auth/select_school`,
    authUser,
    ctl.select_school
  );
};
