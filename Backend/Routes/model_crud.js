const crud = require("../Controllers/crudController");
const authUser = require("../Middlewares/authUser")["authenticate"];
const permissionMiddleware = require("../Middlewares/permissionMiddleware");

module.exports = (app) => {
  // app.get(
  //   `${process.env.APP_URL}/:model/ifDataExist/:data`,
  //   authUser,
  //   permissionMiddleware,
  //   (req, res, next) => {
  //     req.params.data = JSON.parse(req.params.data);
  //     next();
  //   },
  //   crud.ifDataExist
  // );
  app.get(
    `${process.env.APP_URL}/:model/by-role/:role`,
    authUser,
    permissionMiddleware,
    crud.getItemsByRole
  );
  app.get(
    `${process.env.APP_URL}/:model/`,
    authUser,
    permissionMiddleware,
    crud.index
  );
  app.get(
    `${process.env.APP_URL}/:model/getDataById`,
    authUser,
    permissionMiddleware,
    crud.getSingleData
  );
  app.post(
    `${process.env.APP_URL}/:model`,
    authUser,
    permissionMiddleware,
    crud.store
  );
  app.delete(
    `${process.env.APP_URL}/:model/bulk-delete`,
    authUser,
    permissionMiddleware,
    crud.bulkdelete
  );
  app.delete(
    `${process.env.APP_URL}/:model/:id`,
    authUser,
    permissionMiddleware,
    crud.delete
  );
  app.patch(
    `${process.env.APP_URL}/:model/:id`,
    authUser,
    permissionMiddleware,
    crud.updateItem
  );
};
