const ctl = require("../Controllers/countController");
const authUser = require("../Middlewares/authUser")["authenticate"];

module.exports = (app) => {
  app.get(`/api/${process.env.APP_VERSION}/count/:model`, ctl.counter);
  app.get(`/api/${process.env.APP_VERSION}/student_count`, ctl.student_counter);
  app.get(`/api/${process.env.APP_VERSION}/allpermission`, ctl.defaultRoles);
  app.get(
    `/api/${process.env.APP_VERSION}/getschools`,
    authUser,
    ctl.getSchools
  );
};
