module.exports = (app) => {
    require("./auth")(app);
    require("./count")(app);
    require("./model_crud")(app);
}
