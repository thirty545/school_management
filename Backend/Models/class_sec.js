const mongoose = require("mongoose");
const section = require("./section");
const classRef = require("./class");

const classSecSchema = new mongoose.Schema(
  {
    class: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassRef",
    },
    section: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SectionRef",
    },

    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("ClassSecRef", classSecSchema);
