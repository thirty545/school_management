const mongoose = require("mongoose");
const section = require("./section");
const subject = require("./subject");
const classSecRef = require("./class_sec");
// const teacherClass = require("./teacher");

const classSubTeacherSchema = new mongoose.Schema(
  {
    class_sec: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassSecRef",
    },
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Subject",
    },
    teacher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("ClassSubTeacherRef", classSubTeacherSchema);
