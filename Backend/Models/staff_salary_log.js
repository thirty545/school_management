const mongoose = require("mongoose");

const staffSalaryLogSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    isActive: {
      type: Boolean,
      required: true,
    },
    basic_salary: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure basic salary is non-negative
          return value >= 0;
        },
        message: "Basic salary must be non-negative",
      },
    },
    joined_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure the joined date is not in the future
          return value <= new Date();
        },
        message: "Joined date must not be in the future",
      },
    },
    increment_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure the increment date is not in the past
          return value >= new Date();
        },
        message: "Increment date must not be in the past",
      },
    },
    allowance: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure allowance is non-negative
          return value >= 0;
        },
        message: "Allowance must be non-negative",
      },
    },
    paid_salary: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure paid salary is non-negative
          return value >= 0;
        },
        message: "Paid salary must be non-negative",
      },
    },
    payment_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure the payment date is not in the future
          return value <= new Date();
        },
        message: "Payment date must not be in the future",
      },
    },
    description: {
      type: String,
      required: true,
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StaffSalaryLog", staffSalaryLogSchema);
