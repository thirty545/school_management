const { mongoose, Schema } = require("mongoose");

const fee_categorySchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length < 100;
        },
        message: "Must be less than 100 characters",
      },
    },
    payment_schedule: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length < 100;
        },
        message: "Must be less than 100 characters",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("FeeCategory", fee_categorySchema);
