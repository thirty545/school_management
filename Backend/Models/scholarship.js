const { mongoose, Schema } = require("mongoose");

const scholarshipSchema = new mongoose.Schema(
  {
    scholarship_percentage: {
      type: Number,
      required: true,
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },

  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Scholarship", scholarshipSchema);
