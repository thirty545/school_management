const { mongoose, Schema } = require("mongoose");

const student_fee_accountSchema = new mongoose.Schema({
  studentRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },

  fee_categoryRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "FeeCategory",
    required: true,
  },

  scholarshipRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Scholarship",
    required: true,
  },

  total_number_of_month: {
    type: Number,
    required: true,
  },

  paid_months: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("StudentFeeAccount", student_fee_accountSchema);
