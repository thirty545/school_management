const mongoose = require("mongoose");
const studentClassSchema = mongoose.Schema(
  {
    student: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    class_sec: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassSecRef",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Session",
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("student_class", studentClassSchema);
