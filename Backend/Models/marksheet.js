const mongoose = require("mongoose");
require("./class");
require("./student_bio");
require("./exams");
const marksheetSchema = new mongoose.Schema(
  {
    class: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassRef",
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    obtained_marks: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure obtained marks are non-negative
          return value >= 0;
        },
        message: "Obtained marks must be non-negative",
      },
    },
    pass_marks: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure pass marks are non-negative
          return value >= 0;
        },
        message: "Pass marks must be non-negative",
      },
    },
    full_marks: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure full marks are positive
          return value > 0;
        },
        message: "Full marks must be positive",
      },
    },
    exams: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Exam",
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

// Custom validation to ensure obtained marks and pass marks are less than or equal to full marks
marksheetSchema.path("obtained_marks").validate(function (value) {
  return value <= this.full_marks;
}, "Obtained marks must be less than or equal to full marks");

marksheetSchema.path("pass_marks").validate(function (value) {
  return value <= this.full_marks;
}, "Pass marks must be less than or equal to full marks");

module.exports = mongoose.model("Marksheet", marksheetSchema);
