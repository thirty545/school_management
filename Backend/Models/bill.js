const { mongoose, Schema } = require("mongoose");

const billSchema = new mongoose.Schema({
  studentRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },

  fee_classRef: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "FeeClass",
    required: true,
  },

  fee_categoryRef: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "FeeCategory",
    required: true,
  },
  parent_school: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "School",
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  session: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "SessionRef",
  },
});

module.exports = mongoose.model("Bill", billSchema);
