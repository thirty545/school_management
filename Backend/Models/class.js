const mongoose = require("mongoose");

const classSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      validate: {
        validator: function (value) {
          return value.length <= 100;
        },
        message: (props) =>
          `${props.value} exceeds the character limit of 10 for description.`,
      },
    },

    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("ClassRef", classSchema);
