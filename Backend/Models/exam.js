const mongoose = require("mongoose");
const { ADToBS, BSToAD } = require("bikram-sambat-js");

const examSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length < 100;
        },
        message: "Name must be 100 characters or less",
      },
    },

    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
    year: {
      type: Date,
      // validate: {
      //   validator: function (value) {
      //     const currentYear = new Date().getFullYear();
      //     const inputYear = value.getFullYear();
      //     console.log("inputYear:", inputYear);
      //     return (
      //       value <= new Date() && inputYear >= 1900 && inputYear <= currentYear
      //     );
      //   },
      //   message: "Year must be a valid year and not in the future",
      // },
      get: function (value) {
        // Convert the stored AD date to BS year for display
        if (value) {
          const bsDate = ADToBS(value);
          return bsDate.year.toString();
        }
        return value;
      },
      set: function (value) {
        if (value) {
          const bsYear = parseInt(value, 10);
          const bsDate = `${bsYear}-01-01`;
          const adDate = BSToAD(bsDate);
          if (adDate) {
            const jsDate = new Date(adDate.year, adDate.month - 1, adDate.day);
            if (!isNaN(jsDate.valueOf())) {
              return jsDate;
            }
          }
        }
        return value;
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Exam", examSchema);
