const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const { ADToBS, BSToAD } = require("bikram-sambat-js");

const schoolSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length <= 100;
        },
        message: (props) =>
          `${props.value} exceeds the character limit of 10 for description.`,
      },
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    address: {
      String,
    },
    slogan: {
      type: String,
    },
    logo: {
      type: String,
    },
    phone: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          return value > 0;
        },
        message: "Phone number must be positive",
      },
    },
    estd: {
      type: Date,
      // validate: {
      //   validator: function (value) {
      //     const currentYear = new Date().getFullYear();
      //     const inputYear = value.getFullYear();
      //     console.log("inputYear:", inputYear);
      //     return (
      //       value <= new Date() && inputYear >= 1900 && inputYear <= currentYear
      //     );
      //   },
      //   message: "Year must be a valid year and not in the future",
      // },
      get: function (value) {
        // Convert the stored AD date to BS year for display
        if (value) {
          const bsDate = ADToBS(value);
          return bsDate.year.toString();
        }
        return value;
      },
      set: function (value) {
        if (value) {
          const bsYear = parseInt(value, 10);
          const bsDate = `${bsYear}-01-01`;
          const adDate = BSToAD(bsDate);
          if (adDate) {
            const jsDate = new Date(adDate.year, adDate.month - 1, adDate.day);
            if (!isNaN(jsDate.valueOf())) {
              return jsDate;
            }
          }
        }
        return value;
      },
    },
    password: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length >= 6;
        },
        message: "Password must be at least 6 characters long",
      },
    },
  },
  {
    timestamps: true,
  }
);

schoolSchema.pre("save", function (next) {
  if (!this.password) return new Error({ message: "Password required" });

  try {
    console.log("IN hash password", this.password);
    const hashedPassword = bcrypt.hashSync(this.password, 10);
    this.password = hashedPassword;
    next();
  } catch (error) {
    new Error({ message: "Internal Server Error" });
  }
});

schoolSchema.pre("findOneAndUpdate", async function (next) {
  const update = this.getUpdate();

  if (update.password) {
    try {
      const hashedPassword = bcrypt.hashSync(update.password, 10);
      this.setUpdate({ ...update, password: hashedPassword });
    } catch (error) {
      return next(new Error("Internal Server Error"));
    }
  }
  next();
});

module.exports = mongoose.model("School", schoolSchema);
