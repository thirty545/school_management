const { mongoose, Schema } = require("mongoose");
const fee_category = require("./fee_category");

const fee_classSchema = new mongoose.Schema(
  {
    fee_category: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "FeeCategory",
      required: true,
    },
    class: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassRef",
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    increment_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          return value >= new Date();
        },
        message: "Increment date must not be in the past",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("FeeClass", fee_classSchema);
