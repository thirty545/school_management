const mongoose = require("mongoose");
require("./class");

const feeSchema = new mongoose.Schema(
  {
    fee_name: {
      type: String,
      required: true,
      validate: {
        validator: function (value) {
          return value.length < 100;
        },
        message: "Must be less than 100 characters",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
    classRef: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ClassRef",
    },
    fee_amount: {
      type: Number,
      required: true,
    },
    increment_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          return value >= new Date();
        },
        message: "Increment date must not be in the past",
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Fee", feeSchema);
