module.exports = () => {
  require("./class");
  require("./fee");
  require("./exam");
  require("./marksheet");
  require("./subject");
  require("./section");
  require("./session");
  require("./customPermission");
  require("./user");
  require("./school");
  require("./student");
  require("./teacher");
  require("./class_sec");
  require("./class_sec_student");
};
