const { mongoose, Schema } = require("mongoose");

const accountSchema = new mongoose.Schema(
  {
    student: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Student",
      required: true,
    },
    class_sec: {
      class: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class",
        required: true,
      },
      section: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Section",
        required: true,
      },
    },
    feesDue: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          return value >= 0;
        },
        message: "Fees due cannot be negative",
      },
    },
    totalPaid: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          return value >= 0;
        },
        message: "Total paid cannot be negative",
      },
    },
    balance: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          return value >= 0;
        },
        message: "Balance cannot be negative",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Account", accountSchema);
