const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const userSchema = new mongoose.Schema(
  {
    fname: String,
    lname: String,
    email: {
      type: String,
    },
    phone: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          return value > 0;
        },
        message: "Phone number must be positive",
      },
    },
    role: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Role",
    },
    gender: {
      type: String,
      enum: ["male", "female"],
    },
    dob: {
      type: Date,
      validate: {
        validator: function (value) {
          return value <= new Date();
        },
        message: "Date of birth must not be in the future",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    father_name: String,
    mother_name: String,
    address: String,
    profile_photo: String,
    password: {
      type: String,
      validate: {
        validator: function (value) {
          return value.length >= 6;
        },
        message: "Password must be at least 6 characters long",
      },
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    profile_photo: String,
  },
  {
    timestamps: true,
  }
);

userSchema.pre("save", function (next) {
  if (!this.password) return new Error({ message: "Password required" });

  try {
    console.log("IN hash password", this.password);
    const hashedPassword = bcrypt.hashSync(this.password, 10);
    this.password = hashedPassword;
    next();
  } catch (error) {
    new Error({ message: "Internal Server Error" });
  }
});

userSchema.pre("findOneAndUpdate", async function (next) {
  const update = this.getUpdate();

  if (update.password) {
    try {
      const hashedPassword = bcrypt.hashSync(update.password, 10);
      this.setUpdate({ ...update, password: hashedPassword });
    } catch (error) {
      return next(new Error("Internal Server Error"));
    }
  }
  next();
});

module.exports = mongoose.model("User", userSchema);
