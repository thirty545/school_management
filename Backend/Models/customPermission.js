const mongoose = require("mongoose");
const customPermission = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    permissions: {
      type: Map,
      of: {
        type: Map,
        of: Boolean,
      },
      default: {},
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);
module.exports = mongoose.model("CustomPermission", customPermission);
