//1.importing mongoose
const mongoose = require("mongoose");

const roleSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },

    permissions: {
      type: Map,
      of: {
        type: Map,
        of: Boolean,
      },
      default: {},
    },
  },
  {
    timestamps: true,
  }
);
//export the module
module.exports = mongoose.model("Role", roleSchema);
