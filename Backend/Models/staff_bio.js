const mongoose = require("mongoose");

const staffBioSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },

    qualification: {
      type: String,
      required: true,
    },
    contract_date: {
      type: Date,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure the contract date is not in the past
          return value >= new Date();
        },
        message: "Contract date must not be in the past",
      },
    },
    passed_university: {
      type: String,
      required: true,
    },
    citizenship_no: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure citizenship number is positive
          return value > 0;
        },
        message: "Citizenship number must be positive",
      },
    },
    citizenship_photo: {
      type: String,
      required: true,
    },
    class_teacher: {
      type: String,
      required: true,
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    session: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "SessionRef",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StaffBio", staffBioSchema);
