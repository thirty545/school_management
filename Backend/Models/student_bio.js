const mongoose = require("mongoose");

const studentBioSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    parents_name: {
      type: String,
      required: true,
    },
    parents_phone: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // For example, let's say you want to ensure parents' phone number is positive
          return value > 0;
        },
        message: "Parents' phone number must be positive",
      },
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StudentBio", studentBioSchema);
