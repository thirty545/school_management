const mongoose = require("mongoose");
require("./fee");

const studentFeeSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    fee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Fee",
    },
    paid_fee: {
      type: Number,
      required: true,
      validate: {
        validator: function (value) {
          // Add your custom validation logic here
          // For example, let's say you want to ensure paid fee is non-negative
          return value >= 0;
        },
        message: "Paid fee must be non-negative",
      },
    },
    payment_date: {
      type: Date,
      validate: {
        validator: function (value) {
          // Add your custom validation logic here
          // For example, let's say you want to ensure the payment date is not in the future
          return value <= new Date();
        },
        message: "Payment date must not be in the future",
      },
    },
    parent_school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("StudentFee", studentFeeSchema);
