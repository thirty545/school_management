const Role = require("../Models/role");

const authAdmin = async (req, res, next) => {
  try {
    if (!req.user || !req.user.role) {
      return res.status(401).json({ message: "Authentication failed" });
    }

    const role = await Role.findById(req.user.role);

    if (!role || role.name !== "admin") {
      return res
        .status(403)
        .json({ message: `Permission denied for ${role.name}` });
    }

    return next();
  } catch (error) {
    return res.status(500).json({ message: "Internal Server Error" });
  }
};

module.exports = authAdmin;
