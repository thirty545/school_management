const CustomPermission = require("../Models/customPermission");
const UserModel = require("../Models/user");
require("../Models/role");

const permissionMiddleware = async (req, res, next) => {
  const user_id = req.user._id;
  const { model } = req.params;
  const method = req.method.toLowerCase();
  let user;
  let userPermissions;

  try {
    user = await CustomPermission.findOne({ user: user_id });

    if (user) {
      userPermissions = mapToObject(user.permissions);
    } else {
      user = await UserModel.findById(user_id).populate("role");

      if (user.role.name === "superadmin" || user.role.name === "School")
        return next();
      if (!user && !user.role)
        return res.status(403).json({ error: "User or role not found" });

      userPermissions = mapToObject(user.role.permissions);
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Internal Server Error" });
  }

  if (req.path.includes("/by-role/")) {
    if (!userPermissions[model]?.read) {
      return res
        .status(403)
        .json({ error: "No read permission to perform this action" });
    }
  }

  if (method === "get" && !userPermissions[model]?.read) {
    return res
      .status(403)
      .json({ error: "No read permission to perform this action" });
  }

  if (
    (method === "put" || method === "patch" || method === "post") &&
    !userPermissions[model]?.write
  ) {
    return res
      .status(403)
      .json({ error: "No write permission to perform this action" });
  }

  if (method === "delete" && !userPermissions[model]?.delete) {
    return res
      .status(403)
      .json({ error: "No delete permission to perform this action" });
  }

  next();
};

function mapToObject(map) {
  const obj = {};
  for (let [key, value] of map) {
    if (value instanceof Map) {
      obj[key] = mapToObject(value);
    } else {
      obj[key] = value;
    }
  }
  return obj;
}

module.exports = permissionMiddleware;
