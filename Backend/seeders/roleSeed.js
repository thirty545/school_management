const class_sec = require("../Models/class_sec");
const Role = require("../Models/role");

const defaultRoles = [
  {
    name: "School",
    permissions: {
      // Add school-specific permissions
    },
  },
  {
    name: "Student",
    permissions: {
      class: { read: false, write: false, delete: false },
      section: { read: false, write: false, delete: false },
      exams: { read: false, write: false, delete: false },
      fee: { read: false, write: false, delete: false },
      marksheet: { read: false, write: false, delete: false },
      role: { read: false, write: false, delete: false },
      staff_bio: { read: false, write: false, delete: false },
      staff_salary_log: { read: false, write: false, delete: false },
      student_bio: { read: false, write: false, delete: false },
      student_class: { read: false, write: false, delete: false },
      student_fee: { read: false, write: false, delete: false },
      subject_class: { read: false, write: false, delete: false },
      subject: { read: false, write: false, delete: false },
      teacher_class: { read: false, write: false, delete: false },
      user: { read: false, write: false, delete: false },
    },
  },
  {
    name: "Teacher",
    permissions: {
      class: { read: true, write: false, delete: false },
      class_sec: { read: true, write: false, delete: false },
      section: { read: true, write: false, delete: false },
      exams: { read: true, write: true, delete: false },
      fee: { read: false, write: false, delete: false },
      marksheet: { read: true, write: true, delete: false },
      role: { read: false, write: false, delete: false },
      staff_bio: { read: true, write: false, delete: false },
      staff_salary_log: { read: false, write: false, delete: false },
      student_bio: { read: true, write: false, delete: false },
      student_class: { read: true, write: false, delete: false },
      student_fee: { read: false, write: false, delete: false },
      subject_class: { read: true, write: false, delete: false },
      subject: { read: true, write: false, delete: false },
      teacher_class: { read: false, write: false, delete: false },
      user: { read: false, write: false, delete: false },
    },
  },
];

async function seedDefaultRoles(schoolId) {
  console.log("seeding default roles for school:", schoolId);
  try {
    const rolesToCreate = defaultRoles.map((role) => ({
      ...role,
      parent_school: schoolId,
    }));

    const existingRoles = await Role.find({
      name: { $in: defaultRoles.map((role) => role.name) },
      parent_school: schoolId,
    });

    if (existingRoles.length !== defaultRoles.length) {
      const newRoles = rolesToCreate.filter(
        (role) => !existingRoles.some((r) => r.name === role.name)
      );
      await Role.insertMany(newRoles);
      console.log("Default roles seeded successfully for school:", schoolId);
    } else {
      console.log("Default roles already exist for school:", schoolId);
    }
  } catch (error) {
    console.error("Error seeding default roles:", error);
  }
}

module.exports = seedDefaultRoles;
