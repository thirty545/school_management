const mongoose = require("mongoose");

// Index
exports.index = async (req, res) => {
  try {
    const {
      page = 1,
      limit = 10,
      col = "created_at",
      order = -1,
      s_field = "",
      s_query = "",
    } = req.query;

    const model = req.params.model;
    const Model = require(`../Models/${model}`);

    const schema = Model.schema;
    const parent_school = req.user.parent_school || req.user._id;

    const isForeignKey = (path) =>
      schema.paths[path] instanceof mongoose.Schema.Types.ObjectId;

    const potentialForeignKeys = Object.keys(schema.paths).filter(
      (path) => path !== "_id" && isForeignKey(path)
    );

    let query = { parent_school: parent_school };

    if (s_field && s_query) {
      if (schema.path(s_field)) {
        if (schema.path(s_field) instanceof mongoose.Schema.Types.ObjectId) {
          query[s_field] = s_query;
        } else {
          query[s_field] = { $regex: new RegExp(s_query, "i") };
        }
      } else {
        console.warn(`Warning: ${s_field} is not a valid field in the schema.`);
      }
    }

    if (model === "role") {
      query.name = { $ne: "school" };
    }

    if (model === "user") {
      query.role = { $ne: req.user.role };
    }

    // Ensure page and limit are positive integers
    const safePage = Math.max(1, parseInt(page, 10));
    const safeLimit = Math.max(1, parseInt(limit, 10));

    const totalRows = await Model.countDocuments(query);
    const totalPages = Math.ceil(totalRows / safeLimit);

    let mongooseQuery = Model.find(query);

    mongooseQuery = mongooseQuery
      .limit(safeLimit)
      .skip((safePage - 1) * safeLimit)
      .sort({ [col]: order })
      .select("-password");

    if (potentialForeignKeys.length > 0) {
      potentialForeignKeys.forEach((key) => {
        if (key === "class_sec") {
          mongooseQuery = mongooseQuery.populate({
            path: "class_sec",
            populate: [
              { path: "class", select: "name" },
              { path: "section", select: "name" },
            ],
          });
        } else {
          mongooseQuery = mongooseQuery.populate(key);
        }
      });
    }

    console.log("Query:", JSON.stringify(query, null, 2));

    const data = await mongooseQuery.exec();

    console.log(model, "data count:", data.length);

    res.json({ data: data || [], totalPages, totalRows });
  } catch (error) {
    console.error("Error in index:", error);
    res.status(500).json({ message: error.message });
  }
};
// Store
exports.store = async (req, res) => {
  try {
    console.log("Received data:", req.body);
    const model = req.params.model;

    const Model = require(`../Models/${model}`);
    const data = req.body;

    if (
      model === "subject" ||
      model === "role" ||
      model === "exam" ||
      model === "class" ||
      model === "section"
    ) {
      const existingName = await Model.findOne({
        name: data.name,
        parent_school: req.user.parent_school || req.user._id,
      });
      if (existingName) {
        return res
          .status(409)
          .json({ message: `${model} name already exists` });
      }
    } else if (model === "class_sec") {
      const existingClass = await Model.findOne({
        class: data.class,
        section: data.section,
        parent_school: req.user.parent_school || req.user._id,
      });
      if (existingClass) {
        return res.status(409).json({ message: `${model} already exists` });
      }
    } else if (model === "student_class") {
      const existingStudent = await Model.findOne({
        student: data.student,
        class: data.class,
        parent_school: req.user.parent_school || req.user._id,
      });
      if (existingStudent) {
        return res.status(409).json({ message: `${model} already exists` });
      }
    } else if (model === "class_sub_teacher") {
      const existingSub = await Model.findOne({
        class: data.class,
        subject: data.subject,
        parent_school: req.user.parent_school || req.user._id,
      });
      if (existingSub) {
        return res
          .status(409)
          .json({ message: "Subject already exists for this class" });
      }
    }

    data.createdBy = req.user._id;
    data.parent_school = req.user.parent_school || req.user._id;

    const item = new Model(data);
    const savedItem = await item.save();

    return res.status(200).json({
      data: savedItem,
      message: `${model} added successfully`,
    });
  } catch (error) {
    console.error("Error in store:", error);
    res.status(500).json({ message: error.message || "Internal Server Error" });
  }
};

// Get Single Data
exports.getSingleData = async (req, res) => {
  try {
    const model = req.params.model;
    const Model = require(`../Models/${model}`);
    const id = req.query.id;

    if (!id) {
      return res.status(400).json({ message: "ID is required" });
    }

    const schema = Model.schema;
    const isForeignKey = (path) =>
      schema.paths[path] instanceof mongoose.Schema.Types.ObjectId;

    const potentialForeignKeys = Object.keys(schema.paths).filter(
      (path) => path !== "_id" && isForeignKey(path)
    );

    let query = Model.findById(id);

    if (potentialForeignKeys.length > 0) {
      potentialForeignKeys.forEach((key) => {
        if (key === "class_sec") {
          query = query.populate({
            path: "class_sec",
            populate: [
              { path: "class", select: "name" },
              { path: "section", select: "name" },
            ],
          });
        } else {
          query = query.populate(key);
        }
      });
    }

    const data = await query.exec();

    if (!data) {
      return res.status(404).json({ message: "Data not found" });
    }

    return res.json({ data });
  } catch (error) {
    console.error("Error in getSingleData:", error);
    return res
      .status(500)
      .json({ message: "Error fetching data", error: error.message });
  }
};

// Delete
exports.delete = async (req, res) => {
  try {
    const model = req.params.model;
    const id = req.params.id;
    const Model = require(`../Models/${model}`);

    const deletedUnit = await Model.findByIdAndRemove(id);
    if (!deletedUnit) {
      return res.status(404).json({ message: `${model} not found` });
    }

    res.json({ message: `${model} deleted successfully` });
  } catch (error) {
    console.error("Error in delete:", error);
    res.status(500).json({ message: `${model} deleted successfully` });
  }
};

// Bulk Delete
exports.bulkdelete = async (req, res) => {
  try {
    const model = req.params.model;
    const Model = require(`../Models/${model}`);
    const { ids } = req.body;

    if (!ids || !Array.isArray(ids) || ids.length === 0) {
      return res.status(400).json({ message: "Invalid or empty array of IDs" });
    }

    const result = await Model.deleteMany({ _id: { $in: ids } });

    if (result.deletedCount > 0) {
      return res.json({
        message: `${result.deletedCount} documents deleted successfully`,
      });
    } else {
      return res.json({ message: "No documents found for the given IDs" });
    }
  } catch (error) {
    console.error("Error in bulkdelete:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

exports.getItemsByRole = async (req, res) => {
  try {
    const {
      page = 1,
      limit = 10,
      col = "created_at",
      order = -1,
      s_field = "",
      s_query = "",
    } = req.query;
    const { model, role } = req.params;
    const Model = require(`../Models/${model}`);
    const Role = require("../Models/role");

    const roleDoc = await Role.findOne({ name: role });

    if (!roleDoc) {
      return res.status(404).json({ message: "Role not found" });
    }

    let query = Model.find({
      role: roleDoc._id,
      parent_school: req.user.parent_school || req.user._id,
    });

    if (s_field && s_query) {
      query = query.where(s_field).regex(new RegExp(s_query, "i"));
    }

    const totalRows = await Model.countDocuments(query);
    const totalPages = Math.ceil(totalRows / limit);

    const data = await query
      .limit(+limit)
      .skip((page - 1) * limit)
      .sort({ [col]: order })
      .select("-password")
      .populate("role", "name")
      .exec();

    res.json({ data, totalPages, totalRows });
  } catch (error) {
    console.error("Error in getItemsByRole:", error);
    res.status(500).json({ message: error.message });
  }
};

// Update Item
exports.updateItem = async (req, res) => {
  try {
    const model = req.params.model;
    const id = req.params.id;
    const Model = require(`../Models/${model}`);
    const updatedData = req.body;

    if (
      model === "subject" ||
      model === "role" ||
      model === "exam" ||
      model === "class" ||
      model === "section"
    ) {
      const existingName = await Model.findOne({
        name: updatedData.name,
        parent_school: req.user.parent_school || req.user._id,
        _id: { $ne: id },
      });
      if (existingName) {
        return res
          .status(409)
          .json({ message: `${model} name already exists` });
      }
    } else if (model === "class_sec") {
      const existingClass = await Model.findOne({
        class: updatedData.class,
        section: updatedData.section,
        parent_school: req.user.parent_school || req.user._id,
        _id: { $ne: id },
      });
      if (existingClass) {
        return res.status(409).json({ message: `${model} already exists` });
      }
    } else if (model === "student_class") {
      const existingStudent = await Model.findOne({
        student: updatedData.student,
        class: updatedData.class,
        parent_school: req.user.parent_school || req.user._id,
        _id: { $ne: id },
      });
      if (existingStudent) {
        return res.status(409).json({ message: `${model} already exists` });
      }
    }

    const item = await Model.findByIdAndUpdate(id, updatedData, { new: true });

    if (!item) {
      return res.status(404).json({ message: "Not found" });
    }
    return res
      .status(200)
      .json({ data: item, message: `${model} updated successfully` });
  } catch (error) {
    console.error("Error in updateItem:", error);
    res.status(500).json({ message: "Error updating" });
  }
};

exports.createBill = async (req, res) => {
  try {
    console.log("Received data:", req.body);
    const { student, class: studentClass, section, fees } = req.body;
    const parent_school = req.user.parent_school || req.user._id;

    // Check if the account exists
    let account = await Account.findOne({
      student,
      class: studentClass,
      section,
      parent_school,
    });

    if (!account) {
      return res.status(404).json({ message: "Account not found" });
    }

    // Calculate the total fee
    let totalAmount = 0;
    for (const fee of fees) {
      const feeCategory = await Fee.findById(fee.fee_category);
      if (feeCategory) {
        totalAmount += fee.amount;
      } else {
        return res.status(404).json({ message: "Fee category not found" });
      }
    }

    // Create the bill
    const billData = {
      student,
      class: studentClass,
      section,
      fees,
      totalAmount,
      createdBy: req.user._id,
      parent_school,
    };

    const bill = new Bill(billData);
    const savedBill = await bill.save();

    // Update the account balance and last bill reference
    account.balance += totalAmount;
    account.lastBill = savedBill._id;
    await account.save();

    return res.status(200).json({
      data: savedBill,
      message: "Bill created successfully and account updated",
    });
  } catch (error) {
    console.error("Error in createBill:", error);
    res.status(500).json({ message: error.message || "Internal Server Error" });
  }
};