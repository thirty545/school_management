const mongoose = require("mongoose");
const user = require("../Models/user");
exports.counter = (req, res) => {
  const model = req.params.model;
  const Model = require(`../Models/${model}`);
  Model.countDocuments({})
    .then((count) => {
      res.json({
        model: model,
        count: count,
      });
    })
    .catch((err) => {
      res.json(err);
    });
};
exports.student_counter = async (req, res) => {
  try {
    const section = require("../Models/class");
    const studentModel = require("../Models/student_class");
    const allClass = await section.find();
    let allData = [];

    await Promise.all(
      allClass.map(async (item) => {
        let class_id = item._id;
        let students = await studentModel.find({ class_id: class_id });
        let studentCount = students.length;
        allData.push({
          grade: item.grade,
          section: item.section,
          totalStudents: studentCount,
        });
      })
    );

    res.json(allData);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};
exports.defaultRoles = (req, res) => {
  const models = [
    "class",
    "exams",
    "fee",
    "marksheet",
    "role",
    "staff_bio",
    "staff_salary_log",
    "student_bio",
    "student_class",
    "student_fee",
    "subject_class",
    "subject",
    "teacher_class",
    "user",
  ];
  let permissionObject = {};
  models.forEach(
    (item) =>
      (permissionObject[item] = { write: false, read: false, delete: false })
  );
  res.json(permissionObject);
};

exports.getSchools = async (req, res) => {
  try {
    const Model = mongoose.model("User");
    const schools = await Model.find({ parent_school: { $exists: false } });
    const ss = schools.filter((school) => school.email !== req.user.email);

    res.json(ss);
  } catch (error) {
    console.log("error occured during getting school", error);
    res.status(500).json({ error: " Error occured" });
  }
};
