const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../Models/user");
const School = require("../Models/school");
const Role = require("../Models/role");
const seedDefaultRoles = require("../seeders/roleSeed");

exports.login = async (req, res) => {
  const { email, password, school } = req.body;

  const isEmail = /\S+@\S+\.\S+/.test(email);
  const isPhone = /^\d{10}$/.test(email);

  let query;
  if (isEmail) {
    query = { email: email, parent_school: school };
  } else if (isPhone) {
    query = { phone: email, parent_school: school };
  } else {
    return res.status(400).json({ message: "Invalid email or phone format" });
  }
  try {
    const user = await User.findOne(query);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    const isValidUser = bcrypt.compareSync(password, user.password);
    if (!isValidUser) {
      return res.status(401).json({ message: "Invalid password" });
    }

    const userRole = await Role.findOne({ _id: user.role });
    console.log("User role:", userRole.name);

    // Create token
    const userDetails = {
      uid: user._id,
      email: user.email,
      role: user.role,
      role_name: userRole.name,
      parent_school: user.parent_school,
    };
    const session = jwt.sign(userDetails, process.env.JWT_SECRET, {
      expiresIn: parseInt(process.env.EXPIRE_IN) || 86400,
    });
    console.log("Token created");

    // Send response with token
    res.status(200).json({
      session,
      message: "User logged in Successfully!",
      userDetails: userDetails,
    });
  } catch (error) {
    console.error("Login error:", error);
    res
      .status(500)
      .json({ message: "Internal Server Error", error: error.message });
  }
};

exports.register = async (req, res) => {
  const data = req.body;

  if (!data.email || !data.password) {
    return res.status(400).json({ message: "Fill all details" });
  }

  const existingSchool = await School.findOne({ email: data.email });
  if (existingSchool) {
    return res.status(401).json({ message: "Email already taken" });
  }

  if (data.password.length < 6) {
    return res.status(400).json({ message: "Choose a stronger password" });
  }

  try {
    // Hash the password before saving it
    const hashedPassword = await bcrypt.hash(data.password, 10);

    const newSchool = new School({
      name: data.name,
      address: data.address,
      phone: data.phone,
      slogan: data.slogan,
      logo: data.logo,
      estd: data.estd,
      email: data.email,
      password: hashedPassword, // Store the hashed password
    });

    await newSchool.save();

    // Create roles for the school
    const roles = await seedDefaultRoles(newSchool._id);
    const schoolRole = await Role.findOne({
      name: "School",
      parent_school: newSchool._id,
    });

    const newUser = new User({
      fname: "Admin",
      email: data.email,
      password: hashedPassword, // Use the same hashed password
      parent_school: newSchool._id,
      role: schoolRole._id,
      phone: data.phone,
      address: data.address,
    });

    await newUser.save();

    return res.status(201).json({
      message: "Registration successful",
      schoolId: newSchool._id,
      userId: newUser._id,
    });
  } catch (error) {
    return res
      .status(500)
      .json({ message: "Registration failed", error: error.message });
  }
};

exports.schools = async (req, res) => {
  try {
    const model = req.params.model;
    const schools = await School.find({});
    return res.json({ data: schools });
  } catch (error) {
    console.error("Error in index:", error);
    res.status(500).json({ message: error.message });
  }
};

exports.select_school = async (req, res) => {
  const { school_id } = req.body;

  try {
    req.user["parent_school"] = school_id;
    res.status(200).json({ message: "school selected" });
  } catch (error) {
    res.status(500).json({ message: "server error" });
  }
};
