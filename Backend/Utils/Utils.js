const modules = [
  "class",
  "section",
  "exams",
  "fee",
  "marksheet",
  "role",
  "staff_bio",
  "staff_salary_log",
  "student_bio",
  "student_class",
  "student_fee",
  "subject_class",
  "subject",
  "teacher_class",
  "user",
];

module.exports = { modules };
