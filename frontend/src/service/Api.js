import http from "../config/http";
import { loggedInData } from "../config/authData";

export const config = () => {
  const data = loggedInData();

  return {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${data.token}`,
      "X-School-ID": data.userDetails.parent_school || null,
    },
  };
};

export const addData = async (modelname, data) => {
  try {
    const response = await http.post(`/${modelname}`, data, config());
    return response;
  } catch (error) {
    console.log("Error while calling addData api", error.message);
    return error.response;
  }
};

export const getDataById = async (modelname, id) => {
  try {
    const response = await http.get(
      `/${modelname}/getDataById?id=${id}`,
      config()
    );
    return response.data;
  } catch (error) {
    console.log("Error", error.message);
    throw error;
  }
};

export const getDatas = async (modelname, tableState) => {
  let orderType = tableState.order === "asc" ? 1 : -1;
  if (modelname === "classRef") modelname = "class";
  if (modelname === "roleRef") modelname = "role";
  if (modelname === "sectionRef") modelname = "section";
  const {
    limit = 10,
    orderBy = "created_by",
    s_field = "",
    s_query = "",
    currentPage = 1,
    role = "",
  } = tableState;

  try {
    let response;
    if (tableState.s_query !== "") {
      response = await http.get(
        `/${modelname}?limit=${limit}&page=${currentPage}&col=${orderBy}&order=${orderType}&s_field=${s_field}&s_query=${s_query}`,

        config()
      );
    } else {
      response = await http.get(
        `/${modelname}?limit=${limit}&page=${
          currentPage + 1
        }&col=${orderBy}&order=${orderType}`,
        config()
      );
    }

    if (response?.data?.data.length > 0) {
      const parsedData = response.data.data.map(
        ({ createdAt, updatedAt, year, increment_date, dob, ...rest }) => ({
          ...rest,
          increment_date: increment_date
            ? formatDate(new Date(increment_date))
            : null,
          dob: dob ? formatDate(new Date(dob)) : null,
          year: year ? formatDate(new Date(year)) : null,
          createdAt: createdAt ? formatDate(new Date(createdAt)) : null,
          updatedAt: updatedAt ? formatDate(new Date(updatedAt)) : null,
        })
      );

      return { ...response.data, data: parsedData };
    }

    return response.data;
  } catch (error) {
    console.log("Error while calling getDatas api", error.message);
  }
};

export const editData = async (modelname, id, newData) => {
  try {
    const response = await http.patch(`/${modelname}/${id}`, newData, config());
    return response;
  } catch (error) {
    console.log("Error while calling editData api", error.message);
    throw error;
  }
};

export const deleteData = async (modelname, id) => {
  try {
    const response = await http.delete(`/${modelname}/${id}`, config());
    return response;
  } catch (error) {
    console.log("Error while calling deleteData api", error.message);
  }
};

export const bulkDeleteData = async (modelname, ids) => {
  try {
    const response = await http.delete(`/${modelname}/bulk-delete`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${loggedInData().token}`,
      },
      data: { ids },
    });
    return response;
  } catch (error) {
    console.log(error);
  }
};

export const getItemsByRole = async (modelname, role, tableState) => {
  const {
    limit = 10,
    orderBy = "created_at",
    order = "asc",
    s_field = "",
    s_query = "",
    currentPage = 1,
  } = tableState;

  const orderType = order === "asc" ? 1 : -1;

  try {
    const response = await http.get(
      `/${modelname}/by-role/${role}?limit=${limit}&page=${currentPage}&col=${orderBy}&order=${orderType}&s_field=${s_field}&s_query=${s_query}`,
      config()
    );
    return response.data;
  } catch (error) {
    console.log("Error while calling getItemsByRole api", error.message);
    throw error;
  }
};

export const checkIfDataExists = async (modelname, datas) => {
  try {
    const response = await http.get(
      `/${modelname}/ifDataExist/${JSON.stringify(datas)}`,
      config()
    );
    return response.data;
  } catch (error) {
    console.log("Error while calling checkIfDataExists api", error.message);
    throw error;
  }
};

// Function to format date to DD/MM/YYYY
const formatDate = (date) => {
  const day = date.getDate().toString().padStart(2, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const year = date.getFullYear();
  return `${month}-${day}-${year}`;
};
