import * as React from "react";
import { useEffect, useState } from "react";
// import axios from "axios";
import PopUp from "../components/customs/PopUP";
import {
  Toolbar,
  AppBar,
  CssBaseline,
  Drawer,
  Box,
  IconButton,
  TextField,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { Outlet } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import Navbar from "../components/Navbar";
import { AccountCircle, Menu, Notifications } from "@mui/icons-material";
import { useSelector } from "react-redux";
import { getDataById } from "../service/Api";

const AppLayout = () => {
  const { userDetails: user } = useSelector((state) => state.user);
  const [drawerWidth, setDrawerWidth] = React.useState(240);
  const [isOpen, setOpen] = React.useState(false);
  const [schoolData, setSchoolData] = useState("");

  const isMobile = useMediaQuery((theme) => theme.breakpoints.down("md"));

  React.useEffect(() => {
    if (isMobile) {
      setOpen(true);
      setDrawerWidth(0);
    } else {
      setDrawerWidth(240);
    }
  }, [isMobile]);

  useEffect(() => {
    const fetchSchoolData = async () => {
      try {
        const response = await getDataById("school", user.parent_school);

        setSchoolData(response.data);
      } catch (error) {
        console.error("Error fetching school data:", error);
        setSchoolData(null); // Set to null or some default value
      }
    };

    if (user.parent_school) {
      fetchSchoolData();
    } else {
      setSchoolData(null);
    }
  }, [user.parent_school]);

  const handleDrawerToggle = () => {
    setOpen(!isOpen);
  };
  const [ButtonPopup, setButtonPopup] = useState(false);
  return (
    <div className="flex">
      <CssBaseline />

      <Navbar
        isOpen={isOpen}
        drawerWidth={drawerWidth}
        handleDrawerToggle={handleDrawerToggle}
        schoolData={schoolData}
      />
      <Drawer
        className="w-60 flex-shrink-0"
        classes={{
          paper: "w-60 box-border",
        }}
        variant={isMobile ? "temporary" : "permanent"}
        anchor="left"
        open={isOpen}
        onClose={handleDrawerToggle}
      >
        <Sidebar />
      </Drawer>
      <main className="flex-grow bg-gray-100 h-full p-3">
        <Toolbar />
        <Outlet />
      </main>
    </div>
  );
};

export default AppLayout;
