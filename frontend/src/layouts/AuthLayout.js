import React from "react";
import { Outlet } from "react-router-dom";
import { Typography } from "@mui/material";
const { Title } = Typography;

const AuthLayout = () => {
    return (
        <>
            <Outlet />
        </>
    );
};

export default AuthLayout;
