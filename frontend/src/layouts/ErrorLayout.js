import React from "react";
import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";

export default function ErrorLayout() {
  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          background: "white",
          textAlign: "center",
          fontSize: 60,
          p: "2rem 5rem",
        }}
      >
        <Outlet />
      </Box>
    </Box>
  );
}
