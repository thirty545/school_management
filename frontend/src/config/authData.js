export const loggedInData = () => {
  const userData = localStorage.getItem("userData");

  if (!userData) {
    // Return a default object with empty values
    return {
      isLoggedIn: false,
      userDetails: {},
      token: "",
    };
  }

  return JSON.parse(userData);
};

export const setLoggedInData = () => {};