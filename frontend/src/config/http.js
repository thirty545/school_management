import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const http = axios.create({
  baseURL: `${process.env.REACT_APP_URL}/api/${process.env.REACT_APP_VERSION}`,
});

const AxiosInterceptor = ({ children }) => {
  const navigate = useNavigate();

  useEffect(() => {
    const resInterceptor = (response) => {
      return response;
    };

    const errInterceptor = (error) => {
      if (
        error?.response?.status === 401 ||
        error?.response?.status === 402 ||
        error?.response?.status === 403 ||
        error?.response?.status === 404 ||
        error?.response?.status === 500
      ) {
        console.log(error);
        // Instead of navigating, we'll return a rejected promise
        return Promise.reject(error);
      }
      return Promise.reject(error);
    };

    const interceptor = http.interceptors.response.use(
      resInterceptor,
      errInterceptor
    );

    return () => http.interceptors.response.eject(interceptor);
  }, [navigate]);

  return children;
};

export default http;
export { AxiosInterceptor };
