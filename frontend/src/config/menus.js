import {
  Book,
  LightMode,
  Logout,
  People,
  Settings,
  Voicemail,
} from "@mui/icons-material";
import { ReactComponent as BudgetCalculator } from "../assets/estimate.svg";
import { ReactComponent as GraduationCap } from "../assets/GraduationCap.svg";
import { ReactComponent as SubBook } from "../assets/subBook.svg";
import { ReactComponent as User } from "../assets/user.svg";
import { ReactComponent as Exam } from "../assets/exam.svg";
import { ReactComponent as Setting } from "../assets/setting.svg";
import { ReactComponent as Class1 } from "../assets/class.svg";
import { ReactComponent as Admissions } from "../assets/Admissions.svg";
import { PiHandCoins } from "react-icons/pi";
import { TbReceiptRupee } from "react-icons/tb";
import { LiaChalkboardTeacherSolid } from "react-icons/lia";
import { PiStudentThin } from "react-icons/pi";
import { LuCalendarClock } from "react-icons/lu";
import { AiOutlineSchedule } from "react-icons/ai";
import { TbLayoutDashboardFilled } from "react-icons/tb";
import { PiExamLight } from "react-icons/pi";
import { sub } from "date-fns";
import { style } from "@mui/system";

export const menus = [
  {
    name: "Dashboard",
    icon: (
      <TbLayoutDashboardFilled
        color="#fff"
        size={30}
        strokeWidth={1}
        stroke="black"
        className="mr-[0.7rem]"
      />
    ),
    link: "/",
  },
  {
    name: "Account",
    icon: <BudgetCalculator className="mr-3" />,
    link: "/account",
    submenus: [
      {
        name: "Fee",
        icon: <PiHandCoins className="mr-2" size={30} />,
        link: "fee",
      },
      {
        name: "Category",
        icon: <PiHandCoins className="mr-2" size={30} />,
        link: "category",
      },
      {
        name: "Fee Class",
        icon: <PiHandCoins className="mr-2" size={30} />,
        link: "feeClass",
      },
      {
        name: "Bills",
        icon: <TbReceiptRupee size={30} className="mr-2" strokeWidth={1} />,
        link: "Bills",
      },
    ],
  },

  {
    name: "Academy",
    icon: <GraduationCap className="mr-[0.4rem]" />,
    link: "/academy",
    submenus: [
      {
        name: "Admission",
        icon: <Admissions className="mr-2" size={30} />,
        link: "admission",
      },
      {
        name: "Class",
        icon: <Class1 className="mr-2" size={30} />,
        link: "class",
      },
      {
        name: "Section",
        icon: <People className="mr-2" />,
        link: "section",
      },
      {
        name: "Class Subject Teacher",
        icon: <Book className="mr-2 focus:mr-0" />,
        link: "ClassSubTeacher",
      },
      {
        name: "Class & Section",
        icon: <LightMode className="mr-2" />,
        link: "class_sec",
      },
      {
        name: "Student",
        icon: <PiStudentThin className="mr-[0.23rem]" size={30} />,
        link: "students",
      },
      {
        name: "Subjects",
        icon: <SubBook className="mr-3" size={30} />,
        link: "subject",
      },
      {
        name: "Teacher",
        icon: <LiaChalkboardTeacherSolid className="mr-[0.6rem]" size={25} />,
        link: "teacher",
      },
      {
        name: "Teaching & Sehedule",
        icon: <LuCalendarClock className="mr-3" size={23} />,
        link: "attendance",
      },
      {
        name: "Routine",
        icon: <AiOutlineSchedule className="mr-3" size={23} />,
        link: "routine",
      },
    ],
  },

  {
    name: "Users",
    icon: <User className="mr-[0.6rem]" />,
    link: "/main",
    submenus: [
      {
        name: "Users",
        icon: <People className="mr-2" />,
        link: "user",
      },
      {
        name: "Adminstrative",
        icon: <SubBook className="mr-2" />,
        link: "administrative",
      },
      {
        name: "Non-teaching,Staff",
        icon: <Voicemail className="mr-2" />,
        link: "staff",
      },
    ],
  },
  {
    name: "Examination",
    icon: <Exam className="mr-[0.85rem]" />,
    link: "/examination",
    submenus: [
      {
        name: "Exam",
        icon: <PiExamLight className="mr-2" size={23} />,
        link: "exam",
      },
      {
        name: "Exam,Routine ",
        icon: <Book className="mr-2" />,
        link: "addexam",
      },
      {
        name: "Marks entry",
        icon: <Voicemail className="mr-2" />,
        link: "marks",
      },
      {
        name: "Grade",
        icon: <Book className="mr-2" />,
        link: "grade",
      },
      {
        name: "Result",
        icon: <Book className="mr-2" />,
        link: "result",
      },
      {
        name: "Attendence",
        icon: <Book className="mr-2" />,
        link: "attendence",
      },
    ],
  },
  {
    name: "Settings",
    icon: <Setting className="mr-2" />,
    link: "/settings",
    submenus: [
      {
        name: "Role",
        icon: <Settings className="mr-2" />,
        link: "role",
      },
      {
        name: "profile",
        icon: <Book className="mr-2" />,
        link: "addfee",
      },
      {
        name: "Academy Year",
        icon: <Voicemail className="mr-2" />,
        link: "updatefee",
      },
    ],
  },
  {
    name: "Logout",
    icon: <Logout className="mr-[1.01rem]" />,
    link: "/logout",
  },
];
