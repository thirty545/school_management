export const userColumns = [
  { field: "fname", label: "First Name" },
  { field: "lname", label: "Last Name" },
  { field: "email", label: "Email" },
  { field: "phone", label: "Phone" },
  { field: "gender", label: "gender" },
  { field: "dob", label: "Date of birth" },
  { field: "address", label: "Address" },
];

export const studentColumns = [
  { field: "user", label: "Name of Students" },
  { field: "parents_name", label: "Guardians Name" },
  {
    field: "parents_phone",
    label: "Phone Number",
  },
];

export const classColumns = [
  { field: "grade", label: "Grade" },
  { field: "section", label: "Section" },
];

export const examColumns = [
  { field: "name", label: "Exam name" },
  { field: "year", label: "Exam year" },
];

export const feeColumns = [
  { field: "fee_name", label: "Fee Name" },
  { field: "classRef", label: "Class" },
  { field: "fee_amount", label: "Fee Amount" },
  { field: "increment_date", label: "Increment Date" },
];

export const subjectColumns = [{ field: "name", label: "Subject Name" }];

export const roleColumns = [{ field: "name", label: "Role name" }];
export const marksheetColumns = [
  { field: "class", label: "Class" },
  { field: "user", label: " Name of Student" },
  {
    field: "obtained_marks",
    label: "Obtained Marks",
  },
  { field: "pass_marks", label: "Pass Marks" },
  { field: "full_marks", label: "Full Marks" },
  { field: "exams", label: "Exam" },
];
export const modulesColumns = [
  { field: "module_name", label: "Module name" },
];
export const studentClassColumns = [
  { field: "StudentBio", label: " Student ID" },
  { field: "class_id", label: "Class ID" },
];
export const staffSalaryLogColumns = [
  { field: "user", label: "Staff Name" },
  { field: "isActive", label: "Active" },
  { field: "basic_salary", label: "Basic Salary" },
  { field: "joined_date", label: "Joined Date" },
  { field: "increment_date", label: "Inrement Date" },
  { field: "allowance", label: "Allowance" },
  { field: "paid_salary", label: "Paid Salary" },
  { field: "payment_date", label: "Payment Date" },
  { field: "description", label: "description" },
];

export const studentFeeColumns = [
  { field: "user", label: " Student ID" },
  { field: "fee", label: "Fee ID" },
  {
    field: "paid_fee",
    label: "Fee Paid",
  },
  { field: "payment_date", label: "Payment Date" },
];