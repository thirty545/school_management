import { Routes, Route, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { AxiosInterceptor } from "./config/http";

// Layout
import AppLayout from "./layouts/AppLayout";
import ErrorLayout from "./layouts/ErrorLayout";

// Pages
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import Dashboard from "./pages/Dashboard";
import Error401 from "./pages/error/Error401";
import ExamTable from "./smsComponents/exam/ExamTable";
import RoleTable from "./smsComponents/academic/RoleTable";
import FeeTable from "./smsComponents/fee/FeeTable";
import FeeCategoryTable from "./smsComponents/fee/categoryTable";
import FeeClassTable from "./smsComponents/fee/feeClassTable";
import AccountTable from "./smsComponents/fee/accountTable";
import Marksheet from "./smsComponents/exam/form/marksheet";
import ClassTable from "./smsComponents/academic/ClassTable";
import ClassAndSectionTable from "./smsComponents/academic/Class_secTable";
import SubjectTable from "./smsComponents/academic/SubjectTable";
import UsersTable from "./smsComponents/academic/UserTable";
import TeacherTable from "./smsComponents/academic/TeacherTable";
import Logout from "./pages/auth/Logout";
import SchoolPicker from "./pages/auth/SchoolPicker";
import AddUserForm from "./smsComponents/academic/form/user";
import SectionTable from "./smsComponents/academic/sectionTable";
import ClassForm from "./smsComponents/academic/form/class";
import ClassAndSectionForm from "./smsComponents/academic/form/class_sec";
import SectionForm from "./smsComponents/academic/form/section";
import ExamForm from "./smsComponents/exam/form/exam";
import FeeForm from "./smsComponents/fee/form/fee";
import RoleForm from "./smsComponents/academic/form/role";
import SubjectForm from "./smsComponents/academic/form/subject";
import Administrative from "./smsComponents/academic/form/Administrative";
import ClassSubTeacher from "./smsComponents/academic/subjectTeacherClass";
import StudentTable from "./smsComponents/academic/StudentTable";
import AdmissionForm from "./smsComponents/academic/form/admission";
import Payment from "./pages/auth/Payment";

const App = () => {
  const isLoggedIn = useSelector((state) => state.user?.isLoggedIn);

  return (
    <AxiosInterceptor>
      <Routes>
        <Route
          path="/"
          element={isLoggedIn ? <AppLayout /> : <Navigate to="/login" />}
        >
          <Route index element={<Dashboard />} />

          <Route path="/main">
            <Route path="user">
              <Route index element={<UsersTable />} />
              <Route path="add" element={<AddUserForm />} />
            </Route>
            <Route path="administrative"></Route>
          </Route>

          <Route path="/account">
            <Route path="fee">
              <Route index element={<FeeTable />} />
            </Route>
            <Route path="category">
              <Route index element={<FeeCategoryTable />} />
            </Route>
            <Route path="FeeClass">
              <Route index element={<FeeClassTable />} />
            </Route>
            <Route path="Bills">
              <Route index element={<AccountTable />} />
            </Route>
          </Route>

          <Route path="/academy">
            <Route index element={<ClassTable />} />
            <Route path="add" element={<ClassForm />} />
            <Route path="classsubteacher" element={<ClassSubTeacher />} />
            <Route path="class">
              <Route index element={<ClassTable />} />
              <Route path="add" element={<ClassForm />} />
              <Route path="edit/:id" element={<ClassForm />} />
            </Route>
            <Route path="section">
              <Route index element={<SectionTable />} />
              <Route path="add" element={<SectionForm />} />
              <Route path="edit/:id" element={<SectionForm />} />
            </Route>
            <Route path="class_sec">
              <Route index element={<ClassAndSectionTable />} />
              <Route path="add" element={<ClassAndSectionForm />} />
              <Route path="edit/:id" element={<ClassAndSectionForm />} />
            </Route>
            <Route path="subject">
              <Route index element={<SubjectTable />} />
              <Route path="add" element={<SubjectForm />} />
            </Route>
            <Route path="students">
              <Route index element={<StudentTable />} />
            </Route>
            <Route path="student">
              <Route path="student/admission" element={<AdmissionForm />} />
            </Route>
            <Route path="teacher">
              <Route index element={<TeacherTable />} />
            </Route>
          </Route>

          <Route path="/examination">
            <Route path="exam">
              <Route index element={<ExamTable />} />
              <Route path="add" element={<ExamForm />} />
            </Route>
            <Route path="marks">
              <Route index element={<Marksheet />} />
            </Route>
            <Route path="result">
              <Route index element={<Marksheet />} />
            </Route>
          </Route>

          <Route path="/settings">
            <Route path="role">
              <Route index element={<RoleTable />} />
              <Route path="add" element={<RoleForm />} />
            </Route>
          </Route>

          <Route path="/error" element={<ErrorLayout />}>
            <Route path="401" element={<Error401 />} />
          </Route>

          <Route path="*" element={<>PageNot Found</>} />
        </Route>

        <Route path="/login" element={<Login />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Payment" element={<Payment />} />
        <Route path="/logout" element={<Logout />} />
        <Route path="/seed" element={<SchoolPicker />} />
      </Routes>
    </AxiosInterceptor>
  );
};

export default App;
