import { createTheme } from "@mui/material";

const theme = createTheme({
  components: {
    MuiButton: {
      styleOverrides: {
        contained: {
          boxShadow: "none",
        },
      },
    },
  },
});

export default theme;
