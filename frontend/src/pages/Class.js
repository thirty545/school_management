import CustomTable from "../components/customs/CustomTable";
import { classColumns } from "../config/tableValues";
const Users = () => {
  return <CustomTable formName="class" columns={classColumns} />;
};

export default Users;
