import React from "react";
import { staffSalaryLogColumns } from "../config/tableValues";
import CustomTable from "../components/customs/CustomTable";

const Student_class = () => {
  return <CustomTable formName="staff_salary_log" columns={staffSalaryLogColumns} />;
};

export default Student_class;