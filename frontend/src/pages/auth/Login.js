import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { login } from "../../redux/reducers/userSlice";
import { useNavigate, Link as NavLink } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";
import http from "../../config/http";
import { FormControl, useFormControlContext } from "@mui/base/FormControl";
import { Autocomplete, TextField } from "@mui/material";
import loginimg from "../../assets/img/loginimg.png";
import eye from "../../assets/eye.svg";
import hide from "../../assets/hide-eye.svg";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

const Label = ({ children, htmlFor }) => {
  const formControlContext = useFormControlContext();
  const { error, required } = formControlContext;

  return (
    <label
      htmlFor={htmlFor}
      className={`block mb-2 ${error ? "text-red-500" : "text-gray-700"} ${
        required ? 'after:content-["*"] after:ml-0.5 after:text-red-500' : ""
      }`}
    >
      {children}
    </label>
  );
};

const validationSchema = Yup.object().shape({
  school: Yup.object().nullable().required("School is required"),
  email: Yup.string().required("Email or Phone is required"),
  password: Yup.string().required("Password is required"),
});

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [schools, setSchools] = useState([]);
  const [errorMessage, setErrorMessage] = useState("");

  const {
    control,
    handleSubmit,
    formState: { errors },
    setError,
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  useEffect(() => {
    fetchSchools();
  }, []);

  const fetchSchools = async () => {
    try {
      const response = await http.get("/auth/schools", {
        params: {
          page: 1,
          limit: 1000,
          col: "name",
          order: 1,
        },
      });
      setSchools(response.data.data);
    } catch (error) {
      console.error("Error fetching school data:", error);
      toast.error("Failed to fetch schools", toastConfig);
    }
  };

  const onSubmit = async (data) => {
    try {
      setErrorMessage("");
      let res = await http.post("/auth/login", data);
      console.log(res);
      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        const userData = {
          isLoggedIn: true,
          userDetails: res.data?.userDetails,
          token: res.data?.session,
        };
        dispatch(login(userData));
        localStorage.userData = JSON.stringify(userData);

        if (res.data?.userDetails?.role_name === "superadmin")
          return navigate("/choose");
        navigate("/");
        // Only reset the form on successful login
      } else {
        console.log("Error Occurred ", res.response.data.message);
        setErrorMessage(res.response.data.message || "Login failed");
      }
    } catch (error) {
      console.log("Error Occurred ", error);

      if (error.response) {
        if (error.response.status === 401) {
          setErrorMessage(
            error.response?.data?.message || "Invalid credentials"
          );
        } else if (error.response.status === 404) {
          setErrorMessage(error.response?.data?.message || "User not found");
        } else if (error.response.status === 500) {
          setErrorMessage("Server error. Please try again later.");
        } else {
          setErrorMessage(error.response?.data?.message || "An error occurred");
        }
      } else {
        setErrorMessage("Unable to connect to the server");
      }
    }
  };

  function handleClick() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
      document.getElementById("pass-icon").src = eye;
    } else {
      x.type = "password";
      document.getElementById("pass-icon").src = hide;
    }
  }

  return (
    <>
      <div className="flex items-center h-screen">
        <div className="hidden md:flex items-center justify-center w-3/5  ">
          <img className="w-4/5 h-screen " src={loginimg} alt="" />
        </div>
        <div className="w-full md:w-1/2 h-full bg-gradient-to-b from-[#c6acdf] to-[#f5eaea] flex md:items-center justify-center">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col justify-center items-center px-3 lg:px-10 mx-auto gap-6"
          >
            <div className="flex justify-center items-center">
              <h2 className="text-4xl font-bold underline decoration-[#5d2e8e]">
                Sign in
              </h2>
            </div>
            <div className="bg-white w-[425px]  px-10 py-5 rounded-lg">
              {errorMessage && (
                <div className="text-red-500 mb-1">{errorMessage}</div>
              )}
              <FormControl className="space-y-6">
                <div>
                  <Label htmlFor="school">Your School</Label>
                  <Controller
                    name="school"
                    control={control}
                    defaultValue={null}
                    render={({ field }) => (
                      <Autocomplete
                        {...field}
                        options={schools}
                        getOptionLabel={(option) => option.name}
                        isOptionEqualToValue={(option, value) =>
                          option._id === value._id
                        }
                        renderInput={(params) => (
                          <TextField
                            {...params}
                            placeholder="Select a school"
                            error={!!errors.school}
                            className="w-full"
                            sx={{
                              "& .MuiInputBase-root": {
                                height: "46px",
                                padding: "0px 9px",
                                border: errors.school
                                  ? "2px solid #ef4444"
                                  : "2px solid black",
                                borderRadius: "5px",
                              },
                              "& .MuiOutlinedInput-notchedOutline": {
                                border: "none",
                              },
                              "& .MuiInputBase-root.Mui-focused": {
                                border: errors.school
                                  ? "2px solid #ef4444"
                                  : "2px solid black",
                              },
                            }}
                          />
                        )}
                        onChange={(_, value) => field.onChange(value)}
                      />
                    )}
                  />
                  {errors.school && (
                    <p className="text-red-500 text-sm mt-1">
                      {errors.school.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="email">Email or Phone</Label>
                  <Controller
                    name="email"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <input
                        {...field}
                        type="text"
                        id="email"
                        autoComplete="username"
                        className={`w-full px-3 py-2 h-[46px] border-2 rounded-md focus:outline-none ${
                          errors.email ? "border-red-500" : "border-black"
                        }`}
                      />
                    )}
                  />
                  {errors.email && (
                    <p className="text-red-500 text-sm mt-1">
                      {errors.email.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="password">Password</Label>
                  <div className="flex items-center justify-end">
                    <Controller
                      name="password"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <input
                          {...field}
                          type="password"
                          id="password"
                          autoComplete="current-password"
                          className={`w-full px-3 py-2 h-[46px] border-2 rounded-md focus:outline-none ${
                            errors.password ? "border-red-500" : "border-black"
                          }`}
                        />
                      )}
                    />
                    <img
                      src={eye}
                      id="pass-icon"
                      onClick={handleClick}
                      className="absolute px-3"
                      alt=""
                    />
                  </div>
                  {errors.password && (
                    <p className="text-red-500 text-sm mt-1">
                      {errors.password.message}
                    </p>
                  )}
                  <div className="flex justify-between gap-1 text-xs my-3">
                    <div className="flex items-center">
                      <input className="text-base" type="checkbox" />
                      &nbsp;<p>Remember me</p>
                    </div>
                    <a className="text-[#5d2e8e] font-bold" href="#">
                      Forgot password?
                    </a>
                  </div>
                </div>
              </FormControl>
              <div className="flex justify-center mt-4">
                <button
                  type="submit"
                  className="bg-[#5d2e8e] py-2 rounded-3xl text-white px-9 hover:bg-[#4d2676]"
                >
                  Login
                </button>
              </div>
              <div className="flex justify-center">
                <p className="mt-4 text-xs font-light">
                  Don't have an account?
                  <NavLink
                    to="/register"
                    className="text-[#5d2e8e] ml-1 hover:underline"
                  >
                    Sign Up
                  </NavLink>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
