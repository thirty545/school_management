import React, { useState, useEffect } from "react";
import { Autocomplete, TextField } from "@mui/material";
import http from "../../config/http";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";
import payimg from "../../assets/img/payment.png";
import esewa from "../../assets/img/esewa.png";
import khalti from "../../assets/img/khalti.png";

const PaymentForm = () => {
  const [schools, setSchools] = useState([]);
  const [selectedSchool, setSelectedSchool] = useState(null);
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");

  useEffect(() => {
    fetchSchools();
  }, []);

  const fetchSchools = async () => {
    try {
      const response = await http.get("/auth/schools", {
        params: {
          page: 1,
          limit: 1000,
          col: "name",
          order: 1,
        },
      });
      setSchools(response.data.data);
    } catch (error) {
      console.error("Error fetching school data:", error);
      toast.error("Failed to fetch schools", toastConfig);
    }
  };

  return (
    <>
      <div className="font-poppins w-screen h-screen">
        <div className="flex justify-center h-full">
          <img className="w-3/5 object-contain" src={payimg} alt="payment" />
          <div className="flex flex-col w-6/12 justify-center items-center bg-gradient-to-b from-[rgba(198,172,223,1)] to-[#f5ecec] p-10">
            <h1 className="text-4xl font-bold underline pb-10 decoration-[#5d2e8e]">
              Payment
            </h1>
            <div className="p-10 w-[350px] bg-white px-10 py-10 rounded-lg">
              <div className="flex flex-col gap-4">
                <div className="flex flex-col gap-2">
                  <p>School Name</p>
                  <Autocomplete
                    options={schools}
                    getOptionLabel={(option) => option.name}
                    isOptionEqualToValue={(option, value) =>
                      option._id === value._id
                    }
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        placeholder="Select a school"
                        className="w-full"
                        sx={{
                          "& .MuiInputBase-root": {
                            height: "46px",
                            padding: "0px 9px",
                            border: "2px solid black",
                            borderRadius: "5px",
                          },
                          "& .MuiOutlinedInput-notchedOutline": {
                            border: "none",
                          },
                          "& .MuiInputBase-root.Mui-focused": {
                            border: "2px solid black",
                          },
                        }}
                      />
                    )}
                    onChange={(_, value) => setSelectedSchool(value)}
                    value={selectedSchool}
                  />
                </div>
                <div className="flex flex-col gap-2">
                  <p>Email</p>
                  <input
                    className="w-full px-3 py-2 border-2 border-black rounded-md focus:outline-none"
                    type="text"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="flex flex-col gap-2">
                  <p>Phone Number</p>
                  <input
                    className="w-full px-3 py-2 border-2 border-black rounded-md focus:outline-none"
                    type="text"
                    placeholder="98XXXXXXXX"
                    value={phoneNumber}
                    onChange={(e) => setPhoneNumber(e.target.value)}
                  />
                </div>
              </div>
              <div className="mt-4 mx-14">
                <h1 className="flex justify-center font-medium text-sm">
                  Payment Method
                </h1>
                <div className="flex items-center justify-center">
                  <img className="w-20 h-8" src={khalti} alt="" />
                  <img className="w-24 h-14" src={esewa} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default PaymentForm;
