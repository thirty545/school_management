import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function Logout() {
  const navigate = useNavigate();
  useEffect(() => {
    localStorage.removeItem("userData");
    navigate("/login");
  });

  return <div>Logging Out</div>;
}
