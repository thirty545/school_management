import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate, Link as NavLink } from "react-router-dom";
import * as Yup from "yup";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastConfig } from "../../config/toastConfig";
import registerImg from "../../assets/img/registerImg.png";
import http from "../../config/http";
import eye from "../../assets/eye.svg";
import hide from "../../assets/hide-eye.svg";
import open from "../../assets/OpenEye.svg";
import close from "../../assets/CloseEye.svg";
import { FormControl, useFormControlContext } from "@mui/base/FormControl";

const Label = ({ children, htmlFor }) => {
  const formControlContext = useFormControlContext();
  const { error, required } = formControlContext;

  return (
    <label
      htmlFor={htmlFor}
      className={`block mb-2 ${error ? "text-red-500" : "text-gray-700"} ${
        required ? 'after:content-["*"] after:ml-0.5 after:text-red-500' : ""
      }`}
    >
      {children}
    </label>
  );
};

const HelperText = ({ children }) => {
  const formControlContext = useFormControlContext();
  const { error } = formControlContext;

  return (
    <p className={` text-sm ${error ? "text-red-500" : "text-gray-500"}`}>
      {children}
    </p>
  );
};

const SchoolRegisterForm = () => {
  const navigate = useNavigate();
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Name must be 100 characters or less")
      .required("Name is required"),
    email: Yup.string()
      .email("Enter valid email")
      .required("Email is required"),
    phone: Yup.string()
      .matches(/^[0-9+\-() ]+$/, "Phone number is not valid")
      .required("Phone number is required"),
    password: Yup.string()
      .min(8, "Password must be at least 8 characters")
      .max(20, "Password must be at most 20 characters")
      .matches(/[A-Z]/, "Password must contain at least one uppercase letter")
      .matches(/[a-z]/, "Password must contain at least one lowercase letter")
      .matches(/[0-9]/, "Password must contain at least one number")
      .matches(
        /[@$!%*?&#]/,
        "Password must contain at least one special character"
      )
      .required("Password is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = async (data) => {
    console.log(data);
    try {
      setErrorMessage("");
      const res = await http.post("/auth/register", data);

      if (res.status === 201) {
        toast.success(res.data.message, toastConfig);
        navigate("/login");
      } else {
        handleError(res.data.message || "Registration failed");
      }
    } catch (error) {
      handleError(error.response?.data?.message || "Registration failed");
    }
  };

  const handleError = (message) => {
    console.log("Error Occurred:", message);
    setErrorMessage(message);
  };

  useEffect(() => {
    if (password && confirmPassword && password !== confirmPassword) {
      setErrorMessage("Passwords don't match");
    } else {
      setErrorMessage("");
    }
  }, [password, confirmPassword]);

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleConfirmPasswordChange = (e) => {
    setConfirmPassword(e.target.value);
  };

  function handleClick() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
      document.getElementById("pass-icon").src = eye;
    } else {
      x.type = "password";
      document.getElementById("pass-icon").src = hide;
    }
  }

  function handleShow() {
    var y = document.getElementById("Confirmpassword");
    if (y.type === "password") {
      y.type = "text";
      document.getElementById("pass-eye").src = open;
    } else {
      y.type = "password";
      document.getElementById("pass-eye").src = close;
    }
  }

  return (
    <>
      <div className="flex items-center h-screen">
        <div className="hidden md:flex items-center justify-center w-3/5  ">
          <img className="w-4/5 h-screen " src={registerImg} alt="" />
        </div>
        <div className="w-full md:w-1/2 h-full bg-gradient-to-b max-sm:px-10 from-[#c6acdf] to-[#f5eaea] flex items-center justify-center">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col justify-center items-center px-3 lg:px-10 mx-auto gap-6"
          >
            <div className="flex justify-center items-center">
              <h2 className="text-4xl font-bold underline decoration-[#5d2e8e]">
                Register
              </h2>
            </div>
            <div className="bg-white w-[425px] max-md:w-[370px] max-sm:[335px] px-10 py-5 rounded-lg">
              {errorMessage && (
                <div className="w-full text-red-700 px-4 py-1" role="alert">
                  <span className="block sm:inline">{errorMessage}</span>
                </div>
              )}
              <FormControl className="space-y-2">
                <div>
                  <Label htmlFor="name">School Name</Label>
                  <Controller
                    name="name"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <input
                        {...field}
                        type="text"
                        id="name"
                        placeholder="School's Name"
                        className={`w-full px-3 py-2 h-[38px] border-2 rounded-md focus:outline-none ${
                          errors.name ? "border-red-500" : "border-black"
                        }`}
                      />
                    )}
                  />
                  {errors.name && (
                    <p className="text-red-500 text-sm">
                      {errors.name.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="email">Email</Label>
                  <Controller
                    name="email"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <input
                        {...field}
                        type="email"
                        id="email"
                        placeholder="Email"
                        className={`w-full px-3 py-2 h-[38px] border-2 rounded-md focus:outline-none ${
                          errors.email ? "border-red-500" : "border-black"
                        }`}
                      />
                    )}
                  />
                  {errors.email && (
                    <p className="text-red-500 text-sm ">
                      {errors.email.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="phone">Phone no.</Label>
                  <Controller
                    name="phone"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <input
                        {...field}
                        type="tel"
                        id="phone"
                        placeholder="Phone"
                        className={`w-full px-3 py-2 h-[38px] border-2 rounded-md focus:outline-none ${
                          errors.phone ? "border-red-500" : "border-black"
                        }`}
                      />
                    )}
                  />
                  {errors.phone && (
                    <p className="text-red-500 text-sm ">
                      {errors.phone.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="password">Password</Label>
                  <div className="flex items-center justify-end">
                    <Controller
                      name="password"
                      control={control}
                      defaultValue=""
                      render={({ field }) => (
                        <input
                          {...field}
                          type="password"
                          id="password"
                          placeholder="Password"
                          value={password}
                          onChange={(e) => {
                            field.onChange(e);
                            handlePasswordChange(e);
                          }}
                          className={`w-full px-3 py-2 h-[38px] border-2 rounded-md focus:outline-none ${
                            errors.password ? "border-red-500" : "border-black"
                          }`}
                        />
                      )}
                    />
                    <img
                      src={hide}
                      id="pass-icon"
                      onClick={handleClick}
                      className="absolute px-3"
                      alt=""
                    />
                  </div>
                  {errors.password && (
                    <p className="text-red-500 text-sm ">
                      {errors.password.message}
                    </p>
                  )}
                </div>
                <div>
                  <Label htmlFor="confirmPassword">Confirm Password</Label>
                  <div className="flex items-center justify-end">
                    <input
                      type="password"
                      id="Confirmpassword"
                      placeholder="Confirm Password"
                      value={confirmPassword}
                      onChange={handleConfirmPasswordChange}
                      className={`w-full px-3 py-2 h-[38px] border-2 rounded-md focus:outline-none ${
                        errorMessage ? "border-red-500" : "border-black"
                      }`}
                    />
                    <img
                      src={close}
                      id="pass-eye"
                      onClick={handleShow}
                      className="absolute px-3"
                      alt=""
                    />
                  </div>
                  {errorMessage && (
                    <p className="text-red-500 text-sm">{errorMessage}</p>
                  )}
                </div>
              </FormControl>
              <div className="flex justify-center mt-3 font-normal text-sm">
                <input className="pl-5" type="checkbox" required />
                <p>
                  &nbsp;I agree all statement in
                  <a className="text-[#65329b] hover:underline" href="#">
                    Terms & Conditions
                  </a>
                </p>
              </div>
              <div className="flex justify-center space-x-4 mt-4">
                <button
                  type="submit"
                  className="bg-[#65329b] py-2 rounded-3xl text-white px-14 hover:bg-[#4d2676]"
                >
                  Register
                </button>
              </div>
              <div className="flex justify-center ">
                <p className="mt-4 text-xs">
                  Already have an account?
                  <NavLink
                    to="/login"
                    className="text-[#65329b] hover:underline"
                  >
                    Login
                  </NavLink>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default SchoolRegisterForm;
