import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import {
  Paper,
  Grid,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
} from "@mui/material";
import http from "../../config/http";
import { config } from "../../service/Api";
import { loggedInData } from "../../config/authData";

const SchoolPicker = () => {
  const [schools, setSchools] = useState([]);
  const [selectedValue, setSelectedValue] = useState("");
  const navigate = useNavigate();
  const { handleSubmit } = useForm();

  useEffect(() => {
    const fetchSchools = async () => {
      try {
        const res = await http.get("/getschools", config());
        setSchools(res.data);
      } catch (error) {
        console.log("Error fetching schools: ", error);
      }
    };
    fetchSchools();
  }, []);

  const onSubmit = async () => {
    try {
      let res = await http.post(
        "/auth/select_school",
        {
          school_id: selectedValue,
        },
        config()
      );

      if (res.status === 200) {
        let data = loggedInData();
        data.userDetails["parent_school"] = selectedValue;
        localStorage.setItem("userData", JSON.stringify(data));
        navigate("/");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Paper
      elevation={1}
      style={{ padding: "20px", width: 330, margin: "30px auto" }}
    >
      <Grid container justifyContent="center">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl fullWidth>
            <InputLabel id="school-select-label">Select School</InputLabel>
            <Select
              labelId="school-select-label"
              id="school-select"
              value={selectedValue}
              label="Select School"
              onChange={(e) => setSelectedValue(e.target.value)}
            >
              {schools.map((item) => (
                <MenuItem key={item._id} value={item._id}>
                  {item.email}
                </MenuItem>
              ))}
            </Select>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              sx={{ mt: 3 }}
            >
              Continue
            </Button>
          </FormControl>
        </form>
      </Grid>
    </Paper>
  );
};

export default SchoolPicker;
