import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { Box, TextField, Button, Typography } from "@mui/material";
import { getDatas, editData, deleteData, addData } from "../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const SubjectForm = () => {
  const navigate = useNavigate();
  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Subject name is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = async (data) => {
    try {
      const res = await addData("subject", data);
      console.log(res);
      if (res.statusText === "OK") {
        toast.success("Subject added successfully", toastConfig);
        navigate("/subjects");
      } else {
        toast.error("Subject already exists", toastConfig);
      }
      // Handle successful submission, e.g., show a success message
    } catch (error) {
      console.error("Error adding subject:", error);
      // Handle error, e.g., show an error message
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        Add Subject
      </Typography>
      <Controller
        name="name"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Subject Name"
            variant="outlined"
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        )}
      />
      <Button type="submit" variant="contained" color="primary">
        Add Subject
      </Button>
    </Box>
  );
};

export default SubjectForm;
