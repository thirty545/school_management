import CustomTable from "../components/customs/CustomTable";
import { userColumns } from "../config/tableValues";

const Teacher = () => {
    return (
        <>
            <CustomTable formName="staff_bio" columns={userColumns} />
        </>
    );
};

export default Teacher;
