import CustomTable from "../components/customs/CustomTable";
import { roleColumns } from "../config/tableValues";

const Role = () => {
  return (
    <>
      <CustomTable formName="role" columns={roleColumns} />
    </>
  );
};
export default Role;
