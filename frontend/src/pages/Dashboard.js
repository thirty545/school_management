import React from "react";
import CustomCard from "../components/customs/CustomCard";
import { Grid, Typography } from "@mui/material";

export default function Dashboard() {
  const options = {
    weekday: "short",
    year: "numeric",
    month: "short",
    day: "2-digit",
  };
  const newDate = new Date();
  const formattedDate = newDate.toLocaleDateString("en-US", options);

  return (
    <>
      <Typography variant="h5">Dashboard</Typography>
      <Typography variant="body1" sx={{ color: "grey" }}>
        {formattedDate}
      </Typography>
      <Grid container spacing={2} sx={{ mt: 1 }}>
        <Grid item>
          <CustomCard />
        </Grid>
        <Grid item>
          <CustomCard />
        </Grid>

        <Grid item>
          <CustomCard />
        </Grid>
        <Grid item>
          <CustomCard />
        </Grid>
      </Grid>
    </>
  );
}
