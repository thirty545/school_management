import { Error } from "@mui/icons-material";
import { Button, Typography } from "@mui/material";
import React from "react";
import { useLocation, useNavigate } from "react-router-dom";

export default function Error401() {
  const navigate = useNavigate();
  const location = useLocation();
  const { status, statusText, error, message } = location.state?.data;

  return (
    <>
      <Error fontSize="inherit" color="error" />
      <Typography variant="h3" component="h1" color="error" gutterBottom>
        {statusText}
      </Typography>
      <Typography variant="body1">{error}</Typography>
      <Typography variant="body1">Message: {message}</Typography>
      {status === 403 ||
        (status === 500 && (
          <Button
            variant="outlined"
            color="error"
            onClick={() => navigate("/")}
          >
            Go to Home
          </Button>
        ))}
      {status === 401 && (
        <Button
          variant="outlined"
          color="error"
          onClick={() => navigate("/login")}
        >
          Login
        </Button>
      )}
    </>
  );
}
