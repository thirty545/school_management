import React from "react";
import { studentClassColumns } from "../config/tableValues";
import CustomTable from "../components/customs/CustomTable";

const Student_class = () => {
  return <CustomTable formName="student_class" columns={studentClassColumns} />;
};

export default Student_class;