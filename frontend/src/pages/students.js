import CustomTable from "../components/customs/CustomTable";
import { studentColumns } from "../config/tableValues";

const Students = () => {
  return (
    <>
      <CustomTable formName="student_bio" columns={studentColumns} />
    </>
  );
};

export default Students;
