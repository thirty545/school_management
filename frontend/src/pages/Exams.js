import CustomTable from "../components/customs/CustomTable";
import { examColumns } from "../config/tableValues";

const Exams = () => {
  return (
    <>
      <CustomTable formName="exams" columns={examColumns} />
    </>
  );
};

export default Exams;
