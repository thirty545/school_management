import React from "react";
import { studentFeeColumns } from "../config/tableValues";
import CustomTable from "../components/customs/CustomTable";

const Student_Fee = () => {
  return <CustomTable formName="student_fee" columns={studentFeeColumns} />;
};

export default Student_Fee;