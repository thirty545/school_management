import CustomTable from "../components/customs/CustomTable";
import { userColumns } from "../config/tableValues";

const Users = () => {
  return (
    <>
      <CustomTable formName="user" columns={userColumns} />
    </>
  );
};

export default Users;
