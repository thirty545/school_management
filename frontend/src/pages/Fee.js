import CustomTable from "../components/customs/CustomTable";
import { feeColumns } from "../config/tableValues";

const Fee = () => {
  return (
    <>
      <CustomTable formName="fee" columns={feeColumns} />
    </>
  );
};

export default Fee;
