import React from "react";
import ReactDOM from "react-dom/client";
import Store from "./redux/stores";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import { AxiosInterceptor } from "./config/http";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import App from "./App";
import theme from "./theme";
import "./index.css";
import { TableProvider } from "./context/tableContext";

const queryClient = new QueryClient();
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Provider store={Store}>
          <TableProvider>
            <AxiosInterceptor>
              <QueryClientProvider client={queryClient}>
                <App />
                <ToastContainer />
                <ReactQueryDevtools initialIsOpen={false} />
              </QueryClientProvider>
            </AxiosInterceptor>
          </TableProvider>
        </Provider>
      </ThemeProvider>
    </BrowserRouter>
  </React.StrictMode>
);
