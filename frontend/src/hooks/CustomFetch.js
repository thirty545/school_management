import { useState, useEffect, useCallback } from "react";
import { getDatas } from "../service/Api";

const useFetchStudents = ({
  rowsPerPage,
  page,
  orderBy,
  order,
  searchBy,
  searchField,
  searchClass,
  searchSection,
}) => {
  const [students, setStudents] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const fetchStudents = useCallback(async () => {
    setLoading(true);
    setError(null);
    console.log("fetchStudents called");
    try {
      const { data, totalPages } = await getDatas("student_class", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: searchBy,
        s_query: searchField,
        class: searchClass,
        section: searchSection,
      });
      console.log("data", data);
      setStudents(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching students:", error);
      setError("Failed to fetch students.");
    } finally {
      setLoading(false);
    }
  }, [
    rowsPerPage,
    page,
    orderBy,
    order,
    searchBy,
    searchField,
    searchClass,
    searchSection,
  ]);

  return { students, totalPages, loading, error, fetchStudents };
};

export default useFetchStudents;
