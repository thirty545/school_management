import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { Box, Button, Typography } from "@mui/material";
import { addData } from "../../../service/Api";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastConfig } from "../../../config/toastConfig";
import { ADToBS } from "bikram-sambat-js";

const ExamForm = ({ Exam, onSave, onClose }) => {
  const navigate = useNavigate();

  const getCurrentYearBS = () => {
    const todayAD = new Date();
    const todayBS = ADToBS(todayAD);
    return parseInt(todayBS.split("-")[0], 10);
  };

  const currentYearBS = getCurrentYearBS();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Name must be 100 characters or less")
      .required("Name is required"),

    year: Yup.string()
      .required("Year is required")
      .matches(/^\d{4}$/, "Enter a valid year")
      .test("is-valid-year", "Year must not be in the future", (value) => {
        if (!value) return false;
        const inputYearBS = parseInt(value, 10);
        return inputYearBS <= currentYearBS;
      }),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = async (data) => {
    try {
      const res = await addData("exam", data);
      if (res.statusText === "OK") {
        toast.success("Exam added successfully", toastConfig);
        onSave(); // Call this callback to trigger a re-render in the parent component
        navigate("/examination/exam");
      } else {
        toast.error("Failed to add exam", toastConfig);
      }
    } catch (error) {
      console.error("Error adding exam:", error);
      toast.error("Failed to add exam", toastConfig);
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        Add Exam
      </Typography>
      <div>
        <label htmlFor="exam name">Exam Name</label>
        <Controller
          name="name"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <div>
              <input
                {...field}
                type="text"
                placeholder="Exam Name"
                className={`w-full px-3 py-2 border-2 rounded-md focus:outline-none bg-transparent placeholder-gray-800 ${
                  errors.name ? "border-red-500" : "border-black"
                }`}
              />
              {errors.name && (
                <p className="mt-1 text-sm text-red-500">
                  {errors.name.message}
                </p>
              )}
            </div>
          )}
        />
      </div>

      <div>
        <label htmlFor="Year">Year</label>
        <Controller
          name="year"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <div>
              <input
                {...field}
                type="text"
                placeholder="Year"
                className={`w-full px-3 py-2 border-2 rounded-md focus:outline-none bg-transparent placeholder-gray-800 ${
                  errors.name ? "border-red-500" : "border-black"
                }`}
                maxLength={4}
                pattern="\d{4}"
              />
              {errors.year && (
                <p className="mt-1 text-sm text-red-500">
                  {errors.year.message}
                </p>
              )}
            </div>
          )}
        />
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {Exam ? "Update Class" : "Add Class"}
        </Button>
      </Box>
    </Box>
  );
};

export default ExamForm;
