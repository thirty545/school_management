import React, { useState, useEffect } from "react";
import axios from "axios";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Paper,
  Box,
  TextField,
  MenuItem,
  Select,
  Button,
  Typography,
  FormControl,
  InputLabel,
} from "@mui/material";
import { getDatas, addData } from "../../../service/Api";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const MarksheetForm = () => {
  const navigate = useNavigate();
  const [classes, setClasses] = useState([]);
  const [users, setUsers] = useState([]);
  const [exams, setExams] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const classData = await getDatas("class", {});
        setClasses(classData.data);

        const userData = await getDatas("user", {});
        setUsers(userData.data);

        const examData = await getDatas("exam", {});
        setExams(examData.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  const validationSchema = Yup.object().shape({
    class: Yup.string().required("Class is required"),
    user: Yup.string().required("User is required"),
    obtained_marks: Yup.number()
      .required("Obtained Marks is required")
      .min(0, "Obtained marks must be non-negative")
      .test(
        "isObtainedMarksValid",
        "Obtained marks must be less than or equal to full marks",
        function (value) {
          const { full_marks } = this.parent;
          return value <= full_marks;
        }
      ),
    pass_marks: Yup.number()
      .required("Pass Marks is required")
      .min(0, "Pass marks must be non-negative")
      .test(
        "isPassMarksValid",
        "Pass marks must be less than or equal to full marks",
        function (value) {
          const { full_marks } = this.parent;
          return value <= full_marks;
        }
      ),
    full_marks: Yup.number()
      .required("Full Marks is required")
      .positive("Full marks must be positive"),
    exams: Yup.string().required("Exam is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = async (data) => {
    try {
      await addData("marksheet", data);
      toast.success("Marksheet added successfully", toastConfig);
      navigate("/examination/result");
      // Handle successful submission, e.g., show a success message
    } catch (error) {
      console.error("Error adding Marksheet:", error);

      // Handle error, e.g., show an error message
    }
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden", height: "100vh" }}>
      <Box
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 2,
          maxWidth: 400,
          margin: "0 auto",
          bgcolor: "white",
        }}
      >
        <Typography variant="h4" gutterBottom>
          Create Marksheet
        </Typography>
        <Controller
          name="class"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <FormControl fullWidth>
              <InputLabel>Class</InputLabel>
              <Select {...field} label="Class" error={!!errors.class}>
                <MenuItem value="">Select a class</MenuItem>
                {classes.map((classItem) => (
                  <MenuItem key={classItem._id} value={classItem._id}>
                    {`${classItem.grade} `}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        />
        <Controller
          name="user"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <FormControl fullWidth>
              <InputLabel>User</InputLabel>
              <Select {...field} label="User" error={!!errors.user}>
                <MenuItem value="">Select a user</MenuItem>
                {users.map((user) => (
                  <MenuItem key={user._id} value={user._id}>
                    {`${user.fname} ${user.lname}`}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        />

        <Controller
          name="obtained_marks"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              label="Obtained Marks"
              type="number"
              error={!!errors.obtained_marks}
              helperText={errors.obtained_marks?.message}
            />
          )}
        />
        <Controller
          name="pass_marks"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              label="Pass Marks"
              type="number"
              error={!!errors.pass_marks}
              helperText={errors.pass_marks?.message}
            />
          )}
        />
        <Controller
          name="full_marks"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              label="Full Marks"
              type="number"
              error={!!errors.full_marks}
              helperText={errors.full_marks?.message}
            />
          )}
        />
        <Controller
          name="exams"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <FormControl fullWidth>
              <InputLabel>Exam</InputLabel>
              <Select {...field} label="Exam" error={!!errors.exams}>
                <MenuItem value="">Select an exam</MenuItem>
                {exams.map((exam) => (
                  <MenuItem key={exam._id} value={exam._id}>
                    {exam.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          )}
        />
        <Button type="submit" variant="contained" color="primary">
          Create Marksheet
        </Button>
      </Box>
    </Paper>
  );
};

export default MarksheetForm;
