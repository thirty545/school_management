import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  Button,
  Box,
  IconButton,
} from "@mui/material";
import { getDatas, deleteData } from "../../service/Api";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import CustomPopup from "../../components/customs/PopUP";
import ExamForm from "./form/exam";

const ExamTable = () => {
  const [exams, setExams] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const navigate = useNavigate();
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedExam, setSelectedExam] = useState(null);

  const fetchExams = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("exam", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: "createdAt",
        order: -1,
        s_field: "name",
        s_query: searchField,
      });
      setExams(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching exams:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchField]);

  useEffect(() => {
    fetchExams();
  }, [fetchExams]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleEditExam = (exam) => {
    setSelectedExam(exam);
    setPopupOpen(true);
  };

  const handleDeleteExam = async (id) => {
    try {
      await deleteData("exam", id);
      fetchExams();
    } catch (error) {
      console.error("Error deleting exam:", error);
    }
  };

  const handleAddExam = () => {
    setSelectedExam(null);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedExam(null);
  };

  const handleSaveExam = async () => {
    await fetchExams();
    handleClosePopup();
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <TextField
          label="Search"
          variant="outlined"
          size="small"
          value={searchField}
          onChange={handleSearchFieldChange}
          sx={{ mr: 2 }}
        />
        <Button
          variant="contained"
          sx={{
            background: "#4a2470",
            ":hover": {
              backgroundColor: "#6a3a90",
            },
          }}
          onClick={handleAddExam}
        >
          Add Exam
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Year</TableCell>
              <TableCell>Created By</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : exams.length === 0 ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  No exams found.
                </TableCell>
              </TableRow>
            ) : (
              exams.map((exam, index) => (
                <TableRow
                  key={exam._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{exam.name}</TableCell>
                  <TableCell>
                    {exam.year ? new Date(exam.year).getFullYear() : "N/A"}
                  </TableCell>
                  <TableCell>
                    {exam.createdBy?.fname} {exam.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      sx={{ color: "blue" }}
                      onClick={() => handleEditExam(exam)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteExam(exam._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalPages * rowsPerPage}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <ExamForm
          examData={selectedExam}
          onSave={handleSaveExam}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default ExamTable;
