import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import BillForm from "./form/billForm";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const AccountTable = () => {
  const [classSearch, setClassSearch] = useState("");
  const [sectionSearch, setSectionSearch] = useState("");
  const [orderBy, setOrderBy] = useState("student");
  const [order, setOrder] = useState("asc");
  const [accounts, setAccounts] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [classes, setClasses] = useState([]);
  const [sections, setSections] = useState([]);

  // Fetch classes
  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", {
          limit: rowsPerPage,
          currentPage: page,
          orderBy: orderBy,
          order: 1,
          s_field: "name",
          s_query: "",
        });
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [rowsPerPage, page, orderBy]);

  // Fetch sections based on selected class
  useEffect(() => {
    const fetchSections = async () => {
      if (classSearch) {
        try {
          const sectionData = await getDatas("class_sec", {
            limit: rowsPerPage,
            currentPage: page,
            orderBy: orderBy,
            order: order === "asc" ? 1 : -1,
            s_field: "class",
            s_query: classSearch,
          });
          setSections(sectionData.data);
        } catch (error) {
          console.error("Error fetching sections:", error);
        }
      } else {
        setSections([]);
      }
    };
    fetchSections();
  }, [classSearch, rowsPerPage, page, orderBy, order]);

  // Fetch student accounts
  const fetchAccounts = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("student_account", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: "class_sec",
        s_query: sectionSearch,
      });
      setAccounts(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching accounts:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, orderBy, order, classSearch, sectionSearch]);

  // Handlers for table pagination, sorting, and search
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleClassSearchChange = (event) => {
    setClassSearch(event.target.value);
    setSectionSearch(""); // Reset section when class changes
  };

  const handleSectionSearchChange = (event) => {
    setSectionSearch(event.target.value);
  };

  const handleAddAccount = () => {
    setSelectedAccount(null);
    setPopupOpen(true);
  };

  const handleEditAccount = (account) => {
    setSelectedAccount((prevAccountValue) => {
      const newState = { ...prevAccountValue, ...account };
      return newState;
    });
    setPopupOpen(true);
  };

  const handlePayment = (account) => {
    setSelectedAccount((prevAccountValue) => {
      const newState = { ...prevAccountValue, payment: account.payment };
      return newState;
    });
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedAccount(null);
  };

  const handleSaveAccount = async () => {
    await fetchAccounts();
    handleClosePopup();
  };

  const handleDeleteAccount = async (id) => {
    try {
      await deleteData("student_account", id);
      toast.success("Account deleted successfully", toastConfig);
      fetchAccounts();
    } catch (error) {
      console.error("Error deleting account:", error);
      toast.error("Failed to delete account", toastConfig);
    }
  };

  useEffect(() => {
    fetchAccounts();
  }, [fetchAccounts]);

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Student Accounts
        </Typography>
        <Typography variant="body1">
          This table monitors the accounts of all students according to their
          class and section. You can search, add, edit, and manage accounts from
          here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Class"
            variant="outlined"
            size="small"
            value={classSearch}
            onChange={handleClassSearchChange}
            select
            sx={{ mr: 2, width: 100 }}
          >
            {classes.map((classItem) => (
              <MenuItem key={classItem._id} value={classItem._id}>
                {classItem.name}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            label="Section"
            variant="outlined"
            size="small"
            value={sectionSearch}
            onChange={handleSectionSearchChange}
            select
            sx={{ mr: 2, width: 100 }}
          >
            {sections.map((sectionItem) => (
              <MenuItem key={sectionItem._id} value={sectionItem._id}>
                {sectionItem.section.name}
              </MenuItem>
            ))}
          </TextField>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#5D2E8E",
              "&:hover": { backgroundColor: "#4A2470" },
            }}
            onClick={fetchAccounts}
            startIcon={<SearchIcon />}
          >
            Search
          </Button>
        </Box>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handlePayment}
        >
          Payment
        </Button>

        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddAccount}
          startIcon={<AddIcon />}
        >
          Create Bill
        </Button>
      </Box>

      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {[
                "student",
                "class",
                "section",
                "feesDue",
                "totalPaid",
                "balance",
              ].map((headCell) => (
                <TableCell key={headCell}>
                  <TableSortLabel
                    active={orderBy === headCell}
                    direction={orderBy === headCell ? order : "asc"}
                    onClick={() => handleRequestSort(headCell)}
                  >
                    {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                  </TableSortLabel>
                </TableCell>
              ))}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : (
              accounts.map((account) => (
                <TableRow key={account._id}>
                  <TableCell>{account.student.name}</TableCell>
                  <TableCell>{account.class_sec.class.name}</TableCell>
                  <TableCell>{account.class_sec.section.name}</TableCell>
                  <TableCell>{account.feesDue}</TableCell>
                  <TableCell>{account.totalPaid}</TableCell>
                  <TableCell>{account.balance}</TableCell>
                  <TableCell>
                    <IconButton
                      color="primary"
                      onClick={() => handleEditAccount(account)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      color="secondary"
                      onClick={() => handleDeleteAccount(account._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={totalPages * rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleRowsPerPageChange}
      />

      {popupOpen && (
        <CustomPopup open={popupOpen} onClose={handleClosePopup}>
          <BillForm
            account={selectedAccount}
            onSave={handleSaveAccount}
            onClose={handleClosePopup}
          />
        </CustomPopup>
      )}
    </Paper>
  );
};

export default AccountTable;
