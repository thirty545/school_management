import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  IconButton,
} from "@mui/material";
import { getDatas, editData, deleteData } from "../../service/Api";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import CustomPopup from "../../components/customs/PopUP";
import FeeForm from "./form/fee";

const FeeTable = () => {
  const [fees, setFees] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("fee_name");
  const navigate = useNavigate();
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedFee, setSelectedFee] = useState(null);

  const fetchFees = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("fee", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: "createdAt",
        order: -1,
        s_field: searchBy,
        s_query: searchField,
      });
      setFees(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching fees:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField]);

  useEffect(() => {
    fetchFees();
  }, [fetchFees, searchField, searchBy]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleEditFee = (fee) => {
    setSelectedFee(fee);
    setPopupOpen(true);
  };

  const handleDeleteFee = async (id) => {
    try {
      await deleteData("fee", id);
      fetchFees();
    } catch (error) {
      console.error("Error deleting fee:", error);
    }
  };

  const handleAddFee = () => {
    setSelectedFee(null);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedFee(null);
  };

  const handleSaveFee = async () => {
    await fetchFees();
    handleClosePopup();
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="fee_name">Fee Name</MenuItem>
            <MenuItem value="fee_amount">Fee Amount</MenuItem>
            <MenuItem value="increment_date">Increment Date</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            background: "#4a2470",
            ":hover": {
              backgroundColor: "#6a3a90",
            },
          }}
          onClick={handleAddFee}
        >
          Add Fee
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Fee Name</TableCell>
              <TableCell>Fee Amount</TableCell>
              <TableCell>Increment Date</TableCell>
              <TableCell>Class</TableCell>
              <TableCell>Created By</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : fees.length === 0 ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  No fees found.
                </TableCell>
              </TableRow>
            ) : (
              fees.map((fee, index) => (
                <TableRow
                  key={fee._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{fee.fee_name}</TableCell>
                  <TableCell>{fee.fee_amount}</TableCell>
                  <TableCell>
                    {fee.increment_date
                      ? new Date(fee.increment_date).toLocaleDateString()
                      : "N/A"}
                  </TableCell>
                  <TableCell>{fee.classRef?.name || "N/A"}</TableCell>
                  <TableCell>
                    {fee.createdBy?.fname} {fee.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      sx={{ color: "blue" }}
                      onClick={() => handleEditFee(fee)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteFee(fee._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalPages * rowsPerPage}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <FeeForm
          feeData={selectedFee}
          onSave={handleSaveFee}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default FeeTable;
