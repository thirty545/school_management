import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  IconButton,
} from "@mui/material";
import { getDatas, editData, deleteData } from "../../service/Api";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";
import CustomPopup from "../../components/customs/PopUP";
import CategoryForm from "./form/category";

const CategoryTable = () => {
  const [categories, setCategories] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("name");
  const navigate = useNavigate();
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedCategory, setSelectedCategory] = useState(null);

  const fetchCategories = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("fee_category", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: "createdAt",
        order: -1,
        s_field: searchBy,
        s_query: searchField,
      });
      setCategories(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching categories:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField]);

  useEffect(() => {
    fetchCategories();
  }, [fetchCategories, searchField, searchBy]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  // const handleEditCategory = (category) => {
  //   setSelectedCategory(category);
  //   console.log("Selected Category:", category);
  //   console.log("setted Selected Category:", selectedCategory);
  //   setPopupOpen(true);
  // };
  const handleEditCategory = (category) => {
    console.log("Selected Category:", category);
    setSelectedCategory(category); // This will trigger the useEffect once the state is updated.
  };

  useEffect(() => {
    if (selectedCategory) {
      console.log(
        "Popup should open now with selected category:",
        selectedCategory
      );
      setPopupOpen(true); // Popup opens only after selectedCategory is set.
    }
  }, [selectedCategory]);

  const handleDeleteCategory = async (id) => {
    try {
      await deleteData("fee_category", id);
      fetchCategories();
    } catch (error) {
      console.error("Error deleting category:", error);
    }
  };

  const handleAddCategory = () => {
    setSelectedCategory(null);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedCategory(null);
  };

  const handleSaveCategory = async () => {
    await fetchCategories();
    handleClosePopup();
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="name">Fee Name</MenuItem>
            <MenuItem value="amount">Fee Amount</MenuItem>
            <MenuItem value="increment_date">Increment Date</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            background: "#4a2470",
            ":hover": {
              backgroundColor: "#6a3a90",
            },
          }}
          onClick={handleAddCategory}
        >
          Add Category
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Category Name</TableCell>
              <TableCell>Payment Schedule</TableCell>
              <TableCell>Created By</TableCell>
              <TableCell>Created At</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : categories.length === 0 ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  No categories found.
                </TableCell>
              </TableRow>
            ) : (
              categories.map((category, index) => (
                <TableRow
                  key={category._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{category.name}</TableCell>
                  <TableCell>{category.payment_schedule}</TableCell>

                  <TableCell>
                    {category.createdBy?.fname} {category.createdBy?.lname}
                  </TableCell>
                  <TableCell>{category.createdAt}</TableCell>
                  <TableCell>
                    <IconButton
                      sx={{ color: "blue" }}
                      onClick={() => handleEditCategory(category)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteCategory(category._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalPages * rowsPerPage}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <CategoryForm
          CategoryData={selectedCategory}
          onSave={handleSaveCategory}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default CategoryTable;
