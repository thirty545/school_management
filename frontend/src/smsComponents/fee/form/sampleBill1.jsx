import React from "react";
import BillRequestComponent from "../billTable";
import BillComponent from "./bill";

const App = () => {
  const classes = [
    { value: "class_1", label: "Class 1" },
    { value: "class_2", label: "Class 2" },
    { value: "class_3", label: "Class 3" },
    // Add more classes as needed
  ];
  const handleBillRequest = ({
    class: selectedClass,
    month: selectedMonth,
  }) => {
    console.log("Generate bill for:", selectedClass, selectedMonth);
    // Fetch or generate bill data based on selectedClass and selectedMonth
    const billData = {
      studentName: "John Doe",
      className: selectedClass,
      rollNumber: "23",
      fees: [
        { description: `${selectedMonth} Tuition Fee`, amount: "$500" },
        { description: "Library Fee", amount: "$50" },
        { description: "Lab Fee", amount: "$100" },
      ],
      totalAmount: "$650",
      dueDate: "30th September 2024",
    };

    // Show the bill
    alert(`Bill generated for ${selectedClass} in ${selectedMonth}`);
    // You can render the BillComponent here or navigate to a new page
  };

  return (
    <div>
      <BillRequestComponent classes={classes} onSubmit={handleBillRequest} />
      {/* You can conditionally render the BillComponent here based on state */}
    </div>
  );
};

export default App;
