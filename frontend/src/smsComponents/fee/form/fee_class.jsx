import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Button,
  Typography,
  Autocomplete,
  CircularProgress,
} from "@mui/material";
import { getDatas, addData, editData, getDataById } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const FeeForm = ({ feeData, onSave, onClose }) => {
  const [classes, setClasses] = useState([]);
  const [categories, setCategories] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [initialFeeData, setInitialFeeData] = useState(null);

  const validationSchema = Yup.object().shape({
    fee_category: Yup.object().nullable().required("Fee category is required"),
    class: Yup.object().nullable().required("Class is required"),
    amount: Yup.number()
      .positive("Fee amount must be positive")
      .required("Fee amount is required"),
    increment_date: Yup.date()
      .min(new Date(), "Increment date must not be in the past")
      .required("Increment date is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialFeeData || {},
  });

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const [classData, categoryData] = await Promise.all([
          getDatas("class", {}),
          getDatas("fee_category", {}),
        ]);
        setClasses(classData.data);
        setCategories(categoryData.data);

        if (feeData) {
          const feeDataDetails = await getDataById("fee_class", feeData._id);
          setInitialFeeData(feeDataDetails.data);
          setValue("fee_category", feeDataDetails.data.fee_category);
          setValue("class", feeDataDetails.data.class);
          setValue("amount", feeDataDetails.data.amount);
          setValue(
            "increment_date",
            feeDataDetails.data.increment_date.split("T")[0]
          );
        }
      } catch (error) {
        console.error("Error fetching data:", error);
        toast.error(`Failed to fetch form data: ${error.message}`, toastConfig);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, [feeData, setValue]);

  const onSubmit = async (data) => {
    setIsLoading(true);
    try {
      const feePayload = {
        fee_category: data.fee_category._id,
        class: data.class._id,
        amount: data.amount,
        increment_date: data.increment_date,
      };
      let res;
      if (initialFeeData) {
        res = await editData("fee_class", initialFeeData._id, feePayload);
      } else {
        res = await addData("fee_class", feePayload);
      }
      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding fee class:", error);
      toast.error(
        `Failed to ${initialFeeData ? "update" : "add"} fee class: ${
          error.message
        }`,
        toastConfig
      );
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        maxWidth: 400,
        gap: 2,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        {initialFeeData ? "Edit Fee" : "Add Fee"}
      </Typography>

      <Controller
        name="fee_category"
        control={control}
        defaultValue={null}
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={categories}
            getOptionLabel={(option) => option.name || ""}
            isOptionEqualToValue={(option, value) =>
              option && value && option._id === value._id
            }
            renderInput={(params) => (
              <TextField
                {...params}
                label="Fee Category"
                error={!!errors.fee_category}
                helperText={errors.fee_category?.message}
              />
            )}
            onChange={(_, value) => field.onChange(value)}
          />
        )}
      />
      <Controller
        name="class"
        control={control}
        defaultValue={null}
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={classes}
            getOptionLabel={(option) => option.name || ""}
            isOptionEqualToValue={(option, value) =>
              option && value && option._id === value._id
            }
            renderInput={(params) => (
              <TextField
                {...params}
                label="Class"
                error={!!errors.class}
                helperText={errors.class?.message}
              />
            )}
            onChange={(_, value) => field.onChange(value)}
          />
        )}
      />

      <Controller
        name="amount"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            label="Fee Amount"
            type="number"
            error={!!errors.amount}
            helperText={errors.amount?.message}
          />
        )}
      />

      <Controller
        name="increment_date"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            type="date"
            label="Increment Date"
            InputLabelProps={{ shrink: true }}
            error={!!errors.increment_date}
            helperText={errors.increment_date?.message}
          />
        )}
      />

      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          disabled={isLoading}
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {isLoading ? (
            <CircularProgress size={24} />
          ) : initialFeeData ? (
            "Update Fee"
          ) : (
            "Add Fee"
          )}
        </Button>
      </Box>
    </Box>
  );
};

export default FeeForm;
