import React from "react";
import {
  Box,
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Divider,
  Button,
} from "@mui/material";

const BillComponent = ({ billData, onPrint }) => {
  return (
    <Box
      sx={{
        padding: 4,
        maxWidth: 600,
        margin: "0 auto",
        backgroundColor: "#fff",
        boxShadow: 3,
      }}
    >
      {/* School Name */}
      <Typography variant="h4" align="center" gutterBottom>
        School Name
      </Typography>

      {/* School Address */}
      <Typography variant="body1" align="center">
        123 Main Street, City, Country
      </Typography>
      <Typography variant="body1" align="center" gutterBottom>
        Phone: +123 456 7890
      </Typography>

      <Divider sx={{ marginY: 2 }} />

      {/* Student Information */}
      <Box sx={{ marginBottom: 2 }}>
        <Typography variant="h6">Student Information</Typography>
        <Typography variant="body1">Name: {billData.studentName}</Typography>
        <Typography variant="body1">Class: {billData.className}</Typography>
        <Typography variant="body1">Roll No: {billData.rollNumber}</Typography>
      </Box>

      {/* Fee Breakdown */}
      <TableContainer component={Paper}>
        <Table aria-label="fee breakdown">
          <TableHead>
            <TableRow>
              <TableCell>Fee Description</TableCell>
              <TableCell align="right">Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {billData.fees.map((fee, index) => (
              <TableRow key={index}>
                <TableCell>{fee.description}</TableCell>
                <TableCell align="right">{fee.amount}</TableCell>
              </TableRow>
            ))}
            <TableRow>
              <TableCell>
                <Typography variant="h6">Total</Typography>
              </TableCell>
              <TableCell align="right">
                <Typography variant="h6">{billData.totalAmount}</Typography>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>

      <Divider sx={{ marginY: 2 }} />

      {/* Payment Due Date */}
      <Box sx={{ marginBottom: 2 }}>
        <Typography variant="body1">
          Payment Due Date: {billData.dueDate}
        </Typography>
      </Box>

      {/* Print Button */}
      <Box sx={{ display: "flex", justifyContent: "center", marginTop: 3 }}>
        <Button variant="contained" color="primary" onClick={onPrint}>
          Print Bill
        </Button>
      </Box>
    </Box>
  );
};

export default BillComponent;
