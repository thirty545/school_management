import React from "react";
import BillComponent from "./bill";

const sampleBillData = {
  studentName: "John Doe",
  className: "5th Grade",
  rollNumber: "23",
  fees: [
    { description: "Tuition Fee", amount: "$500" },
    { description: "Library Fee", amount: "$50" },
    { description: "Lab Fee", amount: "$100" },
  ],
  totalAmount: "$650",
  dueDate: "30th September 2024",
};

const App = () => {
  const handlePrint = () => {
    window.print();
  };

  return <BillComponent billData={sampleBillData} onPrint={handlePrint} />;
};

export default App;
