import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Box, TextField, Button, MenuItem, Typography } from "@mui/material";
import { getDatas, addData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const CategoryForm = ({ CategoryData, onSave, onClose }) => {
  const [classSearch, setClassSearch] = useState("");
  const [classes, setClasses] = useState([]);
  const [formKey, setFormKey] = useState(0);
  const navigate = useNavigate();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Category name must be less than 100 characters")
      .required("Category name is required"),
    payment_schedule: Yup.string().required("Schedule is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: CategoryData || { name: "", payment_schedule: "" }, // Ensure defaultValues are set correctly
  });

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", classSearch);
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [classSearch]);

  useEffect(() => {
    // Reset the form when CategoryData changes
    if (CategoryData) {
      reset(CategoryData);
    }
  }, [CategoryData, reset]);

  const onSubmit = async (data) => {
    try {
      const categoryData = {
        ...data,
      };
      await addData("fee_category", categoryData);
      toast.success("Category added successfully", toastConfig);
      onSave();
      setFormKey((prevKey) => prevKey + 1);
    } catch (error) {
      console.error("Error adding category:", error);
      toast.error("Failed to add category", toastConfig);
    }
  };

  return (
    <Box
      key={formKey}
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        maxWidth: 400,
        gap: 2,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        {CategoryData ? "Edit Category" : "Add Category"}
      </Typography>

      <div>
        <label htmlFor="name">Fee Category</label>
        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              placeholder="Fee Category"
              error={!!errors.name}
              helperText={errors.name?.message}
              fullWidth
              sx={{
                "& .MuiInputBase-root": {
                  height: "46px",
                  border: errors.name ? "2px solid #ef4444" : "2px solid black",
                  borderRadius: "5px",
                },
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
              }}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="payment_schedule">Payment Schedule</label>
        <Controller
          name="payment_schedule"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              select
              placeholder="Payment Schedule"
              error={!!errors.payment_schedule}
              helperText={errors.payment_schedule?.message}
              value={field.value} // Ensure the value is correctly bound
              onChange={field.onChange}
              sx={{
                "& .MuiInputBase-root": {
                  height: "46px",
                  border: errors.payment_schedule
                    ? "2px solid #ef4444"
                    : "2px solid black",
                  borderRadius: "5px",
                },
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
                marginBottom: 2,
              }}
            >
              <MenuItem value="monthly">Monthly</MenuItem>
              <MenuItem value="quarterly">Quarterly</MenuItem>
              <MenuItem value="half-yearly">Half-Yearly</MenuItem>
              <MenuItem value="yearly">Yearly</MenuItem>
            </TextField>
          )}
        />
      </div>

      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {CategoryData ? "Update Category" : "Add Category"}
        </Button>
      </Box>
    </Box>
  );
};

export default CategoryForm;
