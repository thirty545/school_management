import React, { useState, useEffect, useCallback } from "react";
import {
  Box,
  TextField,
  MenuItem,
  Button,
  Typography,
  Paper,
  CircularProgress,
  Autocomplete,
} from "@mui/material";
import { Controller, useForm } from "react-hook-form";
import { getDatas } from "../../../service/Api";

const BillRequestComponent = ({ onSubmit = () => {} }) => {
  // Provide a default no-op function
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [selectedMonth, setSelectedMonth] = useState("");
  const [classes, setClasses] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchClasses = useCallback(async () => {
    setLoading(true);
    try {
      const response = await getDatas("class", {});
      const data = response.data;
      setClasses(data.map((cls) => ({ value: cls.id, label: cls.name })));
    } catch (error) {
      console.error("Error fetching classes:", error);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchClasses();
  }, [fetchClasses]);

  const handleMonthChange = (event) => {
    setSelectedMonth(event.target.value);
  };

  const handleFormSubmit = (data) => {
    if (data.class && selectedMonth) {
      onSubmit({ class: data.class, month: selectedMonth });
    } else {
      alert("Please select both class and month.");
    }
  };

  return (
    <Paper elevation={3} sx={{ padding: 4, maxWidth: 400, margin: "0 auto" }}>
      <Typography variant="h6" gutterBottom>
        Create Bill
      </Typography>

      <TextField
        select
        label="Select Month"
        value={selectedMonth}
        onChange={handleMonthChange}
        fullWidth
        margin="normal"
        variant="outlined"
      >
        {[
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ].map((month) => (
          <MenuItem key={month} value={month}>
            {month}
          </MenuItem>
        ))}
      </TextField>

      {loading ? (
        <Box sx={{ display: "flex", justifyContent: "center", marginTop: 2 }}>
          <CircularProgress />
        </Box>
      ) : (
        <Controller
          name="class"
          control={control}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={classes}
              getOptionLabel={(option) => option.label || ""}
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Class"
                  error={!!errors.class}
                  helperText={errors.class?.message}
                  fullWidth
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      )}

      <Box sx={{ display: "flex", justifyContent: "center", marginTop: 3 }}>
        <Button
          variant="contained"
          sx={{
            background: "#4a2470",
            ":hover": {
              backgroundColor: "#6a3a90",
            },
          }}
          onClick={handleSubmit(handleFormSubmit)}
        >
          Generate Bill
        </Button>
      </Box>
    </Paper>
  );
};

export default BillRequestComponent;
