import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Button,
  Typography,
  Autocomplete,
} from "@mui/material";
import { getDatas, addData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const FeeForm = ({ FeeData, onSave, onClose }) => {
  const [classSearch, setClassSearch] = useState("");
  const [classes, setClasses] = useState([]);
  const [formKey, setFormKey] = useState(0);
  const navigate = useNavigate();

  const validationSchema = Yup.object().shape({
    fee_name: Yup.string()
      .max(100, "Fee name must be less than 100 characters")
      .required("Fee name is required"),
    class: Yup.object().nullable().required("Class is required"),
    fee_amount: Yup.number()
      .positive("Fee amount must be positive")
      .required("Fee amount is required"),
    increment_date: Yup.date()
      .min(new Date(), "Increment date must not be in the past")
      .required("Increment date is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: FeeData || { name: "" },
  });

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", classSearch);
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [classSearch]);

  const onSubmit = async (data) => {
    try {
      const feeData = {
        ...data,
        class: {
          _id: data.class._id,
          name: data.class.name,
        },
      };
      await addData("fee", feeData);
      toast.success("Fee added successfully", toastConfig);
      onSave();
      setFormKey((prevKey) => prevKey + 1);
    } catch (error) {
      console.error("Error adding fee:", error);
      toast.error("Failed to add fee", toastConfig);
    }
  };

  return (
    <Box
      key={formKey}
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        maxWidth: 400,
        gap: 2,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        Add Fee
      </Typography>

      <div>
        <label htmlFor="fee_name">Fee name</label>
        <Controller
          name="fee_name"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              placeholder="Fee Name"
              error={!!errors.fee_name}
              helperText={errors.fee_name?.message}
              fullWidth
              sx={{
                "& .MuiInputBase-root": {
                  height: "46px",
                  border: errors.fee_name
                    ? "2px solid #ef4444"
                    : "2px solid black",
                  borderRadius: "5px",
                },
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
              }}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="class">Class</label>
        <Controller
          name="class"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={classes}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search class"
                  onChange={(e) => setClassSearch(e.target.value)}
                  error={!!errors.class}
                  helperText={errors.class?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="fee_amount">Fee Amount</label>
        <Controller
          name="fee_amount"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              placeholder="Enter fee amount"
              type="number"
              error={!!errors.fee_amount}
              helperText={errors.fee_amount?.message}
              fullWidth
              sx={{
                "& .MuiInputBase-root": {
                  height: "46px",
                  border: errors.fee_amount
                    ? "2px solid #ef4444"
                    : "2px solid black",
                  borderRadius: "5px",
                },
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
              }}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="increment_date">Date</label>
        <Controller
          name="increment_date"
          control={control}
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              type="date"
              error={!!errors.increment_date}
              helperText={errors.increment_date?.message}
              fullWidth
              sx={{
                "& .MuiInputBase-root": {
                  height: "46px",
                  border: errors.increment_date
                    ? "2px solid #ef4444"
                    : "2px solid black",
                  borderRadius: "5px",
                },
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
              }}
            />
          )}
        />
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {FeeData ? "Update Fee" : "Add Fee"}
        </Button>
      </Box>
    </Box>
  );
};

export default FeeForm;
