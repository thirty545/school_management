import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate, Link as NavLink } from "react-router-dom";
import * as Yup from "yup";
import { Box, TextField, Button, Typography } from "@mui/material";
import { addData } from "../../service/Api";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toastConfig } from "../../config/toastConfig";
import { ADToBS } from "bikram-sambat-js";
import http from "../../config/http";

const SchoolRegisterForm = () => {
  const navigate = useNavigate();

  const getCurrentYearBS = () => {
    const todayAD = new Date();
    const todayBS = ADToBS(todayAD);
    return parseInt(todayBS.split("-")[0], 10);
  };

  const currentYearBS = getCurrentYearBS();

  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Name must be 100 characters or less")
      .required("Name is required"),
    email: Yup.string()
      .email("Enter valid email")
      .required("Email is required"),
    address: Yup.string()
      .max(100, "Address must be 100 characters or less")
      .required("Address is required"),
    slogan: Yup.string()
      .max(100, "Max characters allowed is 100")
      .required("Slogan is required"),
    logo: Yup.string().required("Logo is required"),
    phone: Yup.string()
      .matches(/^[0-9+\-() ]+$/, "Phone number is not valid")
      .required("Phone number is required"),
    password: Yup.string()
      .min(8, "Password must be at least 8 characters")
      .max(20, "Password must be at most 20 characters")
      .matches(/[A-Z]/, "Password must contain at least one uppercase letter")
      .matches(/[a-z]/, "Password must contain at least one lowercase letter")
      .matches(/[0-9]/, "Password must contain at least one number")
      .matches(
        /[@$!%*?&#]/,
        "Password must contain at least one special character"
      )
      .required("Password is required"),
    estd: Yup.string()
      .required("Year is required")
      .matches(/^\d{4}$/, "Enter a valid year")
      .test("is-valid-year", "Year must not be in the future", (value) => {
        if (!value) return false;
        const inputYearBS = parseInt(value, 10);
        return inputYearBS <= currentYearBS;
      }),
  });

  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = async (data) => {
    try {
      let res = await http.post("/auth/register", data);

      if (res?.data?.code === 11000) toast.warn("Email taken", toastConfig);
      if (res.statusText === "OK") {
        toast.info(res?.data?.message, toastConfig);
        navigate("/login");
      }

      toast.warn(res?.data?.message, toastConfig);
    } catch (error) {
      console.log("Error Occured ", error);
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        Add School
      </Typography>
      <Controller
        name="name"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Name"
            variant="outlined"
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        )}
      />
      <Controller
        name="email"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Email"
            variant="outlined"
            error={!!errors.email}
            helperText={errors.email?.message}
          />
        )}
      />
      <Controller
        name="address"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Address"
            variant="outlined"
            error={!!errors.address}
            helperText={errors.address?.message}
          />
        )}
      />
      <Controller
        name="slogan"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Slogan"
            variant="outlined"
            error={!!errors.slogan}
            helperText={errors.slogan?.message}
          />
        )}
      />
      <Controller
        name="logo"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Logo"
            variant="outlined"
            error={!!errors.logo}
            helperText={errors.logo?.message}
          />
        )}
      />
      <Controller
        name="phone"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Phone"
            variant="outlined"
            error={!!errors.phone}
            helperText={errors.phone?.message}
          />
        )}
      />
      <Controller
        name="password"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Password"
            type="password"
            variant="outlined"
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        )}
      />
      <Controller
        name="estd"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Established Year (BS)"
            variant="outlined"
            error={!!errors.estd}
            helperText={errors.estd?.message}
            inputProps={{
              maxLength: 4,
              pattern: "\\d{4}",
            }}
          />
        )}
      />
      <div style={{ display: "flex", gap: 10, marginTop: 2 }}>
        <Button type="submit" variant="contained" color="primary">
          Register
        </Button>
        <Button onClick={() => reset()} variant="outlined" color="primary">
          Reset
        </Button>
      </div>
      <Typography>
        Already a user? <NavLink to="/login">Login here</NavLink>
      </Typography>
    </Box>
  );
};

export default SchoolRegisterForm;
