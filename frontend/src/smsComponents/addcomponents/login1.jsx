import React, { useState, useEffect } from "react";
import "../../css/login.css";
import eye from "../../assets/eye.svg";
import hide from "../../assets/hide-eye.svg";
import loginimg from "../../assets/img/loginimg.png";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { useDispatch } from "react-redux";
import { login } from "../../redux/reducers/userSlice";
import { useNavigate, Link as NavLink } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { toast } from "react-toastify";
import { Button, Box } from "@mui/material";
// import { Lock, Login as SignIn } from "@mui/icons-material";
import { toastConfig } from "../../config/toastConfig";
import http from "../../config/http";
// import { getDatas } from "../../service/Api";

const LoginForm = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [schools, setSchools] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [currentPage, setCurrentPage] = useState(1);

  const {
    control,
    register1,
    handleSubmit,
    formState: { errors },
  } = useForm();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await http.get("/auth/schools", {
          params: {
            page: currentPage,
            limit: 10, // Set the desired limit
            col: "name", // Set the desired sorting column
            order: 1, // Set the desired sorting order (1 for ascending, -1 for descending)
            s_field: "", // Set the desired search field
            s_query: "", // Set the desired search query
          },
        });
        setSchools(response.data.data);
        setTotalPages(response.data.totalPages);
      } catch (error) {
        console.error("Error fetching school data:", error);
      }
    };
    fetchData();
  }, [currentPage]);

  const onSubmit = async (data) => {
    try {
      console.log("data", data);
      let res = await http.post("/auth/login", data);
      if (res.status === 201) toast.warn(res?.data?.message, toastConfig);

      if (res.status === 200) {
        const userData = {
          isLoggedIn: true,
          userDetails: res.data?.userDetails,
          token: res.data?.session,
        };

        dispatch(login(userData));
        localStorage.userData = JSON.stringify(userData);

        if (res.data?.userDetails?.role_name === "superadmin")
          return navigate("/choose");
        navigate("/");
      }
    } catch (error) {
      console.log("Error Occured ", error);
    }
  };

  var x;
  function handleClick() {
    var x = document.getElementById("password");
    if (x.type === "password") {
      x.type = "text";
      var x = (document.getElementById("pass-icon").src = eye);
    } else {
      x.type = "password";
      var x = (document.getElementById("pass-icon").src = hide);
    }
  }

  return (
    <>
      <div className="login-container">
        <div className="login-card">
          <div className="login-img">
            <img src={loginimg} alt="" />
          </div>
          <div className="login-form">
            <h1 className="name">Log in</h1>
            <div className="card">
              <Box
                component="form"
                onSubmit={handleSubmit(onSubmit)}
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  gap: 2,
                  maxWidth: 400,
                  margin: "0 auto",
                }}
              >
                <div className="card-1">
                  <div>
                    <p>School Name</p>
                    <Autocomplete
                      disablePortal
                      className="card-body"
                      options={schools.map((school) => ({
                        value: school._id,
                        label: school.name,
                      }))}
                      renderInput={(field) => (
                        <TextField
                          {...field}
                          className="border"
                          label="School Name"
                        />
                      )}
                      isOptionEqualToValue={(option, value) =>
                        option.value === value.value
                      }
                    />
                  </div>
                  <Controller
                    name="email"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <TextField
                        {...field}
                        label="Email or Phone"
                        error={!!errors.email}
                        helperText={errors.email?.message}
                      />
                    )}
                  />
                  <Controller
                    name="password"
                    control={control}
                    defaultValue=""
                    render={({ field }) => (
                      <TextField
                        {...field}
                        label="Password"
                        type="password"
                        error={!!errors.password}
                        helperText={errors.password?.message}
                      />
                    )}
                  />
                </div>
                <div className="bottom-container">
                  <input className="checkbox" type="checkbox" />
                  Remember me
                  <a className="forgot-pw" href="#">
                    Forgot password?
                  </a>
                </div>
                <div className="btn">
                  <Button
                    type="submit"
                    className="button-css"
                    variant="contained"
                    color="primary"
                  >
                    Login
                  </Button>
                </div>
              </Box>
              <div className="sign-up">
                <p className="p">
                  Don’t have an account?<a rhef="/Register1">Sign in</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginForm;
