import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  Button,
  Box,
  IconButton,
} from "@mui/material";
import { getDatas, editData, deleteData } from "../../service/Api";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { useNavigate } from "react-router-dom";

const RoleTable = () => {
  const [roles, setRoles] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const navigate = useNavigate();
  const fetchRoles = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("role", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: "createdAt",
        order: -1,
        s_field: "name",
        s_query: searchField,
      });
      setRoles(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching roles:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchField]);

  useEffect(() => {
    fetchRoles();
  }, [fetchRoles]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleEditRole = async (role) => {
    // Add code to navigate to the role edit page
    console.log("Edit role:", role);
  };

  const handleDeleteRole = async (id) => {
    try {
      await deleteData("role", id);
      fetchRoles();
    } catch (error) {
      console.error("Error deleting role:", error);
    }
  };

  const handleAddRole = () => {
    navigate("/settings/role/add");
    // Add code to navigate to the role creation page
    console.log("Add role button clicked");
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <TextField
          label="Search"
          variant="outlined"
          size="small"
          value={searchField}
          onChange={handleSearchFieldChange}
          sx={{ mr: 2 }}
        />
        <Button
          variant="contained"
          sx={{
            background: "#4a2470",
            ":hover": {
              backgroundColor: "#6a3a90",
            },
          }}
          onClick={handleAddRole}
        >
          Add Role
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Created By</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={3} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : roles.length === 0 ? (
              <TableRow>
                <TableCell colSpan={3} align="center">
                  No roles found.
                </TableCell>
              </TableRow>
            ) : (
              roles.map((role, index) => (
                <TableRow
                  key={role._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{role.name}</TableCell>
                  <TableCell>
                    {role.createdBy?.fname} {role.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      sx={{ color: "blue" }}
                      onClick={() => handleEditRole(role)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteRole(role._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalPages * rowsPerPage}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
    </Paper>
  );
};

export default RoleTable;
