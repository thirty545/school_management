import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import ClassAndSectionForm from "./form/class_sec";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const ClassAndSectionTable = () => {
  const [orderBy, setOrderBy] = useState("class");
  const [order, setOrder] = useState("asc");
  const [classSec, setClassSec] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("class");
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedClassSec, setSelectedClassSec] = useState(null);

  const fetchClassSec = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("class_sec", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: searchBy,
        s_query: searchField,
      });
      setClassSec(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching class sections:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField, orderBy, order]);

  useEffect(() => {
    fetchClassSec();
  }, [fetchClassSec]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleAddClassSec = () => {
    setSelectedClassSec(null);
    setPopupOpen(true);
  };

  const handleEditClassSec = (classSec) => {
    setSelectedClassSec(classSec);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedClassSec(null);
  };

  const handleSaveClassSec = async () => {
    await fetchClassSec();
    handleClosePopup();
  };

  const handleDeleteClassSec = async (id) => {
    try {
      await deleteData("class_sec", id);
      toast.success("Class section deleted successfully", toastConfig);
      fetchClassSec();
    } catch (error) {
      console.error("Error deleting class section:", error);
      toast.error("Failed to delete class section", toastConfig);
    }
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Class and Sections
        </Typography>
        <Typography variant="body1">
          This table displays all class and section combinations in the system.
          You can search, add, edit, and manage class-section pairs from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="class">Class</MenuItem>
            <MenuItem value="section">Section</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddClassSec}
          startIcon={<AddIcon />}
        >
          Add Class and Section
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["class", "section", "createdBy", "createdAt"].map(
                (headCell) => (
                  <TableCell key={headCell}>
                    <TableSortLabel
                      active={orderBy === headCell}
                      direction={orderBy === headCell ? order : "asc"}
                      onClick={() => handleRequestSort(headCell)}
                    >
                      {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                    </TableSortLabel>
                  </TableCell>
                )
              )}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : classSec.length === 0 ? (
              <TableRow>
                <TableCell colSpan={5} align="center">
                  No class sections found.
                </TableCell>
              </TableRow>
            ) : (
              classSec.map((cs, index) => (
                <TableRow
                  key={cs._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{cs.class?.name || "N/A"}</TableCell>
                  <TableCell>{cs.section?.name || "N/A"}</TableCell>
                  <TableCell>
                    {cs.createdBy?.fname} {cs.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    {new Date(cs.createdAt).toLocaleDateString()}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      onClick={() => handleEditClassSec(cs)}
                      sx={{ color: "blue" }}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      onClick={() => handleDeleteClassSec(cs._id)}
                      sx={{ color: "red" }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={classSec.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <ClassAndSectionForm
          classSec={selectedClassSec}
          onSave={handleSaveClassSec}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default ClassAndSectionTable;
