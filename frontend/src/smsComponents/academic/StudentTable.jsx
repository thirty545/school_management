import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import Admission from "./form/admission";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const StudentTable = () => {
  const [classSearch, setClassSearch] = useState("");
  const [sectionSearch, setSectionSearch] = useState("");
  const [orderBy, setOrderBy] = useState("fname");
  const [order, setOrder] = useState("asc");
  const [students, setStudents] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("fname");
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedStudent, setSelectedStudent] = useState(null);
  const [classes, setClasses] = useState([]);
  const [sections, setSections] = useState([]);

  // Fetch classes
  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", {
          limit: rowsPerPage,
          currentPage: page,
          orderBy: orderBy,
          order: 1,
          s_field: searchBy,
          s_query: searchField,
        });
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [rowsPerPage, page, orderBy, searchBy, searchField]);

  // Fetch sections based on selected class
  useEffect(() => {
    const fetchSections = async () => {
      if (classSearch) {
        try {
          const sectionData = await getDatas("class_sec", {
            limit: rowsPerPage,
            currentPage: page,
            orderBy: orderBy,
            order: order === "asc" ? 1 : -1,
            s_field: "class",
            s_query: classSearch,
          });
          setSections(sectionData.data);
        } catch (error) {
          console.error("Error fetching sections:", error);
        }
      } else {
        setSections([]);
      }
    };
    fetchSections();
  }, [classSearch, rowsPerPage, page, orderBy, order]);

  // Fetch students
  const fetchStudents = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("student_class", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: "class_sec",
        s_query: sectionSearch,
      });
      setStudents(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching students:", error);
    } finally {
      setLoading(false);
    }
  }, [
    page,
    rowsPerPage,
    searchBy,
    searchField,
    orderBy,
    order,
    classSearch,
    sectionSearch,
  ]);

  // Handlers for table pagination, sorting, and search
  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleClassSearchChange = (event) => {
    setClassSearch(event.target.value);
    setSectionSearch(""); // Reset section when class changes
  };

  const handleSectionSearchChange = (event) => {
    console.log("sectionSearch", sectionSearch);
    setSectionSearch(event.target.value);
  };

  const handleAddStudent = () => {
    setSelectedStudent(null);
    setPopupOpen(true);
  };

  const handleEditStudent = (student) => {
    setSelectedStudent((prevStudentValue) => {
      const newState = { ...prevStudentValue, ...student };
      return newState;
    });
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedStudent(null);
  };

  const handleSaveStudent = async () => {
    await fetchStudents();
    handleClosePopup();
  };

  const handleSearchStudents = () => {
    setPage(0);
    fetchStudents();
  };

  const handleDeleteStudent = async (id) => {
    try {
      await deleteData("student_class", id);
      toast.success("Student deleted successfully", toastConfig);
      fetchStudents();
    } catch (error) {
      console.error("Error deleting student:", error);
      toast.error("Failed to delete student", toastConfig);
    }
  };

  console.log(
    "selectedStudent is outSide HandleEdit function",
    selectedStudent
  );

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Students
        </Typography>
        <Typography variant="body1">
          This table displays all students in the system. You can search, add,
          edit, and manage students from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Class"
            variant="outlined"
            size="small"
            value={classSearch}
            onChange={handleClassSearchChange}
            select
            sx={{ mr: 2, width: 100 }}
          >
            {classes.map((classItem) => (
              <MenuItem key={classItem._id} value={classItem._id}>
                {classItem.name}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            label="Section"
            variant="outlined"
            size="small"
            value={sectionSearch}
            onChange={handleSectionSearchChange}
            select
            sx={{ mr: 2, width: 100 }}
          >
            {sections.map((sectionItem) => (
              <MenuItem key={sectionItem._id} value={sectionItem._id}>
                {sectionItem.section.name}
              </MenuItem>
            ))}
          </TextField>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#5D2E8E",
              "&:hover": { backgroundColor: "#4A2470" },
            }}
            onClick={handleSearchStudents}
            startIcon={<SearchIcon />}
          >
            Search
          </Button>
        </Box>

        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddStudent}
          startIcon={<AddIcon />}
        >
          Add Student
        </Button>
      </Box>

      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {[
                "name",
                "email",
                "phone",
                "class",
                "section",
                "gender",
                "dob",
              ].map((headCell) => (
                <TableCell key={headCell}>
                  <TableSortLabel
                    active={orderBy === headCell}
                    direction={orderBy === headCell ? order : "asc"}
                    onClick={() => handleRequestSort(headCell)}
                  >
                    {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                  </TableSortLabel>
                </TableCell>
              ))}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : students.length === 0 ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  No students found.
                </TableCell>
              </TableRow>
            ) : (
              students.map((student, index) => (
                <TableRow
                  key={student._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{`${student.student.fname} ${student.student.lname}`}</TableCell>
                  <TableCell>{student.student.email}</TableCell>
                  <TableCell>{student.student.phone}</TableCell>
                  <TableCell>
                    {student.class_sec?.class?.name || "N/A"}
                  </TableCell>
                  <TableCell>
                    {student.class_sec?.section?.name || "N/A"}
                  </TableCell>
                  <TableCell>{student.student.gender}</TableCell>
                  <TableCell>{student.student.dob}</TableCell>
                  <TableCell>
                    <IconButton
                      sx={{ color: "blue" }}
                      onClick={() => handleEditStudent(student)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteStudent(student._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={totalPages * rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      {popupOpen && (
        <CustomPopup
          open={popupOpen}
          onClose={handleClosePopup}
          title="Add / Edit Student"
        >
          <Admission
            studentData={selectedStudent}
            onSave={handleSaveStudent}
            onClose={handleClosePopup}
          />
        </CustomPopup>
      )}
    </Paper>
  );
};

export default StudentTable;
