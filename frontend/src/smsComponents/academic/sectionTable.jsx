import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import SectionForm from "./form/section";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const SectionTable = () => {
  const [orderBy, setOrderBy] = useState("name");
  const [order, setOrder] = useState("asc");
  const [sections, setSections] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("name");
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedSection, setSelectedSection] = useState(null);

  const fetchSections = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("section", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: searchBy,
        s_query: searchField,
      });
      setSections(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching sections:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField, orderBy, order]);

  useEffect(() => {
    fetchSections();
  }, [fetchSections]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleAddSection = () => {
    setSelectedSection(null);
    setPopupOpen(true);
  };

  const handleEditSection = (section) => {
    setSelectedSection(section);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedSection(null);
  };

  const handleSaveSection = async () => {
    await fetchSections();
    handleClosePopup();
  };

  const handleDeleteSection = async (id) => {
    try {
      await deleteData("section", id);
      toast.success("Section deleted successfully", toastConfig);
      fetchSections();
    } catch (error) {
      console.error("Error deleting section:", error);
      toast.error("Failed to delete section", toastConfig);
    }
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Sections
        </Typography>
        <Typography variant="body1">
          This table displays all sections in the system. You can search, add,
          edit, and manage sections from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="name">Section Name</MenuItem>
            <MenuItem value="createdBy">Created By</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddSection}
          startIcon={<AddIcon />}
        >
          Add Section
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["name", "createdBy", "createdAt"].map((headCell) => (
                <TableCell key={headCell}>
                  <TableSortLabel
                    active={orderBy === headCell}
                    direction={orderBy === headCell ? order : "asc"}
                    onClick={() => handleRequestSort(headCell)}
                  >
                    {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                  </TableSortLabel>
                </TableCell>
              ))}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : sections.length === 0 ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  No sections found.
                </TableCell>
              </TableRow>
            ) : (
              sections.map((section, index) => (
                <TableRow
                  key={section._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{section.name}</TableCell>
                  <TableCell>
                    {section.createdBy?.fname} {section.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    {new Date(section.createdAt).toLocaleDateString()}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      onClick={() => handleEditSection(section)}
                      sx={{ color: "blue" }}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      onClick={() => handleDeleteSection(section._id)}
                      sx={{ color: "red" }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={sections.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <SectionForm
          section={selectedSection}
          onSave={handleSaveSection}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default SectionTable;
