// UserTable.jsx
import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

import { getDatas } from "../../service/Api.js";
import { useNavigate } from "react-router-dom";
import ActionButtons from "../../components/ActionButtons.js";
import { loggedInData } from "../../config/authData.js";

const UserTable = () => {
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("fname");
  const navigate = useNavigate();
  const loggedInUser = loggedInData();

  const fetchUsers = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("user", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: "createdAt",
        order: -1,
        s_field: searchBy,
        s_query: searchField,
      });
      console.log("data of user", data);
      setUsers(data);
      console.log("data of user", users);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching users:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField]);

  useEffect(() => {
    console.log("Fetching users...");
    console.log("loggedInUser", loggedInUser);
    fetchUsers();
  }, [fetchUsers]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleAddUser = () => {
    navigate("/users/add");
  };

  const handleEditUser = (user) => {
    navigate(`/users/edit/${user._id}`, { state: { user } });
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Users
        </Typography>
        <Typography variant="body1">
          This table displays all users in the system. You can search, add,
          edit, and manage users from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="fname">First Name</MenuItem>
            <MenuItem value="lname">Last Name</MenuItem>
            <MenuItem value="email">Email</MenuItem>
            <MenuItem value="phone">Phone</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={handleAddUser}
          startIcon={<AddIcon />}
        >
          Add User
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>First Name</TableCell>
              <TableCell>Last Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>Role</TableCell>
              <TableCell>Gender</TableCell>
              <TableCell>Date of Birth</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : users.length === 0 ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  No users found.
                </TableCell>
              </TableRow>
            ) : (
              users.map((user, index) => (
                <TableRow
                  key={user._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{user.fname}</TableCell>
                  <TableCell>{user.lname}</TableCell>
                  <TableCell>{user.email}</TableCell>
                  <TableCell>{user.phone}</TableCell>
                  <TableCell>{user.role?.name || "N/A"}</TableCell>
                  <TableCell>{user.gender || "N/A"}</TableCell>
                  <TableCell>
                    {user.dob ? new Date(user.dob).toLocaleDateString() : "N/A"}
                  </TableCell>
                  <TableCell>
                    <ActionButtons
                      item_id={user._id}
                      formName="user"
                      getData={fetchUsers}
                      handleEditUser={() => handleEditUser(user)}
                    />
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalPages * rowsPerPage}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
    </Paper>
  );
};

export default UserTable;
