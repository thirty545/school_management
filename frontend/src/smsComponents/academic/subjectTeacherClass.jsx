import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import TeacherForm from "./form/ClassSubTeacher";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const TeacherTable = () => {
  const [classSearch, setClassSearch] = useState("");
  const [subjectSearch, setSubjectSearch] = useState("");
  const [orderBy, setOrderBy] = useState("teacherName");
  const [order, setOrder] = useState("asc");
  const [teachers, setTeachers] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedTeacher, setSelectedTeacher] = useState(null);
  const [totalRows, setTotalRows] = useState(0);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("fname");

  const fetchTeachers = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages, totalRows } = await getDatas(
        "class_sub_teacher",
        {
          limit: rowsPerPage,
          currentPage: page,
          orderBy: orderBy,
          order: order === "asc" ? 1 : -1,
          s_field: searchBy,
          s_query: searchField,
        }
      );
      console.log("teacher Data", data);
      setTeachers(data || []);
      setTotalPages(totalPages || 0);
      setTotalRows(totalRows || 0);
    } catch (error) {
      console.error("Error fetching teachers:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, orderBy, order, searchBy, searchField]);

  useEffect(() => {
    fetchTeachers();
  }, [fetchTeachers]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleAdd = () => {
    setSelectedTeacher(null);
    setPopupOpen(true);
  };

  const handleEditTeacher = (teacher) => {
    setSelectedTeacher(teacher);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedTeacher(null);
  };

  const handleSaveTeacher = async () => {
    await fetchTeachers();
    handleClosePopup();
  };

  const handleDeleteTeacher = async (id) => {
    try {
      await deleteData("teacher", id);
      toast.success("Teacher deleted successfully", toastConfig);
      fetchTeachers();
    } catch (error) {
      console.error("Error deleting teacher:", error);
      toast.error("Failed to delete teacher", toastConfig);
    }
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Teachers
        </Typography>
        <Typography variant="body1">
          This table displays all teachers in the system. You can search, add,
          edit, and manage teachers and their associated classes and subjects
          from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <div className="border-b border-gray-200"></div>

      <div className="flex justify-end items-center p-4">
        <button
          className="bg-[#5D2E8E] hover:bg-[#4A2470] text-white font-bold py-2 px-4 rounded flex items-center"
          onClick={handleAdd}
        >
          <AddIcon className="mr-2" />
          Add STC
        </button>
      </div>

      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["TeacherName", "Class", "Section", "Subject"].map(
                (headCell) => (
                  <TableCell key={headCell}>
                    <TableSortLabel
                      active={orderBy === headCell}
                      direction={orderBy === headCell ? order : "asc"}
                      onClick={() => handleRequestSort(headCell)}
                    >
                      {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                    </TableSortLabel>
                  </TableCell>
                )
              )}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : !teachers?.length ? (
              <TableRow>
                <TableCell colSpan={5} align="center">
                  No teachers found.
                </TableCell>
              </TableRow>
            ) : (
              teachers.map((teacher, index) => (
                <TableRow
                  key={teacher._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>
                    {teacher.teacher?.fname} {teacher.teacher?.lname}{" "}
                  </TableCell>
                  <TableCell>
                    {teacher.class_sec?.class?.name || "N/A"}
                  </TableCell>
                  <TableCell>
                    {teacher.class_sec?.section?.name || "N/A"}
                  </TableCell>
                  <TableCell>{teacher.subject?.name || "N/A"}</TableCell>
                  <TableCell>
                    <IconButton
                      aria-label="edit"
                      sx={{ color: "blue" }}
                      onClick={() => handleEditTeacher(teacher)}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      aria-label="delete"
                      sx={{ color: "red" }}
                      onClick={() => handleDeleteTeacher(teacher._id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={totalRows}
        page={page}
        onPageChange={handlePageChange}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleRowsPerPageChange}
      />

      {popupOpen && (
        <CustomPopup open={popupOpen} onClose={handleClosePopup}>
          <TeacherForm
            teacher={selectedTeacher}
            onSave={handleSaveTeacher}
            onClose={handleClosePopup}
          />
        </CustomPopup>
      )}
    </Paper>
  );
};

export default TeacherTable;
