import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
  IconButton,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import { getDatas, deleteData } from "../../service/Api";
import CustomPopup from "../../components/customs/PopUP";
import ClassForm from "./form/class";
import { toast } from "react-toastify";
import { toastConfig } from "../../config/toastConfig";

const ClassTable = () => {
  const [orderBy, setOrderBy] = useState("name");
  const [order, setOrder] = useState("asc");
  const [classes, setClasses] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("name");
  const [popupOpen, setPopupOpen] = useState(false);
  const [selectedClass, setSelectedClass] = useState(null);

  const fetchClasses = useCallback(async () => {
    setLoading(true);
    try {
      const { data, totalPages } = await getDatas("class", {
        limit: rowsPerPage,
        currentPage: page,
        orderBy: orderBy,
        order: order === "asc" ? 1 : -1,
        s_field: searchBy,
        s_query: searchField,
      });
      setClasses(data);
      setTotalPages(totalPages);
    } catch (error) {
      console.error("Error fetching classes:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField, orderBy, order]);

  useEffect(() => {
    fetchClasses();
  }, [fetchClasses]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleAddClass = () => {
    setSelectedClass(null);
    setPopupOpen(true);
  };

  const handleEditClass = (classData) => {
    setSelectedClass(classData);
    setPopupOpen(true);
  };

  const handleClosePopup = () => {
    setPopupOpen(false);
    setSelectedClass(null);
  };

  const handleSaveClass = async () => {
    await fetchClasses();
    handleClosePopup();
  };

  const handleDeleteClass = async (id) => {
    try {
      await deleteData("class", id);
      toast.success("Class deleted successfully", toastConfig);
      fetchClasses();
    } catch (error) {
      console.error("Error deleting class:", error);
      toast.error("Failed to delete class", toastConfig);
    }
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Classes
        </Typography>
        <Typography variant="body1">
          This table displays all classes in the system. You can search, add,
          edit, and manage classes from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="name">Class</MenuItem>
            <MenuItem value="createdBy">Created By</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddClass}
          startIcon={<AddIcon />}
        >
          Add Class
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["name", "createdBy", "createdAt"].map((headCell) => (
                <TableCell key={headCell}>
                  <TableSortLabel
                    active={orderBy === headCell}
                    direction={orderBy === headCell ? order : "asc"}
                    onClick={() => handleRequestSort(headCell)}
                  >
                    {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                  </TableSortLabel>
                </TableCell>
              ))}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : classes.length === 0 ? (
              <TableRow>
                <TableCell colSpan={4} align="center">
                  No classes found.
                </TableCell>
              </TableRow>
            ) : (
              classes.map((classData, index) => (
                <TableRow
                  key={classData._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>{classData.name}</TableCell>
                  <TableCell>
                    {classData.createdBy?.fname} {classData.createdBy?.lname}
                  </TableCell>
                  <TableCell>
                    {new Date(classData.createdAt).toLocaleDateString()}
                  </TableCell>
                  <TableCell>
                    <IconButton
                      onClick={() => handleEditClass(classData)}
                      sx={{ color: "blue" }}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      onClick={() => handleDeleteClass(classData._id)}
                      sx={{ color: "red" }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={classes.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
      <CustomPopup open={popupOpen} onClose={handleClosePopup}>
        <ClassForm
          classData={selectedClass}
          onSave={handleSaveClass}
          onClose={handleClosePopup}
        />
      </CustomPopup>
    </Paper>
  );
};

export default ClassTable;
