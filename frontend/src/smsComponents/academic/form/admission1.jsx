import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, MenuItem } from "@mui/material";
import { editData, getDatas } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const Admission = ({ student, onSave, onClose }) => {
  const [formData, setFormData] = useState({
    fname: "",
    lname: "",
    class: "",
    class_sec: "",
  });
  const [classes, setClasses] = useState([]);
  const [sections, setSections] = useState([]);

  useEffect(() => {
    fetchClasses();
    if (student) {
      setFormData({
        fname: student.student?.fname || "",
        lname: student.student?.lname || "",
        class: student.class_sec?.class?._id || "",
        class_sec: student.class_sec?._id || "",
      });
      if (student.class_sec?.class?._id) {
        fetchSections(student.class_sec.class._id);
      }
    }
  }, [student]);

  const fetchClasses = async () => {
    try {
      const response = await getDatas("class", {});
      setClasses(response.data);
    } catch (error) {
      console.error("Error fetching classes:", error);
    }
  };

  const fetchSections = async (classId) => {
    try {
      const response = await getDatas("class_sec", {
        s_field: "class",
        s_query: classId,
      });
      setSections(response.data);
    } catch (error) {
      console.error("Error fetching sections:", error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData((prev) => ({ ...prev, [name]: value }));

    if (name === "class") {
      fetchSections(value);
      setFormData((prev) => ({ ...prev, class_sec: "" }));
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const dataToSubmit = {
        student: {
          fname: formData.fname,
          lname: formData.lname,
        },
        class_sec: formData.class_sec,
      };

      await editData("student_class", student._id, dataToSubmit);
      toast.success("Student updated successfully", toastConfig);
      onSave();
    } catch (error) {
      console.error("Error updating student:", error);
      toast.error("Error updating student", toastConfig);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <TextField
            fullWidth
            label="First Name"
            name="fname"
            value={formData.fname}
            onChange={handleChange}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            label="Last Name"
            name="lname"
            value={formData.lname}
            onChange={handleChange}
            required
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            select
            label="Class"
            name="class"
            value={formData.class}
            onChange={handleChange}
            required
          >
            {classes.map((classItem) => (
              <MenuItem key={classItem._id} value={classItem._id}>
                {classItem.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            select
            label="Section"
            name="class_sec"
            value={formData.class_sec}
            onChange={handleChange}
            required
          >
            {sections.map((section) => (
              <MenuItem key={section._id} value={section._id}>
                {section.section.name}
              </MenuItem>
            ))}
          </TextField>
        </Grid>
      </Grid>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        style={{ marginTop: 20 }}
      >
        Update Student
      </Button>
    </form>
  );
};

export default Admission;
