import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Box, TextField, Button, Typography } from "@mui/material";
import { addData, editData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const SubjectForm = ({ subjectData, onSave, onClose }) => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Subject name exceeds the character limit of 100")
      .required("Subject name is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: subjectData || { name: "" },
  });

  const onSubmit = async (data) => {
    try {
      let res;
      if (subjectData) {
        res = await editData("subject", subjectData._id, data);
      } else {
        res = await addData("subject", data);
      }

      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding subject:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add subject",
        toastConfig
      );
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h5" align="center" gutterBottom>
        {subjectData ? "Edit Subject" : "Add Subject"}
      </Typography>
      <div>
        <label htmlFor="subject">Subject</label>
        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <div>
              <input
                {...field}
                placeholder="Subject Name"
                className={`w-full px-3 py-2 border-2 rounded-md focus:outline-none bg-transparent placeholder-gray-800 ${
                  errors.name ? "border-red-500" : "border-black"
                }`}
              />
              {errors.name && (
                <p className="mt-1 text-sm text-red-500">
                  {errors.name.message}
                </p>
              )}
            </div>
          )}
        />
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {subjectData ? "Update Subject" : "Add Subject"}
        </Button>
      </Box>
    </Box>
  );
};

export default SubjectForm;
