import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Button,
  Typography,
  Autocomplete,
} from "@mui/material";
import {
  getDatas,
  editData,
  addData,
  getDataById,
  getItemsByRole,
} from "../../../service/Api";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const TeacherForm = ({ teacher, onSave, onClose }) => {
  const [classSearch, setClassSearch] = useState("");
  const [sectionSearch, setSectionSearch] = useState("");
  const [subjectSearch, setSubjectSearch] = useState("");
  const [teacherSearch, setTeacherSearch] = useState("");
  const [teachers, setTeachers] = useState([]);
  const [sections, setSections] = useState([]);
  const [subjects, setSubjects] = useState([]);
  const [classes, setClasses] = useState([]);
  const [selectedClass, setSelectedClass] = useState(null);
  const [initialTeacher, setInitialTeacher] = useState(null);
  const navigate = useNavigate();
  const { id } = useParams();

  const validationSchema = Yup.object().shape({
    class: Yup.object().nullable().required("Class is required"),
    section: Yup.object().nullable().required("Section is required"),
    subject: Yup.object().nullable().required("Subject is required"),
    teacher: Yup.object().nullable().required("Teacher is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    watch,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialTeacher || {},
  });

  const watchClass = watch("class");

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", classSearch);
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [classSearch]);

  useEffect(() => {
    const fetchSections = async () => {
      if (watchClass) {
        try {
          const sectionData = await getDatas("class_sec", {
            s_field: "class",
            s_query: watchClass._id,
          });
          setSections(sectionData.data);
        } catch (error) {
          console.error("Error fetching sections:", error);
        }
      } else {
        setSections([]);
      }
    };
    fetchSections();
  }, [watchClass]);

  useEffect(() => {
    const fetchSubjects = async () => {
      try {
        const subjectData = await getDatas("subject", subjectSearch);
        setSubjects(subjectData.data);
      } catch (error) {
        console.error("Error fetching subjects:", error);
      }
    };
    fetchSubjects();
  }, [subjectSearch]);

  useEffect(() => {
    const fetchTeachers = async () => {
      console.log("fetching Teachers");
      try {
        const teacherData = await getItemsByRole(
          "user",
          "Teacher",
          teacherSearch
        );
        setTeachers(teacherData.data);
        console.log("teacherdata", teacherData.data);
      } catch (error) {
        console.error("Error fetching teachers:", error);
      }
    };
    fetchTeachers();
  }, [teacherSearch]);

  useEffect(() => {
    const fetchTeacherData = async () => {
      console.log("fetching Teacher Data", teacher);
      if (teacher) {
        try {
          const teacherData = await getDataById(
            "class_sub_teacher",
            teacher._id
          );
          setInitialTeacher(teacherData.data);
          setValue("class", teacherData.data.class_sec.class);
          setValue("section", teacherData.data.class_sec);
          setValue("subject", teacherData.data.subject);
          setValue("teacher", teacherData.data.teacher);
          setSelectedClass(teacherData.data.class);
        } catch (error) {
          console.error("Error fetching teacher data:", error);
          toast.error("Failed to fetch teacher data", toastConfig);
        }
      }
    };
    fetchTeacherData();
  }, [teacher, setValue]);

  const handleClassChange = (event, newValue) => {
    setClassSearch(event.target.value);
    setSelectedClass(newValue);
    setValue("section", null);
  };

  const onSubmit = async (data) => {
    try {
      const teacherData = {
        class_sec: data.section._id,
        subject: data.subject._id,
        teacher: data.teacher._id,
      };
      let res;
      if (initialTeacher) {
        res = await editData(
          "class_sub_teacher",
          initialTeacher._id,
          teacherData
        );
      } else {
        res = await addData("class_sub_teacher", teacherData);
      }
      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding teacher:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add teacher",
        toastConfig
      );
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        {initialTeacher ? "Edit Teacher Assignment" : "Add Teacher Assignment"}
      </Typography>

      <div>
        <label htmlFor="class">Class</label>{" "}
        <Controller
          name="class"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={classes}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search class"
                  onChange={(e) => setClassSearch(e.target.value)}
                  error={!!errors.class}
                  helperText={errors.class?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => {
                field.onChange(value);
                handleClassChange(_, value);
              }}
            />
          )}
        />
      </div>
      <div>
        <label htmlFor="section">Section</label>
        <Controller
          name="section"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={sections}
              getOptionLabel={(option) => option.section?.name || ""}
              isOptionEqualToValue={
                (option, value) =>
                  option && value && option.value === value.value // Compare based on `value`
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search section"
                  onChange={(e) => setSectionSearch(e.target.value)}
                  error={!!errors.section}
                  helperText={errors.section?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)} // Ensure field.onChange receives the selected value
            />
          )}
        />
      </div>
      <div>
        <label htmlFor="subject">Subject</label>
        <Controller
          name="subject"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={subjects}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search subject"
                  onChange={(e) => setSubjectSearch(e.target.value)}
                  error={!!errors.subject}
                  helperText={errors.subject?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.subject
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.subject
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="teacher">Teacher</label>
        <Controller
          name="teacher"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={teachers}
              getOptionLabel={(option) =>
                option.fname + " " + option.lname || ""
              }
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Select a teacher"
                  error={!!errors.teacher}
                  helperText={errors.teacher?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.teacher
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.teacher
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>

      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button variant="outlined" onClick={onClose}>
          Cancel
        </Button>
        <Button variant="contained" type="submit">
          {initialTeacher ? "Update" : "Add"}
        </Button>
      </Box>
    </Box>
  );
};

export default TeacherForm;
