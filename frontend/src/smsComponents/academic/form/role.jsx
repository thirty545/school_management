import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Switch,
  FormControlLabel,
  Button,
  Typography,
} from "@mui/material";
import { getDatas, editData, deleteData, addData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";
import { useNavigate } from "react-router-dom";

const RoleForm = () => {
  const navigate = useNavigate();
  const [permissions, setPermissions] = useState({
    class: { read: false, write: false, delete: false },
    exams: { read: false, write: false, delete: false },
    fee: { read: false, write: false, delete: false },
    marksheet: { read: false, write: false, delete: false },
    role: { read: false, write: false, delete: false },
    staff_bio: { read: false, write: false, delete: false },
    staff_salary_log: { read: false, write: false, delete: false },
    student_bio: { read: false, write: false, delete: false },
    student_class: { read: false, write: false, delete: false },
    student_fee: { read: false, write: false, delete: false },
    subject_class: { read: false, write: false, delete: false },
    subject: { read: false, write: false, delete: false },
    teacher_class: { read: false, write: false, delete: false },
    user: { read: false, write: false, delete: false },
  });

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Role name is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const handlePermissionChange = (model, permission, value) => {
    setPermissions((prevPermissions) => ({
      ...prevPermissions,
      [model]: {
        ...prevPermissions[model],
        [permission]: value,
      },
    }));
  };

  const onSubmit = async (data) => {
    try {
      console.log(data);
      const res = await addData("role", { ...data, permissions });
      console.log("response", res);
      if (res.statusText === "OK") {
        console.log("Response is ok", res);
        toast.success("Role added successfully", toastConfig);
        navigate("/settings/role");
      } else {
        toast.error(res.response.data.message, toastConfig);
      }

      // Handle successful submission, e.g., show a success message
    } catch (error) {
      console.error("Error adding role:", error);
      // Handle error, e.g., show an error message
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        Add Role
      </Typography>
      <Controller
        name="name"
        control={control}
        defaultValue=""
        render={({ field }) => (
          <TextField
            {...field}
            label="Role Name"
            variant="outlined"
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        )}
      />
      <Typography variant="h5" gutterBottom>
        Permissions
      </Typography>
      {Object.keys(permissions).map((model) => (
        <Box key={model} sx={{ display: "flex", flexDirection: "column" }}>
          <Typography variant="subtitle1">{model}</Typography>
          <FormControlLabel
            control={
              <Switch
                checked={permissions[model].read}
                onChange={(e) =>
                  handlePermissionChange(model, "read", e.target.checked)
                }
              />
            }
            label="Read"
          />
          <FormControlLabel
            control={
              <Switch
                checked={permissions[model].write}
                onChange={(e) =>
                  handlePermissionChange(model, "write", e.target.checked)
                }
              />
            }
            label="Write"
          />
          <FormControlLabel
            control={
              <Switch
                checked={permissions[model].delete}
                onChange={(e) =>
                  handlePermissionChange(model, "delete", e.target.checked)
                }
              />
            }
            label="Delete"
          />
        </Box>
      ))}
      <Button
        type="submit"
        variant="contained"
        sx={{
          background: "#4a2470",
          ":hover": {
            backgroundColor: "#6a3a90",
          },
        }}
      >
        Add Role
      </Button>
    </Box>
  );
};

export default RoleForm;
