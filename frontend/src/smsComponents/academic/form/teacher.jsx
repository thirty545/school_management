import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { getItemsByRole } from "../service/Api";
import { useNavigate } from "react-router-dom";
import ActionButtons from "../components/ActionButtons";

const TeacherTable = () => {
  const [totalRows, setTotalRows] = useState(0);
  const [orderBy, setOrderBy] = useState("fname");
  const [order, setOrder] = useState("asc");
  const [teachers, setTeachers] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [searchBy, setSearchBy] = useState("fname");
  const navigate = useNavigate();

  const fetchTeachers = useCallback(async () => {
    setLoading(true);
    try {
      console.log("Fetching Teachers...");
      const { data, totalPages, totalRows } = await getItemsByRole(
        "user",
        "Teacher",
        {
          limit: rowsPerPage,
          currentPage: page + 1,
          orderBy: orderBy,
          order: order,
          s_field: searchBy,
          s_query: searchField,
        }
      );
      setTeachers(data);
      setTotalPages(totalPages);
      setTotalRows(totalRows);
    } catch (error) {
      console.error("Error fetching Teachers:", error);
    } finally {
      setLoading(false);
    }
  }, [page, rowsPerPage, searchBy, searchField, orderBy, order]);

  useEffect(() => {
    fetchTeachers();
  }, [fetchTeachers]);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0);
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleSearchByChange = (event) => {
    setSearchBy(event.target.value);
  };

  const handleAddTeacher = () => {
    navigate("/users/teachers/add");
  };

  const handleEditTeacher = (teacher) => {
    navigate(`/users/teachers/edit/${teacher._id}`, { state: { teacher } });
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Teachers
        </Typography>
        <Typography variant="body1">
          This table displays all Teachers in the system. You can search, add,
          edit, and manage teachers from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Search By"
            variant="outlined"
            size="small"
            value={searchBy}
            onChange={handleSearchByChange}
            select
          >
            <MenuItem value="fname">First Name</MenuItem>
            <MenuItem value="email">Email</MenuItem>
            <MenuItem value="phone">Phone</MenuItem>
          </TextField>
        </Box>
        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddTeacher}
          startIcon={<AddIcon />}
        >
          Add Teacher
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["name", "email", "phone", "gender", "dob"].map((headCell) => (
                <TableCell key={headCell}>
                  <TableSortLabel
                    active={orderBy === headCell}
                    direction={orderBy === headCell ? order : "asc"}
                    onClick={() => handleRequestSort(headCell)}
                  >
                    {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                  </TableSortLabel>
                </TableCell>
              ))}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : teachers.length === 0 ? (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  No teachers found.
                </TableCell>
              </TableRow>
            ) : (
              teachers.map((teacher, index) => (
                <TableRow
                  key={teacher._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>
                    {teacher.fname} {teacher.lname}
                  </TableCell>
                  <TableCell>{teacher.email}</TableCell>
                  <TableCell>{teacher.phone}</TableCell>
                  <TableCell>{teacher.gender || "N/A"}</TableCell>
                  <TableCell>
                    {teacher.dob
                      ? new Date(teacher.dob).toLocaleDateString()
                      : "N/A"}
                  </TableCell>
                  <TableCell>
                    <ActionButtons
                      item_id={teacher._id}
                      formName="teacher"
                      getData={fetchTeachers}
                      handleEditTeacher={() => handleEditTeacher(teacher)}
                    />
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={totalRows}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
    </Paper>
  );
};

export default TeacherTable;
