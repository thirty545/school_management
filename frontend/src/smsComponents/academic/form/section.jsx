import React from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Box, TextField, Button, Typography } from "@mui/material";
import { addData, editData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const SectionForm = ({ section, onSave, onClose }) => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(15, "Section must be less than 15 characters")
      .required("Section is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: section || { name: "" },
  });

  const onSubmit = async (data) => {
    try {
      let res;
      if (section) {
        res = await editData("section", section._id, data);
      } else {
        res = await addData("section", data);
      }

      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding section:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add section",
        toastConfig
      );
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        p: 3,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h5" align="center" gutterBottom>
        {section ? "Edit Section" : "Add Section"}
      </Typography>
      <div>
        <label htmlFor="section">Section</label>
        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <div className="w-full">
              <input
                {...field}
                placeholder="Section Name"
                className={`w-full px-3 py-2 border-2 rounded-md focus:outline-none bg-transparent placeholder-gray-800 ${
                  errors.name ? "border-red-500" : "border-black"
                }`}
              />
              {errors.name && (
                <p className="mt-1 text-sm text-red-500">
                  {errors.name.message}
                </p>
              )}
            </div>
          )}
        />
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#5D2E8E" },
          }}
        >
          {section ? "Update Section" : "Add Section"}
        </Button>
      </Box>
    </Box>
  );
};

export default SectionForm;
