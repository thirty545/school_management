import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Button,
  Typography,
  Autocomplete,
} from "@mui/material";
import {
  getDatas,
  editData,
  addData,
  getDataById,
  getItemsByRole,
} from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const Admission = ({ studentData, onSave, onClose }) => {
  const [studentSearch, setStudentSearch] = useState("");
  const [classSearch, setClassSearch] = useState("");
  const [sectionSearch, setSectionSearch] = useState("");
  const [students, setStudents] = useState([]);
  const [classes, setClasses] = useState([]);
  const [sections, setSections] = useState([]);
  const [selectedClass, setSelectedClass] = useState(null);
  const [initialAdmission, setInitialAdmission] = useState(null);

  console.log("studentData is in admission form", studentData);

  const validationSchema = Yup.object().shape({
    student: Yup.object().nullable().required("Student is required"),
    class: Yup.object().nullable().required("Class is required"),
    section: Yup.object().nullable().required("Section is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
    watch,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialAdmission || {},
  });

  const watchClass = watch("class");

  useEffect(() => {
    const fetchStudents = async () => {
      try {
        const studentData = await getItemsByRole(
          "user",
          "Student",
          studentSearch
        );
        setStudents(studentData.data);
      } catch (error) {
        console.error("Error fetching students:", error);
      }
    };
    fetchStudents();
  }, [studentSearch]);

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", classSearch);
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [classSearch]);

  useEffect(() => {
    const fetchSections = async () => {
      if (watchClass) {
        try {
          const sectionData = await getDatas("class_sec", {
            s_field: "class",
            s_query: watchClass._id,
          });
          setSections(sectionData.data);
        } catch (error) {
          console.error("Error fetching sections:", error);
        }
      } else {
        setSections([]);
      }
    };
    fetchSections();
  }, [watchClass]);

  useEffect(() => {
    console.log("fetching studentData:", studentData);
    const fetchAdmissionData = async () => {
      if (studentData) {
        console.log("studentData:", studentData);
        try {
          const admissionData = await getDataById(
            "student_class",
            studentData._id
          );
          setInitialAdmission(admissionData.data);
          setValue("student", admissionData.data.student);
          setValue("class", admissionData.data.class_sec.class);
          setValue("section", admissionData.data.class_sec);
          setSelectedClass(admissionData.data.class_sec.class);
        } catch (error) {
          console.error("Error fetching admission data:", error);
          toast.error("Failed to fetch admission data", toastConfig);
        }
      }
    };
    fetchAdmissionData();
  }, [studentData, setValue]);

  const handleClassChange = (event, newValue) => {
    setClassSearch(event.target.value);
    setSelectedClass(newValue);
    setValue("section", null);
  };

  const onSubmit = async (data) => {
    try {
      const admissionData = {
        student: data.student._id,
        class_sec: data.section._id,
      };
      let res;
      if (initialAdmission) {
        res = await editData(
          "student_class",
          initialAdmission._id,
          admissionData
        );
      } else {
        res = await addData("student_class", admissionData);
      }
      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding admission:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add admission",
        toastConfig
      );
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        {initialAdmission ? "Edit Admission" : "Add Admission"}
      </Typography>

      <div>
        <label htmlFor="student">Student</label>
        <Controller
          name="student"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={students}
              getOptionLabel={(option) =>
                `${option.fname} ${option.lname}` || ""
              }
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search student"
                  onChange={(e) => setStudentSearch(e.target.value)}
                  error={!!errors.student}
                  helperText={errors.student?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.student
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.student
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>
      <div>
        <label htmlFor="class">Class</label>
        <Controller
          name="class"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={classes}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search class"
                  onChange={(e) => setClassSearch(e.target.value)}
                  error={!!errors.class}
                  helperText={errors.class?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => {
                field.onChange(value);
                handleClassChange(_, value);
              }}
            />
          )}
        />
      </div>
      <div>
        <label htmlFor="section">Section</label>
        <Controller
          name="section"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={sections}
              getOptionLabel={(option) => option.section?.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Section"
                  onChange={(e) => setSectionSearch(e.target.value)}
                  error={!!errors.section}
                  helperText={errors.section?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button variant="outlined" onClick={onClose}>
          Cancel
        </Button>
        <Button variant="contained" type="submit">
          {initialAdmission ? "Update" : "Add"}
        </Button>
      </Box>
    </Box>
  );
};

export default Admission;
