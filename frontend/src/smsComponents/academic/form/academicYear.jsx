import React, { useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Box, TextField, Button, Typography } from "@mui/material";
import { addData, editData } from "../../../service/Api";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const SessionForm = ({ sessionData, onSave, onClose, fetchSessions }) => {
  const validationSchema = Yup.object().shape({
    name: Yup.string()
      .max(100, "Session name exceeds the character limit of 100")
      .required("Session name is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: sessionData || { name: "" },
  });

  const onSubmit = async (data) => {
    try {
      let res;
      if (sessionData) {
        res = await editData("session", sessionData._id, data);
        fetchSessions();
      } else {
        res = await addData("session", data);
        fetchSessions();
      }

      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding session:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add session",
        toastConfig
      );
    }
  };

  useEffect(() => {
    fetchSessions();
  }, []);

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h5" align="center" gutterBottom>
        {sessionData ? "Edit Academic Year" : "Add Session"}
      </Typography>
      <Controller
        name="name"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            label="Session Name"
            variant="outlined"
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        )}
      />
      <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
        <Button onClick={onClose} variant="outlined">
          Cancel
        </Button>
        <Button
          type="submit"
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
        >
          {sessionData ? "Update Session" : "Add Session"}
        </Button>
      </Box>
    </Box>
  );
};

export default SessionForm;
