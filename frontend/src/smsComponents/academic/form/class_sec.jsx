import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  Box,
  TextField,
  Button,
  Typography,
  Autocomplete,
} from "@mui/material";
import { getDatas, editData, addData, getDataById } from "../../../service/Api";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig";

const ClassAndSectionForm = ({ classSec, onSave, onClose }) => {
  const [classSearch, setClassSearch] = useState("");
  const [sectionSearch, setSectionSearch] = useState("");
  const [classes, setClasses] = useState([]);
  const [sections, setSections] = useState([]);
  const navigate = useNavigate();
  const { id } = useParams();
  const [initialClassSec, setInitialClassSec] = useState(null);

  const validationSchema = Yup.object().shape({
    class: Yup.object().nullable().required("Class is required"),
    section: Yup.object().nullable().required("Section is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialClassSec || {},
  });

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const classData = await getDatas("class", classSearch);
        setClasses(classData.data);
      } catch (error) {
        console.error("Error fetching classes:", error);
      }
    };
    fetchClasses();
  }, [classSearch]);

  useEffect(() => {
    const fetchSections = async () => {
      try {
        const sectionData = await getDatas("section", sectionSearch);
        console.log(sectionData.data);
        setSections(sectionData.data);
      } catch (error) {
        console.error("Error fetching sections:", error);
      }
    };
    fetchSections();
  }, [sectionSearch]);

  useEffect(() => {
    const fetchClassSecData = async () => {
      if (classSec) {
        try {
          const classSecData = await getDataById("class_sec", classSec._id);
          setInitialClassSec(classSecData.data);
          setValue("class", classSecData.data.class);
          setValue("section", classSecData.data.section);
        } catch (error) {
          console.error("Error fetching class section data:", error);
          toast.error("Failed to fetch class section data", toastConfig);
        }
      }
    };
    fetchClassSecData();
  }, [classSec, setValue]);

  const onSubmit = async (data) => {
    try {
      const classSecData = {
        class: data.class._id,
        section: data.section._id,
      };
      let res;
      if (initialClassSec) {
        res = await editData("class_sec", initialClassSec._id, classSecData);
      } else {
        res = await addData("class_sec", classSecData);
      }
      if (res.status === 200) {
        toast.success(res.data.message, toastConfig);
        onSave();
      } else {
        toast.error(res.data.message, toastConfig);
      }
    } catch (error) {
      console.error("Error updating/adding class Sec:", error);
      toast.error(
        error.response?.data?.message || "Failed to update/add class",
        toastConfig
      );
    }
  };

  return (
    <Box
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      sx={{
        display: "flex",
        flexDirection: "column",
        gap: 2,
        maxWidth: 400,
        margin: "0 auto",
      }}
    >
      <Typography variant="h4" gutterBottom>
        {initialClassSec ? "Edit Class Section" : "Add Class Section"}
      </Typography>

      <div>
        <label htmlFor="class">Class</label>
        <Controller
          name="class"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={classes}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search class"
                  onChange={(e) => setClassSearch(e.target.value)}
                  error={!!errors.class}
                  helperText={errors.class?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.class
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>

      <div>
        <label htmlFor="section">Section</label>
        <Controller
          name="section"
          control={control}
          defaultValue={null}
          render={({ field }) => (
            <Autocomplete
              {...field}
              options={sections}
              getOptionLabel={(option) => option.name || ""}
              isOptionEqualToValue={(option, value) =>
                option && value && option._id === value._id
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  placeholder="Search section"
                  onChange={(e) => setSectionSearch(e.target.value)}
                  error={!!errors.section}
                  helperText={errors.section?.message}
                  sx={{
                    "& .MuiInputBase-root": {
                      height: "46px",
                      padding: "0px 9px",
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                      borderRadius: "5px",
                    },
                    "& .MuiOutlinedInput-notchedOutline": {
                      border: "none",
                    },
                    "& .MuiInputBase-root.Mui-focused": {
                      border: errors.section
                        ? "2px solid #ef4444"
                        : "2px solid black",
                    },
                  }}
                />
              )}
              onChange={(_, value) => field.onChange(value)}
            />
          )}
        />
      </div>

      <Button type="submit" variant="contained" color="primary">
        {initialClassSec ? "Update Class Section" : "Add Class Section"}
      </Button>
    </Box>
  );
};

export default ClassAndSectionForm;
