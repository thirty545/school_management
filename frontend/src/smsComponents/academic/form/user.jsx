import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import {
  getDatas,
  editData,
  addData,
  getDataById,
} from "../../../service/Api.js";
import { useNavigate, useLocation, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { toastConfig } from "../../../config/toastConfig.js";
import { loggedInData } from "../../../config/authData.js";

const AddUserForm = () => {
  const [roles, setRoles] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();
  const { id } = useParams();
  const loggedInUser = loggedInData();
  const [initialUser, setInitialUser] = useState(null);

  const validationSchema = Yup.object().shape({
    fname: Yup.string().required("First name is required"),
    lname: Yup.string().required("Last name is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    phone: Yup.number()
      .positive("Phone number must be positive")
      .required("Phone number is required"),
    role: Yup.string().required("Role is required"),
    gender: Yup.string().required("Gender is required"),
    dob: Yup.date()
      .max(new Date(), "Date of birth must not be in the future")
      .required("Date of birth is required"),
    address: Yup.string().required("Address is required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 characters long")
      .required("Password is required"),
    mother_name: Yup.string().required("Mother's name is required"),
    father_name: Yup.string().required("Father's name is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialUser || {},
  });

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const roleData = await getDatas("role", { order: 1 });
        setRoles(roleData.data);
      } catch (error) {
        console.error("Error fetching roles:", error);
      }
    };
    fetchRoles();
  }, []);

  useEffect(() => {
    const fetchUserData = async () => {
      if (id) {
        try {
          const userData = await getDataById("user", id);
          setInitialUser(userData.data);
        } catch (error) {
          console.error("Error fetching user data:", error);
        }
      }
    };
    fetchUserData();
  }, [id]);

  useEffect(() => {
    if (initialUser) {
      Object.keys(initialUser).forEach((key) => {
        if (key === "role" && typeof initialUser[key] === "object") {
          setValue(key, initialUser[key]._id);
        } else {
          setValue(key, initialUser[key]);
        }
      });
    }
  }, [initialUser, setValue]);

  const onSubmit = async (data) => {
    try {
      if (initialUser) {
        await editData("user", initialUser._id, data);
        toast.success("User updated successfully", toastConfig);
      } else {
        await addData("user", data);
        toast.success("User added successfully", toastConfig);
      }
      navigate("/main/user");
    } catch (error) {
      console.error("Error updating/adding user:", error);
      toast.error("Failed to update/add user", toastConfig);
    }
  };

  return (
    <div className="bg-white p-4 m-2 md:p-4 space-y-4 md:space-y-6 overflow-hidden shadow-lg rounded-md">
      {/* upper text */}
      <div className="flex flex-col font-Inter gap-4 md:gap-8">
        <h2 className="text-xl md:text-2xl font-bold text-[#5d2e8e]">
          {initialUser ? "Edit User" : "Application Form For User"}
        </h2>
        <p className="text-sm md:text-base">
          The User form must be filled by the teacher to be the member of our
          school.
        </p>
      </div>
      {/* upload */}
      <div className="flex flex-col font-Inter justify-center rounded-lg items-center py-5 bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC]">
        <label className="font-extrabold mb-2" htmlFor="photo">
          Photo of the Student
        </label>
        <div className="flex flex-col items-center justify-center px-7 py-7  bg-gray-50 rounded-2xl border-[#5d2e8e] shadow-md shadow-[#e9cdcd] gap-3 border-2 max-w-sm">
          <div className="grid gap-1">
            <h2 className="text-center font-bold text-[#5d2e8e] font-Rounded text-base md:text-lg leading-4">
              Upload PDF/Doc
            </h2>
          </div>
          <label>
            <input type="file" hidden />
            <div className="flex w-28 h-9 px-2 flex-col bg-indigo-600 rounded-full shadow text-white text-xs font-semibold leading-4 items-center justify-center cursor-pointer focus:outline-none">
              Choose File
            </div>
          </label>
        </div>
      </div>
      {/* form start */}
      <form onSubmit={handleSubmit(onSubmit)} className="w-full space-y-4">
        <label className="font-extrabold font-Inter" htmlFor="Student Details">
          Student Details
        </label>
        <div className="bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC] px-4 md:px-10 py-6 md:py-10 rounded-lg space-y-4">
          <div className="flex flex-col md:flex-row justify-between gap-4">
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="fname"
              >
                First Name
              </label>
              <Controller
                name="fname"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                    type="text"
                  />
                )}
              />
              {errors.fname && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.fname.message}
                </p>
              )}
            </div>
            {/* fname*/}
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="lname"
              >
                Last Name
              </label>
              <Controller
                name="lname"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                    type="text"
                  />
                )}
              />
              {errors.lname && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.lname.message}
                </p>
              )}
            </div>
            {/* lname*/}
          </div>
          <div className="flex flex-col md:flex-row justify-between gap-4">
            <div className="flex flex-col w-full md:w-1/2 ">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="Gender"
              >
                Gender
              </label>
              <Controller
                name="gender"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <select
                    {...field}
                    className="rounded-md outline-none px-1 py-3 text-base md:text-lg w-full"
                  >
                    <option value="">Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
                )}
              />
              {errors.gender && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.gender.message}
                </p>
              )}
            </div>
            {/*gender*/}
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="dob"
              >
                Date of Birth
              </label>
              <Controller
                name="dob"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="rounded-md outline-none px-1 py-2 text-base md:text-lg w-full"
                    type="date"
                  />
                )}
              />
              {errors.dob && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.dob.message}
                </p>
              )}
            </div>
            {/*dob*/}
          </div>
          {/*flex*/}
          <div className="flex flex-col md:flex-row justify-between gap-4">
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="Address"
              >
                Address
              </label>
              <Controller
                name="address"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="rounded-md outline-none px-1 py-2 text-base md:text-lg w-full"
                    rows="3"
                  />
                )}
              />
              {errors.address && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.address.message}
                </p>
              )}
            </div>
            {/*address*/}
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="password"
              >
                Password
              </label>
              <Controller
                name="password"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    type="password"
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                  />
                )}
              />
              {errors.password && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.password.message}
                </p>
              )}
            </div>
            {/*password*/}
          </div>
          <div className="flex flex-col md:flex-row justify-between gap-4">
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="email"
              >
                Email Address
              </label>
              <Controller
                name="email"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                  />
                )}
              />
              {errors.email && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.email.message}
                </p>
              )}
            </div>
            {/*email*/}
            <div className="flex flex-col w-full md:w-1/2">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="phone"
              >
                Phone Number
              </label>
              <Controller
                name="phone"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <input
                    {...field}
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                    type="tel"
                  />
                )}
              />
              {errors.phone && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.phone.message}
                </p>
              )}
            </div>
            {/*phone*/}
          </div>
          <div className="flex flex-col md:flex-row justify-between gap-4">
            <div className="flex flex-col w-full">
              <label
                className="text-base md:text-lg font-semibold font-Inter"
                htmlFor="role"
              >
                Role
              </label>
              <Controller
                name="role"
                control={control}
                defaultValue=""
                render={({ field }) => (
                  <select
                    {...field}
                    className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                  >
                    <option value="">Select a role</option>
                    {roles.map((role) => (
                      <option key={role._id} value={role._id}>
                        {role.name}
                      </option>
                    ))}
                  </select>
                )}
              />
              {errors.role && (
                <p className="mt-1 text-sm text-red-600">
                  {errors.role.message}
                </p>
              )}
            </div>
            {/*role*/}
          </div>
        </div>
        <div>
          <label
            className="font-extrabold font-Inter"
            htmlFor="Parent/Guardian"
          >
            Parent/Guardian
          </label>
          <div className="bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC] px-4 md:px-10 py-6 md:py-10 rounded-lg space-y-4">
            <div className="flex flex-col md:flex-row justify-between gap-4">
              <div className="flex flex-col w-full md:w-1/2">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="mother_name"
                >
                  mother_name
                </label>
                <Controller
                  name="mother_name"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="text"
                    />
                  )}
                />
                {errors.fname && (
                  <p className="mt-1 text-sm text-red-600">
                    {errors.mother_name.message}
                  </p>
                )}
              </div>
              {/* mother_name*/}
              <div className="flex flex-col w-full md:w-1/2">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="father_name"
                >
                  Father_name
                </label>
                <Controller
                  name="father_name"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="text"
                    />
                  )}
                />
                {errors.lname && (
                  <p className="mt-1 text-sm text-red-600">
                    {errors.father_name.message}
                  </p>
                )}
              </div>
              {/* fatherName*/}
            </div>
          </div>
        </div>
        <div className="flex justify-end">
          <button
            type="submit"
            className="bg-[#5d2e8e] hover:bg-purple-800 text-base md:text-lg font-Inter font-medium text-white px-4 md:px-5 py-2 rounded-2xl"
          >
            {initialUser ? "Update User" : "Add User"}
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddUserForm;
