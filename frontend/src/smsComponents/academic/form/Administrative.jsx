import React, { useState, useEffect } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { Box } from "@mui/material";
import { useNavigate, useLocation, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import {
  getDatas,
  getDataById,
  editData,
  addData,
} from "../../../service/Api.js";
import { loggedInData } from "../../../config/authData.js";
import { toastConfig } from "../../../config/toastConfig.js";

const Administrative = () => {
  const [roles, setRoles] = useState([]);
  const navigate = useNavigate();
  const location = useLocation();
  const { id } = useParams();
  const loggedInUser = loggedInData();
  const [initialUser, setInitialUser] = useState(null);

  const validationSchema = Yup.object().shape({
    fname: Yup.string().required("First name is required"),
    lname: Yup.string().required("Last name is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Email is required"),
    phone: Yup.number()
      .positive("Phone number must be positive")
      .required("Phone number is required"),
    role: Yup.string().required("Role is required"),
    gender: Yup.string().required("Gender is required"),
    dob: Yup.date()
      .max(new Date(), "Date of birth must not be in the future")
      .required("Date of birth is required"),
    address: Yup.string().required("Address is required"),
    password: Yup.string()
      .min(6, "Password must be at least 6 characters long")
      .required("Password is required"),
    isActive: Yup.boolean().required("Active status is required"),
  });

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(validationSchema),
    defaultValues: initialUser || {},
  });

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const roleData = await getDatas("role", { order: 1 });
        setRoles(roleData.data);
      } catch (error) {
        console.error("Error fetching roles:", error);
      }
    };
    fetchRoles();
  }, []);

  useEffect(() => {
    const fetchUserData = async () => {
      if (id) {
        try {
          const userData = await getDataById("user", id);
          setInitialUser(userData.data);
        } catch (error) {
          console.error("Error fetching user data:", error);
        }
      }
    };
    fetchUserData();
  }, [id]);

  useEffect(() => {
    if (initialUser) {
      Object.keys(initialUser).forEach((key) => {
        if (key === "role" && typeof initialUser[key] === "object") {
          setValue(key, initialUser[key]._id);
        } else {
          setValue(key, initialUser[key]);
        }
      });
    }
  }, [initialUser, setValue]);

  const onSubmit = async (data) => {
    try {
      if (initialUser) {
        await editData("user", initialUser._id, data);
        toast.success("User updated successfully", toastConfig);
      } else {
        await addData("user", data);
        toast.success("User added successfully", toastConfig);
      }
      navigate("/main/user");
    } catch (error) {
      console.error("Error updating/adding user:", error);
      toast.error("Failed to update/add user", toastConfig);
    }
  };

  return (
    <Box component="form" onSubmit={handleSubmit(onSubmit)}>
      <div className="bg-white p-4 m-6 md:p-10 space-y-4 md:space-y-6 overflow-hidden shadow-lg rounded-md">
        <div className="flex flex-col font-Inter gap-4 md:gap-8">
          <h1 className="text-xl md:text-2xl font-bold text-[#5d2e8e]">
            Application Form For User
          </h1>
          <p className="text-sm md:text-base">
            The User form must be filled by the teacher to be the member of our
            school.
          </p>
        </div>
        <div className="flex flex-col font-Inter justify-center rounded-lg items-center py-5 bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC]">
          <label className="font-extrabold mb-2" htmlFor="photo">
            Photo of the Student
          </label>
          <div className="flex flex-col items-center justify-center px-7 py-7  bg-gray-50 rounded-2xl border-[#5d2e8e] shadow-md shadow-[#e9cdcd] gap-3 border-2 max-w-sm">
            <div className="grid gap-1">
              <h2 className="text-center font-bold text-[#5d2e8e] font-Rounded text-base md:text-lg leading-4">
                Upload PDF/Doc
              </h2>
            </div>
            <label>
              <input type="file" hidden />
              <div className="flex w-28 h-9 px-2 flex-col bg-indigo-600 rounded-full shadow text-white text-xs font-semibold leading-4 items-center justify-center cursor-pointer focus:outline-none">
                Choose File
              </div>
            </label>
          </div>
        </div>
        <div>
          <label
            className="font-extrabold font-Inter"
            htmlFor="Student Details"
          >
            Student Details
          </label>
          <div className="bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC] px-4 md:px-10 py-6 md:py-10 rounded-lg space-y-4">
            <div className="flex flex-col md:flex-row justify-between gap-4">
              <div className="flex flex-col w-full md:w-1/2">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="fName"
                >
                  First Name
                </label>
                <Controller
                  name="fname"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="text"
                    />
                  )}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/2">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="lName"
                >
                  Last Name
                </label>
                <Controller
                  name="lname"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="text"
                    />
                  )}
                />
              </div>
            </div>
            {/*complite*/}
            <div className="flex flex-col md:flex-row justify-between gap-4">
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="Gender"
                >
                  Gender
                </label>
                <Controller
                  name="gender"
                  control={control}
                  render={({ field }) => (
                    <select
                      {...field}
                      className="rounded-md outline-none px-1 py-3 text-base md:text-lg w-full"
                    >
                      <option value="">Choose an option</option>
                      <option value="male">Male</option>
                      <option value="female">Female</option>
                      <option value="other">Other</option>
                    </select>
                  )}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="dob"
                >
                  Date of Birth
                </label>
                <Controller
                  name="dob"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-3 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="date"
                    />
                  )}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="Address"
                >
                  Address
                </label>
                <Controller
                  name="address"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="text"
                    />
                  )}
                />
              </div>
            </div>
          </div>
        </div>
        <div>
          <label
            className="font-extrabold font-Inter"
            htmlFor="Parent/Guardian"
          >
            Parent/Guardian
          </label>
          <div className="bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC] px-4 md:px-10 py-6 md:py-10 rounded-lg space-y-4">
            <div className="flex flex-col md:flex-row justify-between gap-4">
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="email"
                >
                  Email Address
                </label>
                <Controller
                  name="email"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="email"
                    />
                  )}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="phone"
                >
                  Phone Number
                </label>
                <Controller
                  name="phone"
                  control={control}
                  render={({ field }) => (
                    <input
                      {...field}
                      className="px-1 py-2 text-base md:text-xl rounded-md outline-none w-full"
                      type="tel"
                    />
                  )}
                />
              </div>
              <div className="flex flex-col w-full md:w-1/3">
                <label
                  className="text-base md:text-lg font-semibold font-Inter"
                  htmlFor="role"
                >
                  Role
                </label>
                <Controller
                  name="role"
                  control={control}
                  render={({ field }) => (
                    <select
                      {...field}
                      className="rounded-md outline-none px-1 py-3 text-base md:text-lg w-full"
                    >
                      <option value="">Choose a Role</option>
                      {roles.map((role) => (
                        <option key={role._id} value={role._id}>
                          {role.name}
                        </option>
                      ))}
                    </select>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="flex justify-end">
          <button
            className="bg-[#5d2e8e] hover:bg-purple-800 text-base md:text-lg font-Inter font-medium text-white px-4 md:px-5 py-2 rounded-2xl"
            type="submit"
          >
            {initialUser ? "Update User" : "Add User"}
          </button>
        </div>
      </div>
    </Box>
  );
};

export default Administrative;
