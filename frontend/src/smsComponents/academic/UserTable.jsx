import React, { useState, useEffect } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  CircularProgress,
  TextField,
  MenuItem,
  Button,
  Box,
  Typography,
  TableSortLabel,
} from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import AddIcon from "@mui/icons-material/Add";

import { getDatas, getItemsByRole } from "../../service/Api.js";
import { useNavigate } from "react-router-dom";
import ActionButtons from "../../components/ActionButtons.js";
import { loggedInData } from "../../config/authData.js";

const UserTable = () => {
  const [totalRows, setTotalRows] = useState(0);
  const [orderBy, setOrderBy] = useState("fname");
  const [order, setOrder] = useState("asc");
  const [users, setUsers] = useState([]);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);
  const [loading, setLoading] = useState(false);
  const [searchField, setSearchField] = useState("");
  const [role, setRole] = useState("");
  const navigate = useNavigate();
  const loggedInUser = loggedInData();
  const [searchBy, setSearchBy] = useState("fname");
  const [roles, setRoles] = useState([]);
  const [userSearch, setUserSearch] = useState("");

  const fetchUsers = async () => {
    try {
      const studentData = await getItemsByRole("user", role, userSearch);
      setUsers(studentData.data);
    } catch (error) {
      console.error("Error fetching Users:", error);
    }
  };

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const roleData = await getDatas("role", { order: 1 });
        setRoles(roleData.data);
      } catch (error) {
        console.error("Error fetching roles:", error);
      }
    };
    fetchRoles();
  }, []);

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const handleRequestSort = (property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
    setPage(0); // Reset to first page when sorting
  };

  const handleRowsPerPageChange = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearchFieldChange = (event) => {
    setSearchField(event.target.value);
  };

  const handleRoleChange = (event) => {
    setRole(event.target.value);
    console.log("Selected Role:", role);
  };

  const handleSearchUsers = () => {
    setPage(0);
    fetchUsers();
  };

  const handleAddUser = () => {
    navigate("/main/user/add");
  };

  const handleEditUser = (user) => {
    navigate(`/users/edit/${user._id}`, { state: { user } });
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <Box sx={{ p: 2, backgroundColor: "#f9f9f9" }}>
        <Typography variant="h5" gutterBottom>
          Users
        </Typography>
        <Typography variant="body1">
          This table displays all users in the system. You can search, add,
          edit, and manage users from here.
        </Typography>
      </Box>
      <Box sx={{ borderBottom: "1px solid #e0e0e0" }} />
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          p: 2,
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            size="small"
            value={searchField}
            onChange={handleSearchFieldChange}
            sx={{ mr: 2 }}
          />
          <TextField
            label="Role"
            variant="outlined"
            size="small"
            value={role}
            onChange={handleRoleChange}
            select
            sx={{ mr: 2, width: 125 }}
          >
            {roles.map((roleItem) => (
              <MenuItem key={roleItem._id} value={roleItem.name}>
                {roleItem.name}
              </MenuItem>
            ))}
          </TextField>
          <Button
            variant="contained"
            sx={{
              backgroundColor: "#5D2E8E",
              "&:hover": { backgroundColor: "#4A2470" },
            }}
            onClick={handleSearchUsers}
            startIcon={<SearchIcon />}
          >
            Search
          </Button>
        </Box>

        <Button
          variant="contained"
          sx={{
            backgroundColor: "#5D2E8E",
            "&:hover": { backgroundColor: "#4A2470" },
          }}
          onClick={handleAddUser}
          startIcon={<AddIcon />}
        >
          Add User
        </Button>
      </Box>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              {["fname", "email", "phone", "role", "gender", "dob"].map(
                (headCell) => (
                  <TableCell key={headCell}>
                    <TableSortLabel
                      active={orderBy === headCell}
                      direction={orderBy === headCell ? order : "asc"}
                      onClick={() => handleRequestSort(headCell)}
                    >
                      {headCell.charAt(0).toUpperCase() + headCell.slice(1)}
                    </TableSortLabel>
                  </TableCell>
                )
              )}
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  <CircularProgress />
                </TableCell>
              </TableRow>
            ) : users.length === 0 ? (
              <TableRow>
                <TableCell colSpan={8} align="center">
                  No users found.
                </TableCell>
              </TableRow>
            ) : (
              users.map((user, index) => (
                <TableRow
                  key={user._id}
                  sx={{
                    backgroundColor: index % 2 === 0 ? "#ffffff" : "#f3f3f3",
                  }}
                >
                  <TableCell>
                    {user.fname} {user.lname}
                  </TableCell>
                  <TableCell>{user.email}</TableCell>
                  <TableCell>{user.phone}</TableCell>
                  <TableCell>{user.role?.name || "N/A"}</TableCell>
                  <TableCell>{user.gender || "N/A"}</TableCell>
                  <TableCell>
                    {user.dob ? new Date(user.dob).toLocaleDateString() : "N/A"}
                  </TableCell>
                  <TableCell>
                    <ActionButtons
                      item_id={user._id}
                      formNavigate="users"
                      getData={fetchUsers}
                      handleEditUser={() => handleEditUser(user)}
                    />
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={users.length} // Use the actual total number of rows
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
    </Paper>
  );
};

export default UserTable;
