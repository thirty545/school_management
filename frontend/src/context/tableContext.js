import React, { createContext, useContext, useState } from "react";

const TableContext = createContext();

export const useTableContext = () => {
  return useContext(TableContext);
};

export const TableProvider = ({ children }) => {
  const [tableState, setTableState] = useState({
    currentPage: 0,
    limit: 5,
    order: "asc",
    orderBy: "createdAt",
    s_field: "",
    s_query: "",
  });

  const contextValue = {
    tableState,
    setTableState,
  };

  return (
    <TableContext.Provider value={contextValue}>
      {children}
    </TableContext.Provider>
  );
};
