import React, { useState, useEffect, useCallback } from "react";
import {
  AppBar,
  Toolbar,
  Box,
  IconButton,
  TextField,
  Typography,
  Select,
  MenuItem,
  Menu,
  Button,
} from "@mui/material";
import { Menu as MenuIcon, Notifications } from "@mui/icons-material";
import { IoIosNotificationsOutline } from "react-icons/io";
import { CiUser, CiCircleQuestion } from "react-icons/ci";
import { VscAccount } from "react-icons/vsc";
import { LuUser, LuUserPlus, LuLogOut } from "react-icons/lu";
import { AiOutlinePieChart } from "react-icons/ai";
import { IoLockClosedOutline } from "react-icons/io5";
import { IoMdSettings } from "react-icons/io";
import PopUp from "./customs/PopUP";
import { getDatas } from "../service/Api";
import SessionForm from "../smsComponents/academic/form/academicYear"; // Ensure this component exists

const Navbar = ({ isOpen, drawerWidth, handleDrawerToggle, schoolData }) => {
  const [buttonPopup, setButtonPopup] = useState(false);
  const [sessions, setSessions] = useState([]);
  const [selectedSession, setSelectedSession] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const [sessionSearch, setSessionSearch] = useState("");
  const [sessionPopupOpen, setSessionPopupOpen] = useState(false);

  const fetchSessions = useCallback(async () => {
    console.log("after adding the session fetchSessions runs");
    try {
      // Fetch sessions data from the API
      const sessionData = await getDatas("session", sessionSearch);

      // Debug the API response
      // console.log("API response:", sessionData);

      if (sessionData.data && sessionData.data.length > 0) {
        // Sort sessions by created date
        const sortedSessions = sessionData.data.sort(
          (a, b) => new Date(b.createdAt) - new Date(a.createdAt)
        );

        // Update state
        setSessions(sortedSessions);
        console.log("sortedSessions is", sortedSessions);

        // Check if there are sessions to select
        if (sortedSessions.length > 0) {
          console.log("we are choosing the ");
          setSelectedSession(sortedSessions[sortedSessions.length - 1].name);
        } else {
          setSelectedSession("");
        }

        // Debug sorted sessions and selected session
        console.log("Sorted sessions:", sortedSessions);
        console.log(
          "Selected session:",
          sortedSessions.length > 0 ? sortedSessions[0].id : "None"
        );
      } else {
        // Handle case with no sessions
        setSessions([]);
        setSelectedSession("");

        // Debug when no sessions are found
        // console.log("No sessions found.");
      }
    } catch (error) {
      // Log errors
      console.error("Error fetching sessions:", error);
    }
  }, []);

  useEffect(() => {
    console.log("before running fetchSessions");
    fetchSessions();
  }, [fetchSessions]);

  useEffect(() => {
    if (sessions.length > 0) {
      setSelectedSession(sessions[0].id);
    }
  }, [sessions]);

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleSessionChange = (event) => {
    event.preventDefault();
    console.log("hanleSessionChange function called");
    console.log("Session changed to:", event.target.value);
    setSelectedSession(event.target.value);
  };

  const handleAddSession = () => {
    setSessionPopupOpen(true);
  };

  const handleCloseSessionPopup = () => {
    setSessionPopupOpen(false);
  };

  const handleSaveSession = async () => {
    try {
      const sessionData = await getDatas("session", {});
      setSessions(sessionData.data);
      if (sessionData.data.length > 0) {
        setSelectedSession(sessionData.data[0].id);
      }
    } catch (error) {
      console.error("Error fetching sessions:", error);
    }
    handleCloseSessionPopup();
  };

  console.log("sessions is", sessions);
  console.log("selectedSession is", selectedSession);

  return (
    <AppBar
      position="fixed"
      open={isOpen}
      sx={{
        width: `calc(100% - ${drawerWidth}px)`,
        ml: isOpen ? `${drawerWidth}px` : 0,
        background: "#c4a6f8",
        boxShadow: "none",
        color: "#333",
      }}
    >
      <Toolbar
        sx={{
          pr: "24px",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Box sx={{ display: "flex", alignItems: "center" }}>
          {/* <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerToggle}
            sx={{
              marginRight: 5,
            }}
          >
            <MenuIcon />
          </IconButton> */}
          <TextField
            type="search"
            size="small"
            placeholder="Search here..."
            sx={{ borderRadius: "50%", ml: 1, }}
          />
        </Box>

        <Box sx={{ display: "flex", alignItems: "center" }}>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              gap: 2,
            }}
          >
            <div className="relative">
              <select
                key={sessions.length}
                defaultValue={selectedSession}
                onChange={handleSessionChange}
                className="h-[45px] pl-2 py-2 text-gray-600 bg-transparent border border-black rounded-md  appearance-none  focus:ring-1 focus:ring-black focus:outline-none"
              >
                <option value="" disabled>
                  Academic Year
                </option>
                {sessions.map((session) => (
                  <option key={session.id} value={session.name}>
                    {session.name}
                  </option>
                ))}
              </select>
              <div className="absolute inset-y-0 right-0 flex items-center px-2 pointer-events-none">
                <svg className="w-4 h-4 fill-current" viewBox="0 0 20 20">
                  <path
                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                    clipRule="evenodd"
                    fillRule="evenodd"
                  ></path>
                </svg>
              </div>
            </div>

            <Button
              variant="contained"
              size="small"
              onClick={handleAddSession}
              sx={{
                backgroundColor: "#5D2E8E",
                "&:hover": { backgroundColor: "#4A2470" },
                height: "45px",
              }}
            >
              Add Session
            </Button>
            <Typography variant="h6" sx={{ fontWeight: "bold" }}>
              {schoolData ? schoolData.name : "School not found"}
            </Typography>
          </Box>

          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="open drawer"
            sx={{ ml: 2 }}
          >
            <IoIosNotificationsOutline />
          </IconButton>
          <IconButton color="inherit" size="large" onClick={handleMenuOpen}>
            <CiUser />
          </IconButton>
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleMenuClose}
            PaperProps={{
              elevation: 0,
              sx: {
                overflow: "visible",
                filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                mt: 1.5,
                "& .MuiAvatar-root": {
                  width: 32,
                  height: 32,
                  ml: -0.5,
                  mr: 1,
                },
                "&:before": {
                  content: '""',
                  display: "block",
                  position: "absolute",
                  top: 0,
                  right: 14,
                  width: 10,
                  height: 10,
                  bgcolor: "background.paper",
                  transform: "translateY(-50%) rotate(45deg)",
                  zIndex: 0,
                },
              },
            }}
            transformOrigin={{ horizontal: "right", vertical: "top" }}
            anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
          >
            <div
              className={`rounded-3xl${Boolean(anchorEl) ? "block" : "hidden"}`}
              style={{
                transformOrigin: "top right",
              }}
            >
              <div
                className="flex flex-col overflow-hidden gap-6 py-1 px-5"
                role="menu"
                aria-orientation="vertical"
              >
                <div className="flex flex-col gap-6 items-center">
                  <VscAccount size={90} />
                  <input
                    type="search"
                    placeholder="Update your status"
                    className="px-2 border-[2px] border-black w-[18rem] rounded-md outline-none py-1"
                  />
                </div>
                <div className="flex flex-col items-start gap-4 ">
                  <div className="flex items-center">
                    <LuUser size={30} />
                    <a
                      href="#"
                      className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Profile
                    </a>
                  </div>
                  <div className="flex items-center">
                    <AiOutlinePieChart size={30} />
                    <a
                      href="/"
                      className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Dashboard
                    </a>
                  </div>
                  <div className="flex items-center">
                    <IoLockClosedOutline size={30} />
                    <a
                      href="#"
                      className="block pl-2 pt-1 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Profile and Activity
                    </a>
                  </div>
                  <div className="flex items-center">
                    <IoMdSettings size={30} />
                    <a
                      href=""
                      className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Settings and Privacy
                    </a>
                  </div>
                  <div className="flex items-center">
                    <CiCircleQuestion size={30} />
                    <a
                      href="#"
                      className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Help Centre
                    </a>
                  </div>
                  <div className="border-t border-gray-400 w-full"></div>
                  <div className="flex items-center">
                    <LuUserPlus size={30} />
                    <a
                      href="#"
                      className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                      onClick={handleMenuClose}
                    >
                      Add Another Account
                    </a>
                  </div>
                  <div className="border-t border-gray-400 w-full"></div>
                </div>
                <div className="flex items-center">
                  <LuLogOut size={30} />
                  <a
                    href="/logout"
                    className="block pl-2 text-base text-gray-700 hover:text-gray-400"
                    onClick={handleMenuClose}
                  >
                    Sign Out
                  </a>
                </div>
                <div className="flex justify-between px-10 py-2 text-xs text-gray-400">
                  <span>Privacy Policy</span>
                  <span>•</span>
                  <span>Terms</span>
                  <span>•</span>
                  <span>Cookies</span>
                </div>
              </div>
            </div>
          </Menu>

          <PopUp trigger={buttonPopup} setTrigger={setButtonPopup} />
        </Box>
      </Toolbar>
      <PopUp open={sessionPopupOpen} onClose={handleCloseSessionPopup}>
        <SessionForm
          onSave={handleSaveSession}
          onClose={handleCloseSessionPopup}
          fetchSessions={fetchSessions}
        />
      </PopUp>
    </AppBar>
  );
};

export default Navbar;
