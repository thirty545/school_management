import React, { useState } from "react";
import { Link } from "react-router-dom";
import { ExpandLess, ExpandMore, KeyboardArrowRight } from "@mui/icons-material";
import SchoolOutlinedIcon from "@mui/icons-material/SchoolOutlined";
import { menus } from "../config/menus";
import "../css/scrollable.css"

const MenuItem = ({
  name,
  icon,
  link,
  submenus,
  activeMenu,
  setActiveMenu,
}) => {
  const isActive = activeMenu === link || activeMenu?.startsWith(link + "/");

  const handleClick = (e) => {
    if (submenus) {
      e && e.preventDefault();
      setActiveMenu(isActive ? null : link);
    } else {
      setActiveMenu(link);
    }
  };

  return (
    <>
      <Link
        to={link}
        onClick={handleClick}
        className={`flex justify-between items-center text-black rounded-lg m-1 h-10 transition-all duration-300 ease-in-out ${
          isActive ? "bg-white px-1" : "hover:bg-white hover:bg-opacity-10"
        }`}
      >
        <div className="flex items-center pl-3">
          <span className="text-inherit transition-transform duration-300 ease-in-out transform hover:scale-110">
            {icon}
          </span>
          <span className=" font-semibold">{name}</span>
        </div>
        {submenus && (
          <button
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              handleClick();
            }}
            className="p-1 transition-transform duration-300 ease-in-out"
          >
            {isActive ? (
              <ExpandLess className="transform rotate-180" />
            ) : (
              <KeyboardArrowRight />
            )}
          </button>
        )}
      </Link>

      {submenus && (
        <div
          className={`pl-2  text-[0.90rem] font-light overflow-hidden transition-all duration-300 ease-in-out ${
            isActive ? "max-h-auto" : "max-h-0"
          }`}
        >
          {submenus.map((submenu) => (
            <MenuItem
              key={submenu.name}
              {...submenu}
              link={`${link}/${submenu.link}`}
              activeMenu={activeMenu}
              setActiveMenu={setActiveMenu}
            />
          ))}
        </div>
      )}
    </>
  );
};

export default function Menu({ schoolData }) {
  const [activeMenu, setActiveMenu] = useState(null);

  return (
    <div className="scrollable bg-gradient-to-b from-[#C19CE5] to-[#F7EFEC] min-h-screen h-full font-poppins overflow-auto">
      <div className="flex flex-col h-full">
        <div className="p-5 text-center">
          {schoolData?.logo ? (
            <img
              src={schoolData.logo}
              alt="School Logo"
              className="max-w-full h-auto school-logo transition-transform duration-300 ease-in-out transform hover:scale-105"
            />
          ) : (
            <div className="flex justify-center items-center w-full h-24 bg-black rounded-lg transition-transform duration-300 ease-in-out transform hover:scale-105">
              <SchoolOutlinedIcon className="text-6xl text-blue-600 w-1/2 max-w-[100px] transition-all duration-300 ease-in-out hover:text-blue-400" />
            </div>
          )}
        </div>
        <ul className="py-1 px-1 w-full">
          {menus.map((menu, index) => (
            <MenuItem
              key={index}
              {...menu}
              activeMenu={activeMenu}
              setActiveMenu={setActiveMenu}
            />
          ))}
        </ul>
      </div>
    </div>
  );
}
