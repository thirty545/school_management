import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

function ChildComponent() {
  const { schoolname, session } = useParams();
  const [data, setData] = useState(null);

  useEffect(() => {
    // Fetch data from backend
    const fetchSchool = useCallback(async () => {
      setLoading(true);
      try {
        const { data, totalPages } = await getDatas("classRef", {
          limit: rowsPerPage,
          currentPage: page,
          orderBy: "createdAt",
          order: -1,
          s_field: searchBy,
          s_query: searchField,
        });
        setClasses(data);
        setTotalPages(totalPages);
      } catch (error) {
        console.error("Error fetching classes:", error);
      } finally {
        setLoading(false);
      }
    }, [page, rowsPerPage, searchBy, searchField]);
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `/api/data?schoolname=${schoolname}&session=${session}`
        );
        setData(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchData();
  }, [schoolname, session]);

  if (!data) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>School: {data.schoolname}</h1>
      <h2>Session: {data.session}</h2>
      {/* Render the rest of your component */}
    </div>
  );
}

export default ChildComponent;
