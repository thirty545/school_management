import React, { useState } from "react";
import { Box, Drawer, IconButton, Modal } from "@mui/material";
import { Delete, Edit } from "@mui/icons-material";
import { deleteData } from "../service/Api";
import DeleteConfirm from "./DeleteConfirm";
import CustomForm from "./customs/CustomForm";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";

export default function ActionButtons({ item_id, formNavigate, getData }) {
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const navigate = useNavigate();
  const formName = formNavigate;

  // Function to handle deletion of an item
  const handleDelete = async () => {
    try {
      let modelName = formName.split("/").pop();
      if (modelName === "students") {
        modelName = "student_class";
      }
      let res = await deleteData(modelName, item_id);
      if (res.status === 200) {
        toast.success("Item deleted successfully!"); // Use toast.success for a positive message
        getData(); // Refresh the data
        setOpenDelete(false); // Close the delete modal
      }
    } catch (error) {
      console.log(error);
      toast.error("Error deleting item!"); // Show an error message
    }
  };

  // Function to navigate to the edit page
  const handleEdit = () => {
    navigate(`/${formName}/edit/${item_id}`);
  };

  return (
    <>
      {/* Edit Button */}
      <IconButton size="small" onClick={handleEdit}>
        <Edit color="info" />
      </IconButton>

      {/* Drawer for edit form */}
      <Drawer
        anchor="right"
        open={openEdit}
        onClose={() => setOpenEdit(false)}
        PaperProps={{
          sx: {
            width: 600,
            padding: 2,
          },
        }}
      >
        <CustomForm
          formName={formName}
          setOpenForm={setOpenEdit}
          getData={getData}
          edit={true}
          itemID={item_id}
        />
      </Drawer>

      {/* Delete Button */}
      <IconButton size="small" onClick={() => setOpenDelete(true)}>
        <Delete color="error" />
      </IconButton>

      {/* Modal for delete confirmation */}
      <Modal open={openDelete} onClose={() => setOpenDelete(false)}>
        <Box
          sx={{
            bgcolor: "white",
            paddingX: 3,
            paddingY: 2,
            borderRadius: 1,
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            boxShadow: "0px 0px 3px #333",
          }}
        >
          <DeleteConfirm setOpen={setOpenDelete} handleDelete={handleDelete} />
        </Box>
      </Modal>
    </>
  );
}
