import {
  IconButton,
  Modal,
  Toolbar,
  Tooltip,
  Typography,
  Box,
} from "@mui/material";
import { Delete } from "@mui/icons-material";
import { alpha } from "@mui/material";
import { useState } from "react";
import { bulkDeleteData } from "../../service/Api";
import DeleteConfirm from "../DeleteConfirm";

export function EnhancedTableToolbar(props) {
  const { selected, setSelected, formName, getData } = props;
  const [open, setOpen] = useState(false);

  const handleBulkDelete = async () => {
    try {
      let res = await bulkDeleteData(formName, selected);
      if (res.status === 200) {
        setOpen(false);
        setSelected([]);
        getData();
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Toolbar
      sx={{
        display: `${selected.length > 0 ? "flex" : "none"}`,
        mt: 1,
        pl: { sm: 2, xl: 0 },
        pr: { xs: 1, sm: 1 },
        ...(selected.length > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity
            ),
        }),
      }}
    >
      {selected.length > 0 && (
        <Typography
          sx={{ flex: "1 1 100%" }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {selected.length} selected
        </Typography>
      )}

      {selected.length > 0 && (
        <Tooltip title="Delete">
          <IconButton onClick={() => setOpen(true)}>
            <Delete />
          </IconButton>
        </Tooltip>
      )}
      <Modal open={open} onClose={() => setOpen(false)}>
        <Box
          sx={{
            bgcolor: "white",
            paddingX: 3,
            paddingY: 2,
            borderRadius: 1,
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            boxShadow: "0px 0px 3px #333",
          }}
        >
          <DeleteConfirm setOpen={setOpen} handleDelete={handleBulkDelete} />
        </Box>
      </Modal>
    </Toolbar>
  );
}
