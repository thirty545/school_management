import { ToastContainer, toast } from "react-toastify";

const CustomToastContainer = () => {
  return (
    <ToastContainer position={toast.POSITION.BOTTOM_RIGHT} theme="colored" />
  );
};

export default CustomToastContainer;
