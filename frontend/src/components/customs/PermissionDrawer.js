import React, { useEffect } from "react";
import {
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { getDatas } from "../../service/Api";

export default function PermissionDrawer() {
  const [data, setData] = React.useState([]);

  const getPermission = async () => {
    let res = await getDatas("role")
    console.log(res);
    setData(data);
  };

  useEffect(() => {
    getPermission();
  }, []);
  return (
    <>
      <Typography variant="body1">Role: Admin</Typography>
      <TableContainer>
        <Table size="small">
          <TableHead>
            <TableRow>
              {["Module", "Read", "Write", "Delete"].map((item) => (
                <TableCell key={item}>{item}</TableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody>
            {data.map((item) => (
              <TableRow key={item._id}>
                <TableCell>{item["module"]?.module_name}</TableCell>
                <TableCell>
                  <Switch defaultChecked={item.read} />
                </TableCell>
                <TableCell>
                  <Switch defaultChecked={item.write} />
                </TableCell>
                <TableCell>
                  <Switch defaultChecked={item.delete} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
