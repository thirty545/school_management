import {
  Box,
  Checkbox,
  TableCell,
  TableHead,
  TableRow,
  TableSortLabel,
} from "@mui/material";
import { visuallyHidden } from "@mui/utils";
import { useTableContext } from "../../context/tableContext";

export function EnhancedTableHead(props) {
  const { onSelectAllClick, numSelected, rowCount, onRequestSort, headCells } =
    props;
  const { tableState } = useTableContext();
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox" sx={{ background: "#efefef" }}>
          <Checkbox
            color="primary"
            size="small"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              "aria-label": "select all desserts",
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.field}
            sortDirection={
              tableState.orderBy === headCell.field ? tableState.order : false
            }
            sx={{ background: "#efefef", fontWeight: "bold" }}
          >
            <TableSortLabel
              active={tableState.orderBy === headCell.field}
              direction={
                tableState.orderBy === headCell.field ? tableState.order : "asc"
              }
              onClick={createSortHandler(headCell.field)}
            >
              {headCell.label}
              {tableState.orderBy === headCell.field ? (
                <Box component="span" sx={visuallyHidden}>
                  {tableState.order === "desc"
                    ? "sorted descending"
                    : "sorted ascending"}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell
          sx={{
            background: "#efefef",
            fontWeight: "bold",
            textAlign: "center",
          }}
        >
          Actions
        </TableCell>
      </TableRow>
    </TableHead>
  );
}
