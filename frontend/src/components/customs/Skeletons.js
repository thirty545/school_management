import { Skeleton, TableCell } from "@mui/material";
import React from "react";

export default function Skeletons({ columns }) {
  return (
    <TableCell colSpan={columns.length + 2} sx={{ fontWeight: "600" }}>
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
      <Skeleton width="100%" height={30} />
    </TableCell>
  );
}
