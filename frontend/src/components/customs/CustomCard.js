import React from "react";
import { Paper, Box, Typography } from "@mui/material";
import { OfflineBolt } from "@mui/icons-material";

export default function CustomCard() {
  return (
    <Paper
      sx={{
        display: "flex",
        justifyContent: "space-between",
        padding: 2,
      }}
    >
      <OfflineBolt sx={{ fontSize: 72, color: "red" }} />
      <Box sx={{ ml: 2 }}>
        <Typography variant="h4" sx={{ fontWeight: "bold" }}>
          42
        </Typography>
        <Typography variant="p">Suraj</Typography>
      </Box>
    </Paper>
  );
}
