import {
  Checkbox,
  FormControl,
  FormControlLabel,
  FormLabel,
  InputLabel,
  MenuItem,
  Radio,
  RadioGroup,
  Select,
  Switch,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import React, { useMemo } from "react";
import { memo } from "react";
const modules = [
  "class",
  "exams",
  "fee",
  "marksheet",
  "role",
  "staff_bio",
  "staff_salary_log",
  "student_bio",
  "student_class",
  "student_fee",
  "subject_class",
  "subject",
  "teacher_class",
  "user",
];

export default function PopulateForm({
  fields = [],
  register,
  errors,
  watch,
  defaultValues,
}) {
  if (!Array.isArray(fields)) {
    console.warn("Fields prop is not an array:", fields);
    return null;
  }
  console.log("fields contains", fields);
  console.log("defaultValues contains", defaultValues);

  function userGenderValue(sentArray) {
    const newGender = sentArray?.find((item) => {
      if (item.value === defaultValues?.gender) {
        console.log("item value contains", item);
        return item;
      }
    })?.label;
    console.log("newGender Value", newGender);
    return newGender || "";
  }

  return (
    <>
      {fields.length > 0 ? (
        fields.map((field) => (
          <React.Fragment key={field.name}>
            {(field.type === "text" ||
              field.type === "email" ||
              field.type === "password" ||
              field.type === "number") && (
              <FormControl fullWidth>
                <InputLabel
                  shrink={true}
                  sx={{
                    background: "white",
                    paddingX: "8px",
                  }}
                >
                  {field.label}
                </InputLabel>
                <TextField
                  variant="outlined"
                  placeholder={field.label}
                  type={field.type}
                  sx={{ mb: 2 }}
                  {...register(field.name, field?.validation)}
                  error={Boolean(errors[field.name])}
                  helperText={errors[field.name]?.message}
                />
              </FormControl>
            )}
            {field.type === "date" && (
              <FormControl fullWidth sx={{ mb: 2 }}>
                <InputLabel
                  shrink={true}
                  sx={{
                    background: "white",
                    paddingX: "8px",
                  }}
                >
                  {field.label}
                </InputLabel>
                <TextField
                  variant="outlined"
                  type={field.type}
                  {...register(field.name, field?.validation)}
                  error={Boolean(errors[field.name])}
                  helperText={errors[field.name]?.message}
                />
              </FormControl>
            )}
            {field.type === "file" && (
              <FormControl fullWidth sx={{ mb: 2 }}>
                <InputLabel
                  shrink={true}
                  sx={{
                    background: "white",
                    paddingX: "8px",
                  }}
                >
                  {field.label}
                </InputLabel>
                <TextField
                  variant="outlined"
                  type={field.type}
                  sx={{ mb: 2 }}
                  {...register(field.name, field?.validation)}
                  error={Boolean(errors[field.name])}
                  helperText={errors[field.name]?.message}
                />
              </FormControl>
            )}
            {field.type === "select" &&
              field.values &&
              field.values.length > 0 && (
                <FormControl variant="outlined" fullWidth sx={{ mb: 2 }}>
                  <InputLabel>{field.label}</InputLabel>
                  <Select
                    label={field.label}
                    defaultValue={
                      field.name === "gender"
                        ? userGenderValue(field.values) || ""
                        : defaultValues[field.name] || ""
                    }
                    {...register(field.name, field?.validation)}
                    error={Boolean(errors[field.name])}
                  >
                    {field.values.map((item) => (
                      // Make sure 'value' here corresponds to the actual 'value' of the field
                      <MenuItem
                        key={item?.value}
                        value={item?.value || "suraj"}
                      >
                        {/* {'userGenderValue returns ' + userGenderValue(field.values)}         */}
                        {item?.label || ""}
                      </MenuItem>
                    ))}
                  </Select>
                  {errors[field.name] && (
                    <Typography variant="body2" color="error">
                      {errors[field.name]?.message}
                    </Typography>
                  )}
                </FormControl>
              )}

            {field.type === "radio" && (
              <React.Fragment key={field.name}>
                <FormLabel id={field.name}>Gender</FormLabel>
                <RadioGroup
                  row={true}
                  defaultValue={field.default}
                  name={field.name}
                  sx={{ mb: 1 }}
                >
                  {field.values.map((item) => (
                    <FormControlLabel
                      key={item.name}
                      value={item.name}
                      control={<Radio />}
                      label={item.label}
                    />
                  ))}
                </RadioGroup>
              </React.Fragment>
            )}
            {field.type === "checkbox" && (
              <FormControlLabel
                control={
                  <Checkbox
                    {...register(field.name, field?.validation)}
                    defaultChecked={field.default}
                  />
                }
                label={field.label}
              />
            )}
            {field.type === "permission" && (
              <React.Fragment key={field.name}>
                <FormLabel id={field.name} sx={{ ml: 2 }}>
                  Permissions
                </FormLabel>
                <>
                  <TableContainer>
                    <Table size="small">
                      <TableHead>
                        <TableRow>
                          {["Module", "Read", "Write", "Delete"].map((item) => (
                            <TableCell key={item}>{item}</TableCell>
                          ))}
                        </TableRow>
                      </TableHead>

                      <TableBody>
                        {modules.map((item) => (
                          <TableRow key={item}>
                            <TableCell>{item}</TableCell>
                            <TableCell>
                              <Switch
                                checked={watch(`${item}-read`) || false}
                                {...register(`${item}-read`)}
                              />
                            </TableCell>
                            <TableCell>
                              <Switch
                                checked={watch(`${item}-write`) || false}
                                {...register(`${item}-write`)}
                              />
                            </TableCell>
                            <TableCell>
                              <Switch
                                checked={watch(`${item}-delete`) || false}
                                {...register(`${item}-delete`)}
                              />
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </>
              </React.Fragment>
            )}
          </React.Fragment>
        ))
      ) : (
        <Typography>No fields to display</Typography>
      )}
    </>
  );
}
