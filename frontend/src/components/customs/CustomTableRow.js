import React from "react";
import { TableRow, TableCell, Checkbox } from "@mui/material";
import ActionButtons from "../ActionButtons";
import Skeletons from "./Skeletons";

const CustomTableRow = ({
  data,
  loading,
  columns,
  isSelected,
  handleClick,
  formName,
  getData,
}) => {
  if (!data || data.length === 0) {
    return (
      <TableRow>
        {loading === true ? (
          <Skeletons columns={columns} />
        ) : (
          <TableCell
            colSpan={columns.length + 2}
            sx={{ textAlign: "center", fontWeight: "600", height: 300 }}
          >
            No data to display
          </TableCell>
        )}
      </TableRow>
    );
  }

  return data.map((item, index) => {
    const isItemSelected = isSelected(item._id);
    const labelId = `enhanced-table-checkbox-${index}`;

    return (
      <TableRow
        hover
        key={item._id.toString()}
        selected={isItemSelected}
        tabIndex={-1}
        sx={{ cursor: "pointer", padding: "4px 0" }}
      >
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            size="small"
            checked={isItemSelected}
            onClick={(event) => handleClick(event, item._id)}
            inputProps={{ "aria-labelledby": labelId }}
          />
        </TableCell>
        {columns.map((column) => (
          <TableCell key={column.field} sx={{ textAlign: "left", py: 0 }}>
            {typeof item[column.field] === "object"
              ? item[column.field]?.grade ||
                item[column.field]?.name ||
                item[column.field]?.fname +
                item[column.field]?.lname ||
                item[column.field]?.dob ||
                item[column.field]?.fee_name ||
                "No Date"
              : item[column.field]}
          </TableCell>
        ))}
        <TableCell sx={{ textAlign: "center", py: 0 }}>
          <ActionButtons
            item_id={item._id}
            formName={formName}
            getData={getData}
          />
        </TableCell>
      </TableRow>
    );
  });
};

export default CustomTableRow;
