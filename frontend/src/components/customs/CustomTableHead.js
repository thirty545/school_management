import React, { useEffect } from "react";
import {
  Box,
  Button,
  Drawer,
  FormControl,
  Select,
  TextField,
  Typography,
  MenuItem,
  InputLabel,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Add } from "@mui/icons-material";
import CustomForm from "./CustomForm";
import { useTableContext } from "../../context/tableContext";

export default function CustomTableHead({
  formName,
  totalRows,
  openForm,
  setOpenForm,
  getData,
  columns,
}) {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));
  const { setTableState } = useTableContext();

  useEffect(() => {
    setTableState((prevState) => ({
      ...prevState,
      s_field: columns[0].field,
    }));
  }, [columns, setTableState]);

  const handleQueryChange = (e) => {
    setTableState((prevState) => ({
      ...prevState,
      s_query: e.target.value,
    }));
  };

  const handleFieldChange = (e) => {
    setTableState((prevState) => ({
      ...prevState,
      s_field: e.target.value,
    }));
  };

  return (
    <Box
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
    >
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box>
          <Typography
            color="primary"
            variant="h6"
            component="span"
            sx={{ fontWeight: "600", textTransform: "capitalize" }}
          >
            {formName} Table
          </Typography>
          <Typography color="gray" variant="body2">
            {totalRows} total {formName.toLowerCase()}
          </Typography>
        </Box>
        <Box sx={{ ml: 3 }}>
          <FormControl
            component="form"
            sx={{ display: "flex", flexDirection: "row" }}
          >
            <InputLabel id="search-by-label">Search By</InputLabel>
            <Select
              label="Search By"
              size="small"
              defaultValue={columns[0]?.field || ""}
              sx={{ width: "150px" }}
              onChange={handleFieldChange}
            >
              {columns.map((item) => (
                <MenuItem key={item?.field} value={item?.field || ""}>
                  {item?.label || ""}
                </MenuItem>
              ))}
            </Select>
            <TextField
              size="small"
              name="s_query"
              type="text"
              variant="outlined"
              placeholder="Enter text to search..."
              onChange={handleQueryChange}
              sx={{ ml: 2 }}
            />
          </FormControl>
        </Box>
      </Box>

      <Box>
        <Button
          variant="outlined"
          startIcon={<Add />}
          size="large"
          onClick={() => setOpenForm(true)}
        >
          Add
        </Button>
        <Drawer
          anchor="right"
          open={openForm}
          onClose={() => setOpenForm(false)}
          PaperProps={{
            sx: {
              width: isMobile ? "100%" : 600,
              padding: 2,
            },
          }}
        >
          <CustomForm
            formName={formName}
            setOpenForm={setOpenForm}
            getData={getData}
          />
        </Drawer>
      </Box>
    </Box>
  );
}
