import React, { useCallback, useEffect, useState } from "react";
import {
  TableContainer,
  Table,
  TableBody,
  TablePagination,
  Paper,
} from "@mui/material";
import { getDatas } from "../../service/Api";
import { EnhancedTableToolbar } from "./EnhancedTableToolbar";
import { EnhancedTableHead } from "./EnhancedTableHead";
import { updateSelected } from "../Utils";
import { useQuery } from "@tanstack/react-query";
import CustomTableHead from "./CustomTableHead";
import CustomTableRow from "./CustomTableRow";
import { useTableContext } from "../../context/tableContext";

export default function CustomTable({ columns = [], formName = "Form Name" }) {
  const { tableState, setTableState } = useTableContext();

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [totalRows, setTotalRows] = useState(0);
  const [selected, setSelected] = useState([]);

  const response = useQuery({
    queryKey: [formName, tableState],
    queryFn: () => getDatas(formName, tableState),
    staleTime: Infinity,
  });

  useEffect(() => {
    if (response.isSuccess) {
      setData(response.data.data);
      setTotalRows(response.data.totalRows);
    }
    setLoading(response.isLoading);
  }, [response.isSuccess, response.data, response.isLoading]);


    
  const getData = () => {};

  // useEffect(() => {
  //   getData();
  // }, [limit, currentPage, orderBy, order, s_query]);

  // const getData = async () => {
  //   try {
  //     setLoading(true);

  //     const response = await getDatas(
  //       formName,
  //       limit,
  //       currentPage,
  //       orderBy,
  //       order,
  //       s_field,
  //       s_query
  //     );
  //     setData(response.data);
  //     setTotalRows(response.totalRows);
  //     setLoading(false);
  //   } catch (err) {
  //     setLoading(false);
  //   }
  // };

  // sorting logics
  const handleRequestSort = (event, property) => {
    const isAsc = tableState.orderBy === property && tableState.order === "asc";
    setTableState((prevState) => ({
      ...prevState,
      order: isAsc ? "desc" : "asc",
      orderBy: property,
    }));
  };

  const handleClick = useCallback(
    (event, id) => {
      const newSelected = updateSelected(selected, id);
      setSelected(newSelected);
    },
    [selected]
  );

  const handleSelectAllClick = useCallback(
    (event) => {
      if (event.target.checked) {
        const newSelected = data.map((n) => n._id);
        setSelected(newSelected);
        return;
      }
      setSelected([]);
    },
    [data]
  );

  const handlePageChange = (e, newPage) => {
    setTableState((prevState) => ({
      ...prevState,
      currentPage: newPage,
    }));
  };

  const isSelected = (id) => selected.indexOf(id) !== -1;

  const handleLimit = (e) => {
    setTableState((prevState) => ({
      ...prevState,
      limit: +e.target.value,
      currentPage: 0,
    }));
  };

  return (
    <Paper sx={{ padding: 2 }}>
      <CustomTableHead
        openForm={openForm}
        setOpenForm={setOpenForm}
        totalRows={totalRows}
        formName={formName}
        getData={getData}
        columns={columns}
      />
      {/* Selection indicator */}
      <EnhancedTableToolbar
        selected={selected}
        setSelected={setSelected}
        formName={formName}
        getData={getData}
      />
      <TableContainer sx={{ mt: 1, minHeight: 400 }}>
        <Table stickyHeader size="small">
          <EnhancedTableHead
            numSelected={selected.length}
            onSelectAllClick={handleSelectAllClick}
            onRequestSort={handleRequestSort}
            rowCount={data.length}
            headCells={columns}
          />
          <TableBody>
            {data && (
              <CustomTableRow
                data={data}
                columns={columns}
                loading={loading}
                isSelected={isSelected}
                handleClick={handleClick}
                formName={formName}
                getData={getData}
              />
            )}
          </TableBody>
        </Table>
      </TableContainer>

      <TablePagination
        rowsPerPageOptions={[5, 10, 20, 50]}
        component="div"
        rowsPerPage={tableState.limit}
        count={totalRows}
        page={tableState.currentPage}
        onPageChange={handlePageChange}
        onRowsPerPageChange={handleLimit}
      />
    </Paper>
  );
}
