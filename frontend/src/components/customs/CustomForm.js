import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import {
  Button,
  FormControl,
  Typography,
  Paper,
  IconButton,
  Box,
} from "@mui/material";
import { Close } from "@mui/icons-material";
import { addData, editData } from "../../service/Api";
import {
  createPermissionPayload,
  fetchSelectValues,
  getSingleData,
} from "../Utils";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import formFields from "../../config/formFields.json";
import PopulateForm from "./PopulateForm";
import { toastConfig } from "../../config/toastConfig";

export default function CustomForm({
  formName,
  setOpenForm = null,
  getData = null,
  edit = false,
  itemID = null,
}) {
  const form = useForm();
  const [fields, setFields] = useState(formFields[formName]);
  const [defaultValues, setDefaultValues] = useState({});

  console.log("fields Contains", fields);

  const {
    register,
    handleSubmit,
    reset,
    formState,
    setValue,
    setError,
    watch,
  } = form;
  const { errors } = formState;

  // useEffect to get formData if edit is true and id provided
  useEffect(() => {
    const handleEdit = async () => {
      let values = await getSingleData(formName, itemID, setValue);
      setDefaultValues(values || {});
      console.log("values Contains inside handleEdit", values);

      // Check if 'role' is a field in your form
      if ("role" in values) {
        // Set the default value for the 'role' field
        console.log("values.role.name", values.role.name);
        setValue("role", values.role.name);
      }
    };

    if (edit && itemID) {
      handleEdit();
    }
  }, [edit, itemID, formName, setValue]);

  // useEffect to fetch select values
  useEffect(() => {
    const handleFetchSelectValues = async (name) => {
      let newFields = await fetchSelectValues(name, fields);
      if (newFields) setFields(newFields);
    };

    if (Array.isArray(fields)) {
      fields.forEach((item) => {
        if (
          item.type === "select" &&
          (!item.values || item.values.length === 0)
        ) {
          handleFetchSelectValues(item.name);
        }
      });
    }
  }, [fields]);

  const handleReset = () => {
    reset();
    reset(defaultValues);
  };

  // check for errors thrown from backend
  const checkErrors = (res) => {
    let result = false;
    if (res.data?.errors) {
      result = true;
      Object.keys(res.data.errors).forEach((key) => {
        setError(key, {
          type: "manual",
          message: res.data.errors[key].message,
        });
      });
    }
    return result;
  };

  // function to close form and update data
  const updateData = () => {
    getData();
    setOpenForm(false);
  };

  const onSubmit = async (data) => {
    let combinedPayload = createPermissionPayload(data);
    console.log("combined payload", combinedPayload);
    try {
      let res;
      if (!edit) res = await addData(formName, combinedPayload);
      else res = await editData(formName, itemID, combinedPayload);
      console.log("res", res);

      if (checkErrors(res)) return;
      if (res.status === 200) updateData();
      toast.success(
        `Item ${!edit ? "added" : "updated"} successfully!`,
        toastConfig
      );
    } catch (error) {
      toast.error("Error updating item!", toastConfig);
    }
  };

  return (
    <>
      <Paper
        sx={{
          px: 1.5,
          mb: 2,
          boxShadow: "none",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            mb: 2,
          }}
        >
          <Typography variant="h5" sx={{ textTransform: "capitalize" }}>
            {formName}
          </Typography>
          <IconButton
            size="small"
            sx={{ mr: -1 }}
            onClick={() => setOpenForm(false)}
          >
            <Close />
          </IconButton>
        </Box>

        <FormControl
          component="form"
          onSubmit={handleSubmit(onSubmit)}
          fullWidth
        >
          <PopulateForm
            fields={fields}
            register={register}
            errors={errors}
            watch={watch}
            defaultValues={defaultValues}
          />

          <div
            style={{
              marginTop: "1rem",
              display: "flex",
              gap: 10,
            }}
          >
            <Button variant="contained" type="submit">
              {edit ? "Update" : "Submit"}
            </Button>
            <Button variant="outlined" onClick={handleReset}>
              Reset
            </Button>
          </div>
        </FormControl>
      </Paper>
    </>
  );
}
