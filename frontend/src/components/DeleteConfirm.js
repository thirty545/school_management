import React from "react";
import { Box, Button, Typography } from "@mui/material";

export default function DeleteConfirm({ setOpen, handleDelete, item_id = null }) {
  const confirmDelete = () => {
    if (item_id) handleDelete(item_id);
    else handleDelete();
    setOpen(false);
  };

  return (
    <>
      <Typography variant="body1" id="modal-modal-title">
        Are you sure to delete?
      </Typography>
      <Box sx={{ mt: 2 }} id="modal-modal-description">
        <Button variant="contained" color="error" onClick={confirmDelete}>
          Delete
        </Button>
        <Button variant="outlined" sx={{ ml: 2 }} onClick={() => setOpen(false)}>
          Cancel
        </Button>
      </Box>
    </>
  );
}
