import { format } from "date-fns";
import { getDataById, getDatas } from "../service/Api";

const modules = [
  "class",
  "exams",
  "fee",
  "marksheet",
  "role",
  "staff_bio",
  "staff_salary_log",
  "student_bio",
  "student_class",
  "student_fee",
  "subject_class",
  "subject",
  "teacher_class",
  "user",
];

export const updateSelected = (selected, id) => {
  const selectedIndex = selected.indexOf(id);
  let newSelected = [];
  if (selectedIndex === -1) {
    newSelected = newSelected.concat(selected, id);
  } else if (selectedIndex === 0) {
    newSelected = newSelected.concat(selected.slice(1));
  } else if (selectedIndex === selected.length - 1) {
    newSelected = newSelected.concat(selected.slice(0, -1));
  } else if (selectedIndex > 0) {
    newSelected = newSelected.concat(
      selected.slice(0, selectedIndex),
      selected.slice(selectedIndex + 1)
    );
  }
  return newSelected;
};

export const createPermissionPayload = (data) => {
  const permissions = {};
  const newData = {};
  Object.keys(data).forEach((key) => {
    const match = key.match(/^(.+)-(read|write|delete)$/);
    console.log("we are inside Object.keys()");

    if (match) {
      console.log("we are inside if");
      const moduleName = match[1];
      const actionType = match[2];
      if (!permissions[moduleName]) permissions[moduleName] = {};

      permissions[moduleName][actionType] = data[key];
    } else {
      newData[key] = data[key];
    }
  });

  const combinedPayload = {
    ...(Object.keys(permissions).length > 0 ? { permissions } : {}),
    ...newData,
  };
  console.log("permissions contain", permissions);
  console.log("newData Contains", newData);

  return combinedPayload;
};

export const fetchSelectValues = async (name, currentFields) => {
  let module = name === "classRef" ? "class" : name;
  try {
    const { data } = await getDatas(module);

    if (data?.length > 0) {
      const selectValues = data.map((item) => ({
        label:
          item.grade ||
          item.name ||
          item.fee_name ||
          `${item.fname} ${item.lname}`,
        value: item._id,
      }));

      const updatedFields = currentFields.map((field) =>
        field.name === name && field.type === "select"
          ? { ...field, values: selectValues }
          : field
      );

      return updatedFields;
    } else return {};
  } catch (error) {
    console.error("Error fetching select values:", error);
  }
};

export const getSingleData = async (formName, itemID, setValue) => {
  let defaultValues = {};
  try {
    let res = await getDataById(formName, itemID);
    console.log("res is  ===============================", res);
    Object.keys(res.data).forEach((key) => {
      if (key === "year" || key === "dob" || key === "increment_date") {
        const date = new Date(res.data[key]);
        const formattedDate = format(date, "yyyy-MM-dd");

        setValue(key, formattedDate);
        defaultValues[key] = res.data[key] || "";
      } else if (key === "permissions") {
        let isAdmin = res.data.name === "superadmin";
        if (isAdmin) {
          for (let module of modules) {
            setValue(`${module}-read`, isAdmin);
            setValue(`${module}-write`, isAdmin);
            setValue(`${module}-delete`, isAdmin);
          }
        } else {
          for (let module in res.data.permissions) {
            setValue(`${module}-read`, res.data.permissions[module].read);
            setValue(`${module}-write`, res.data.permissions[module].write);
            setValue(`${module}-delete`, res.data.permissions[module].delete);
          }
        }
      } else {
        console.log("in else block key contains", key);
        console.log("res.data[key] contains", res.data[key]);
        setValue(key, res.data[key]);
        defaultValues[key] = res.data[key] || "";
      }
    });

    return defaultValues;
  } catch (error) {
    console.error("Error fetching single data:", error);
  }
};
