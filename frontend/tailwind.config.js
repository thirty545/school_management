/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        'poppins': ['Poppins','sans-serif'],
        'Inter':['Inter','sans-serif'],
        'Rounded':['Rounded','sans-serif']
      },
    },
  },
  plugins: [],
}